class MessagesController < ApplicationController
  def create
	@message = Message.new(messages_params)
    respond_to do |format|
      if @message.save
        format.html { redirect_to root_url, notice: 'News was successfully created.' }
      else
        format.html { redirect_to :back }
      end
    end
	end

	def destroy
		@message = Message.find(params[:id])
		@message.destroy
		respond_to do |format|
         format.html { redirect_to admin_url, notice: 'News was successfully destroyed.' }
        end
	end

	private

	def messages_params
		params.require(:message).permit(:name, :email, :phone, :message)
	end
end
