class AdminController < ApplicationController
  before_action :authenticate_admin!
  def index
    @messages = Message.all
  end
end
