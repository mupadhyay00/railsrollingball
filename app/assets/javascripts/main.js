jQuery(document).ready(function(t) {
  'use strict';

  function a() {
    var p = t(window).scrollTop();
    s(p), n = p, o = !1
  }

  function s(p) {
    n - p > l ? r.removeClass('is-hidden') : p - n > l && p > 0 && r.addClass('is-hidden')
  }
  var r = t('.js-autohide'),
    o = !1,
    n = 0,
    l = 10;
  t(window).on('scroll touchmove', function() {
    o || (o = !0, window.requestAnimationFrame ? requestAnimationFrame(a) : setTimeout(a, 0))
  })
});
jQuery(document).ready(function(t) {
  t('#owl').owlCarousel({

    autoPlay: 6000, //Set AutoPlay to 3 seconds
    singleItem: false,
    items : 3,
    itemsDesktop : [1199,3],
    itemsDesktopSmall : [979,2],
    itemsMobile:	[479,1],
    navigation:true,
    pagination:false,
    navigationText: ["<i class='fa fa-long-arrow-left'></i>", "<i class='fa fa-long-arrow-right'></i>"]

  });
  'use strict';
  var a = t('body'),
    s = 'field-empty',
    r = 'field-active';
  t('.fy-field .fy-input').each(function(n, l) {
    var d = t(l).parents('.fy-field');
    l.value && '' === l.value.trim() && d.addClass(s), t(l).on('focus', function() {
      d.addClass(r), d.removeClass(s)
    }), t(l).on('blur', function() {
      d.removeClass(r), '' === l.value.trim() ? d.addClass(s) : d.removeClass(s)
    })
  }), t('.js-lazy').Lazy({
    threshold: 5e3,
    beforeLoad: function(n) {
      var l = t(n);
      l.css('opacity', 0)
    },
    afterLoad: function(n) {
      var l = t(n);
      l.animate({
        opacity: 1
      }, 250), l.addClass('lazy-loaded'), l.parent('.js-lazy-parent').addClass('lazy-loaded'), console.log('loaded ' + n.data('src'))
    },
    onError: function() {}
  }), t('.fy-post-media').fitVids(), t('.fy-post-content').fitVids({
    customSelector: 'iframe'
  }), t('.fy-post-map').fitVids({
    customSelector: 'iframe[src^=\'http://google.com\'], iframe[src^=\'http://www.google.com\'], iframe[src^=\'https://google.com\'], iframe[src^=\'https://www.google.com\']'
  }), t('.fy-page-content').fitVids({
    customSelector: 'iframe[src^=\'http://google.com\'], iframe[src^=\'http://www.google.com\'], iframe[src^=\'https://google.com\'], iframe[src^=\'https://www.google.com\']'
  }), t('.fy-comment-content').fitVids({
    customSelector: 'iframe[src^=\'http://google.com\'], iframe[src^=\'http://www.google.com\'], iframe[src^=\'https://google.com\'], iframe[src^=\'https://www.google.com\']'
  }), t('a[href^="#"]').on('click', function(n) {
    n.preventDefault();
    var l = this.hash,
      d = t(l);
    d.length && t('html, body').stop().animate({
      scrollTop: d.offset().top
    }, 500, 'swing')
  });
  var o = t('.js-reveal-container');
  window.sr = ScrollReveal({
    origin: 'bottom',
    distance: '36px',
    easing: 'ease-in-out',
    duration: 500,
    delay: 0,
    useDelay: 'onload',
    reset: !1,
    opacity: 0,
    scale: 1,
    viewFactor: 0.1
  }), o.each(function(n, l) {
    var d = t(l).find('.js-reveal'),
      p = t(l).find('.js-reveal-fade');
    sr.reveal(d, 50);
    sr.reveal(p, {
      origin: 'top',
      distance: '0'
    }, 50)
  })
});
jQuery(document).ready(function(t) {
  'use strict';
  var a = t('body');
  t('.js-images').each(function(s, r) {
    var n = t(r),
      l = n.find('.js-image'),
      d = function(m) {
        var c = [];
        return m.find('.js-image').each(function(u, g) {
          var h = t(g),
            f = h.attr('href'),
            y = h.data('size').split('x'),
            v = y[0],
            x = y[1],
            b = h.parent('figure').find('figcaption').html();
          c.push({
            src: f,
            w: v,
            h: x,
            title: b
          })
        }), c
      }(n),
      p = document.querySelectorAll('.js-pswp')[0];
    l.on('click', function(m) {
      m.preventDefault(), m.stopPropagation();
      var c = t(this).data('index');
      a.addClass('fy-pswp-open'), a.hasClass('fy-pswp-closed') && a.removeClass('fy-pswp-closed');
      var g = new PhotoSwipe(p, PhotoSwipeUI_Default, d, {
        index: c,
        loop: !0,
        bgOpacity: 1,
        shareEl: !1,
        closeOnScroll: !1,
        showHideOpacity: !1,
        showAnimationDuration: 0,
        hideAnimationDuration: 0,
        preload: !0,
        loadingIndicatorDelay: 0,
        barsSize: {
          top: 60,
          bottom: 'auto'
        }
      });
      g.init(), g.listen('close', function() {
        a.hasClass('fy-pswp-open') && (a.removeClass('fy-pswp-open'), a.addClass('fy-pswp-closed'))
      })
    })
  })
});
jQuery(document).ready(function(t) {
  'use strict';
  var a = t('body'),
    s = t('.fy-page-loading');
  Pace.options = {
    ajax: !1,
    restartOnRequestAfter: !1
  }, Pace.on('done', function() {
    a.addClass('fy-loading-hiding'), a.hasClass('fy-loading-hiding') && setTimeout(function() {
      a.removeClass('fy-loading-hiding'), a.addClass('fy-loading-hidden')
    }, 500)
  })
});
(function(t) {
  t(window).load(function() {
    'use strict';
    var a = t('.fy-masonry');
    a.imagesLoaded(function() {
      a.masonry({
        itemSelector: '.fy-masonry-item',
        columnWidth: '.fy-masonry-size',
        percentPosition: !0,
        horizontalOrder: !0,
        gutter: 0
      })
    })
  })
})(jQuery);
jQuery(document).ready(function(t) {
  'use strict';

  function a(f, y) {
    f.stick_in_parent({
      parent: y,
      bottoming: !1
    })
  }
  var s = t('html'),
    r = t('body'),
    o = t('.js-header'),
    n = t('.js-navigation'),
    l = n.innerHeight(),
    d = n.position(),
    p = d.top,
    m = n.find('li:has(ul)'),
    c = t('.js-sticky-container'),
    u = c.innerWidth();
  '1024' <= u ? (a(n, c), n.trigger('sticky_kit:recalc')) : n.trigger('sticky_kit:detach'), t(window).resize(function() {
    var f = r.innerWidth();
    1024 <= f ? (a(n, c), n.trigger('sticky_kit:recalc')) : n.trigger('sticky_kit:detach')
  }), '1024' <= r.innerWidth() && m.doubleTapToGo(), t(window).on('resize', function() {
    '1024' <= r.innerWidth() && m.doubleTapToGo()
  }), t('.fy-navigation a[href="#"]').on('click', function(f) {
    f.preventDefault()
  });
  var g = t('.js-navigation-trigger');
  g.each(function(f, y) {
    t(y).on('click touchstart', function(v) {
      v.preventDefault(), r.hasClass('navigation-active') ? (r.addClass('navigation-closing'), setTimeout(function() {
        s.removeClass('navigation-active'), r.removeClass('navigation-closing'), r.removeClass('navigation-active')
      }, 250)) : (s.addClass('navigation-active'), r.addClass('navigation-active'))
    })
  });
  var h = t('.js-navigation-close');
  h.each(function(f, y) {
    t(y).on('click touchstart', function() {
      r.hasClass('navigation-active') && (r.addClass('navigation-closing'), setTimeout(function() {
        s.removeClass('navigation-active'), r.removeClass('navigation-closing'), r.removeClass('navigation-active')
      }, 250))
    })
  }), t(document).keyup(function(f) {
    27 == f.keyCode && r.hasClass('navigation-active') && (r.addClass('navigation-closing'), setTimeout(function() {
      s.addClass('navigation-active'), r.removeClass('navigation-closing'), r.removeClass('navigation-active')
    }, 250))
  }), t('.menu-item-has-children a').focus(function() {
    t(this).siblings('.sub-menu').parent('li').addClass('focus')
  }).blur(function() {
    t(this).siblings('.sub-menu').parent('li').removeClass('focus')
  }), t('.sub-menu a').focus(function() {
    t(this).parents('.sub-menu').parent('li').addClass('focus')
  }).blur(function() {
    t(this).parents('.sub-menu').parent('li').removeClass('focus')
  })
});
(function(t, a, s) {
  t.fn.doubleTapToGo = function() {
    return ('ontouchstart' in a || navigator.msMaxTouchPoints || navigator.userAgent.toLowerCase().match(/windows phone os 7/i)) && (this.each(function() {
      var r = !1;
      t(this).on('click', function(o) {
        var n = t(this);
        n[0] != r[0] && (o.preventDefault(), r = n)
      }), t(s).on('click touchstart MSPointerDown', function(o) {
        for (var n = !0, l = t(o.target).parents(), d = 0; d < l.length; d++) l[d] == r[0] && (n = !1);
        n && (r = !1)
      })
    }), this)
  }
})(jQuery, window, document),
function(t) {
  'use strict';
  t.fn.fitVids = function(a) {
    var s = {
      customSelector: null,
      ignore: null
    };
    if (!document.getElementById('fit-vids-style')) {
      var r = document.head || document.getElementsByTagName('head')[0],
        o = document.createElement('div');
      o.innerHTML = '<p>x</p><style id="fit-vids-style">.fluid-width-video-wrapper{width:100%;position:relative;padding:0;}.fluid-width-video-wrapper iframe,.fluid-width-video-wrapper object,.fluid-width-video-wrapper embed {position:absolute;top:0;left:0;width:100%;height:100%;}</style>', r.appendChild(o.childNodes[1])
    }
    return a && t.extend(s, a), this.each(function() {
      var n = ['iframe[src*="player.vimeo.com"]', 'iframe[src*="youtube.com"]', 'iframe[src*="youtube-nocookie.com"]', 'iframe[src*="kickstarter.com"][src*="video.html"]', 'object', 'embed'];
      s.customSelector && n.push(s.customSelector);
      var l = '.fitvidsignore';
      s.ignore && (l = l + ', ' + s.ignore);
      var d = t(this).find(n.join(','));
      d = d.not('object object'), d = d.not(l), d.each(function() {
        var p = t(this);
        if (!(0 < p.parents(l).length) && !('embed' === this.tagName.toLowerCase() && p.parent('object').length || p.parent('.fluid-width-video-wrapper').length)) {
          !p.css('height') && !p.css('width') && (isNaN(p.attr('height')) || isNaN(p.attr('width'))) && (p.attr('height', 9), p.attr('width', 16));
          var m = 'object' === this.tagName.toLowerCase() || p.attr('height') && !isNaN(parseInt(p.attr('height'), 10)) ? parseInt(p.attr('height'), 10) : p.height(),
            c = isNaN(parseInt(p.attr('width'), 10)) ? p.width() : parseInt(p.attr('width'), 10);
          if (!p.attr('name')) {
            var u = 'fitvid' + t.fn.fitVids._count;
            p.attr('name', u), t.fn.fitVids._count++
          }
          p.wrap('<div class="fluid-width-video-wrapper"></div>').parent('.fluid-width-video-wrapper').css('padding-top', 100 * (m / c) + '%'), p.removeAttr('height').removeAttr('width')
        }
      })
    })
  }, t.fn.fitVids._count = 0
}(window.jQuery || window.Zepto),
function(t, a) {
  'function' == typeof define && define.amd ? define('ev-emitter/ev-emitter', a) : 'object' == typeof module && module.exports ? module.exports = a() : t.EvEmitter = a()
}('undefined' == typeof window ? this : window, function() {
  function t() {}
  var a = t.prototype;
  return a.on = function(s, r) {
    if (s && r) {
      var o = this._events = this._events || {},
        n = o[s] = o[s] || [];
      return -1 == n.indexOf(r) && n.push(r), this
    }
  }, a.once = function(s, r) {
    if (s && r) {
      this.on(s, r);
      var o = this._onceEvents = this._onceEvents || {},
        n = o[s] = o[s] || {};
      return n[r] = !0, this
    }
  }, a.off = function(s, r) {
    var o = this._events && this._events[s];
    if (o && o.length) {
      var n = o.indexOf(r);
      return -1 != n && o.splice(n, 1), this
    }
  }, a.emitEvent = function(s, r) {
    var o = this._events && this._events[s];
    if (o && o.length) {
      var n = 0,
        l = o[n];
      r = r || [];
      for (var p, d = this._onceEvents && this._onceEvents[s]; l;) p = d && d[l], p && (this.off(s, l), delete d[l]), l.apply(this, r), n += p ? 0 : 1, l = o[n];
      return this
    }
  }, t
}),
function(t, a) {
  'use strict';
  'function' == typeof define && define.amd ? define(['ev-emitter/ev-emitter'], function(s) {
    return a(t, s)
  }) : 'object' == typeof module && module.exports ? module.exports = a(t, require('ev-emitter')) : t.imagesLoaded = a(t, t.EvEmitter)
}(window, function(t, a) {
  function s(c, u) {
    for (var g in u) c[g] = u[g];
    return c
  }

  function r(c) {
    var u = [];
    if (Array.isArray(c)) u = c;
    else if ('number' == typeof c.length)
      for (var g = 0; g < c.length; g++) u.push(c[g]);
    else u.push(c);
    return u
  }

  function o(c, u, g) {
    return this instanceof o ? void('string' == typeof c && (c = document.querySelectorAll(c)), this.elements = r(c), this.options = s({}, this.options), 'function' == typeof u ? g = u : s(this.options, u), g && this.on('always', g), this.getImages(), d && (this.jqDeferred = new d.Deferred), setTimeout(function() {
      this.check()
    }.bind(this))) : new o(c, u, g)
  }

  function n(c) {
    this.img = c
  }

  function l(c, u) {
    this.url = c, this.element = u, this.img = new Image
  }
  var d = t.jQuery,
    p = t.console;
  o.prototype = Object.create(a.prototype), o.prototype.options = {}, o.prototype.getImages = function() {
    this.images = [], this.elements.forEach(this.addElementImages, this)
  }, o.prototype.addElementImages = function(c) {
    'IMG' == c.nodeName && this.addImage(c), !0 === this.options.background && this.addElementBackgroundImages(c);
    var u = c.nodeType;
    if (u && m[u]) {
      for (var f, g = c.querySelectorAll('img'), h = 0; h < g.length; h++) f = g[h], this.addImage(f);
      if ('string' == typeof this.options.background) {
        var y = c.querySelectorAll(this.options.background);
        for (h = 0; h < y.length; h++) {
          var v = y[h];
          this.addElementBackgroundImages(v)
        }
      }
    }
  };
  var m = {
    1: !0,
    9: !0,
    11: !0
  };
  return o.prototype.addElementBackgroundImages = function(c) {
    var u = getComputedStyle(c);
    if (u)
      for (var f, g = /url\((['"])?(.*?)\1\)/gi, h = g.exec(u.backgroundImage); null !== h;) f = h && h[2], f && this.addBackground(f, c), h = g.exec(u.backgroundImage)
  }, o.prototype.addImage = function(c) {
    var u = new n(c);
    this.images.push(u)
  }, o.prototype.addBackground = function(c, u) {
    var g = new l(c, u);
    this.images.push(g)
  }, o.prototype.check = function() {
    function c(g, h, f) {
      setTimeout(function() {
        u.progress(g, h, f)
      })
    }
    var u = this;
    return this.progressedCount = 0, this.hasAnyBroken = !1, this.images.length ? void this.images.forEach(function(g) {
      g.once('progress', c), g.check()
    }) : void this.complete()
  }, o.prototype.progress = function(c, u, g) {
    this.progressedCount++, this.hasAnyBroken = this.hasAnyBroken || !c.isLoaded, this.emitEvent('progress', [this, c, u]), this.jqDeferred && this.jqDeferred.notify && this.jqDeferred.notify(this, c), this.progressedCount == this.images.length && this.complete(), this.options.debug && p && p.log('progress: ' + g, c, u)
  }, o.prototype.complete = function() {
    var c = this.hasAnyBroken ? 'fail' : 'done';
    if (this.isComplete = !0, this.emitEvent(c, [this]), this.emitEvent('always', [this]), this.jqDeferred) {
      var u = this.hasAnyBroken ? 'reject' : 'resolve';
      this.jqDeferred[u](this)
    }
  }, n.prototype = Object.create(a.prototype), n.prototype.check = function() {
    var c = this.getIsImageComplete();
    return c ? void this.confirm(0 !== this.img.naturalWidth, 'naturalWidth') : void(this.proxyImage = new Image, this.proxyImage.addEventListener('load', this), this.proxyImage.addEventListener('error', this), this.img.addEventListener('load', this), this.img.addEventListener('error', this), this.proxyImage.src = this.img.src)
  }, n.prototype.getIsImageComplete = function() {
    return this.img.complete && void 0 !== this.img.naturalWidth
  }, n.prototype.confirm = function(c, u) {
    this.isLoaded = c, this.emitEvent('progress', [this, this.img, u])
  }, n.prototype.handleEvent = function(c) {
    var u = 'on' + c.type;
    this[u] && this[u](c)
  }, n.prototype.onload = function() {
    this.confirm(!0, 'onload'), this.unbindEvents()
  }, n.prototype.onerror = function() {
    this.confirm(!1, 'onerror'), this.unbindEvents()
  }, n.prototype.unbindEvents = function() {
    this.proxyImage.removeEventListener('load', this), this.proxyImage.removeEventListener('error', this), this.img.removeEventListener('load', this), this.img.removeEventListener('error', this)
  }, l.prototype = Object.create(n.prototype), l.prototype.check = function() {
    this.img.addEventListener('load', this), this.img.addEventListener('error', this), this.img.src = this.url;
    var c = this.getIsImageComplete();
    c && (this.confirm(0 !== this.img.naturalWidth, 'naturalWidth'), this.unbindEvents())
  }, l.prototype.unbindEvents = function() {
    this.img.removeEventListener('load', this), this.img.removeEventListener('error', this)
  }, l.prototype.confirm = function(c, u) {
    this.isLoaded = c, this.emitEvent('progress', [this, this.element, u])
  }, o.makeJQueryPlugin = function(c) {
    c = c || t.jQuery, c && (d = c, d.fn.imagesLoaded = function(u, g) {
      var h = new o(this, u, g);
      return h.jqDeferred.promise(d(this))
    })
  }, o.makeJQueryPlugin(), o
}),
function(t, a) {
  'use strict';

  function s(d, p, m, c, u) {
    function g() {
      j = 1 < t.devicePixelRatio, h(m), 0 <= p.delay && setTimeout(function() {
        f(!0)
      }, p.delay), (0 > p.delay || p.combined) && (c.e = E(p.throttle, function(Ws) {
        'resize' === Ws.type && (O = q = -1), f(Ws.all)
      }), c.a = function(Ws) {
        h(Ws), m.push.apply(m, Ws)
      }, c.g = function() {
        return m = o(m).filter(function() {
          return !o(this).data(p.loadedName)
        })
      }, c.f = function(Ws) {
        for (var Xs, Hs = 0; Hs < Ws.length; Hs++) Xs = m.filter(function() {
          return this === Ws[Hs]
        }), Xs.length && f(!1, Xs)
      }, f(), o(p.appendScroll).on('scroll.' + u + ' resize.' + u, c.e))
    }

    function h(Ws) {
      var Hs = p.defaultImage,
        Xs = p.placeholder,
        Fs = p.imageBase,
        Ns = p.srcsetAttribute,
        Gs = p.loaderAttribute,
        Ys = p._f || {};
      Ws = o(Ws).filter(function() {
        var Ks = o(this),
          Qs = w(this);
        return !Ks.data(p.handledName) && (Ks.attr(p.attribute) || Ks.attr(Ns) || Ks.attr(Gs) || Ys[Qs] !== a)
      }).data('plugin_' + p.name, d);
      for (var qs = 0, js = Ws.length; qs < js; qs++) {
        var Vs = o(Ws[qs]),
          Zs = w(Ws[qs]),
          Us = Vs.attr(p.imageBaseAttribute) || Fs;
        Zs === _s && Us && Vs.attr(Ns) && Vs.attr(Ns, T(Vs.attr(Ns), Us)), Ys[Zs] === a || Vs.attr(Gs) || Vs.attr(Gs, Ys[Zs]), Zs === _s && Hs && !Vs.attr(Os) ? Vs.attr(Os, Hs) : Zs !== _s && Xs && (!Vs.css(Bs) || 'none' === Vs.css(Bs)) && Vs.css(Bs, 'url(\'' + Xs + '\')')
      }
    }

    function f(Ws, Hs) {
      if (!m.length) return void(p.autoDestroy && d.destroy());
      for (var Xs = Hs || m, Fs = !1, Ns = p.imageBase || '', Gs = p.srcsetAttribute, Ys = p.handledName, qs = 0; qs < Xs.length; qs++)
        if (Ws || Hs || v(Xs[qs])) {
          var js = o(Xs[qs]),
            Vs = w(Xs[qs]),
            Zs = js.attr(p.attribute),
            Us = js.attr(p.imageBaseAttribute) || Ns,
            Ks = js.attr(p.loaderAttribute);
          !js.data(Ys) && (!p.visibleOnly || js.is(':visible')) && ((Zs || js.attr(Gs)) && (Vs === _s && (Us + Zs !== js.attr(Os) || js.attr(Gs) !== js.attr(Rs)) || Vs !== _s && Us + Zs !== js.css(Bs)) || Ks) && (Fs = !0, js.data(Ys, !0), y(js, Vs, Us, Ks))
        }
      Fs && (m = o(m).filter(function() {
        return !o(this).data(Ys)
      }))
    }

    function y(Ws, Hs, Xs, Fs) {
      ++_;
      var Ns = function() {
        k('onError', Ws), z(), Ns = o.noop
      };
      k('beforeLoad', Ws);
      var Gs = p.attribute,
        Ys = p.srcsetAttribute,
        qs = p.sizesAttribute,
        js = p.retinaAttribute,
        Vs = p.removeAttribute,
        Zs = p.loadedName,
        Us = Ws.attr(js);
      if (Fs) {
        var Ks = function() {
          Vs && Ws.removeAttr(p.loaderAttribute), Ws.data(Zs, !0), k(pt, Ws), setTimeout(z, 1), Ks = o.noop
        };
        Ws.off(ws).one(ws, Ns).one(ua, Ks), k(Fs, Ws, function(Js) {
          Js ? (Ws.off(ua), Ks()) : (Ws.off(ws), Ns())
        }) || Ws.trigger(ws)
      } else {
        var Qs = o(new Image);
        Qs.one(ws, Ns).one(ua, function() {
          Ws.hide(), Hs === _s ? Ws.attr(Ms, Qs.attr(Ms)).attr(Rs, Qs.attr(Rs)).attr(Os, Qs.attr(Os)) : Ws.css(Bs, 'url(\'' + Qs.attr(Os) + '\')'), Ws[p.effect](p.effectTime), Vs && (Ws.removeAttr(Gs + ' ' + Ys + ' ' + js + ' ' + p.imageBaseAttribute), qs !== Ms && Ws.removeAttr(qs)), Ws.data(Zs, !0), k(pt, Ws), Qs.remove(), z()
        });
        var $s = (j && Us ? Us : Ws.attr(Gs)) || '';
        Qs.attr(Ms, Ws.attr(qs)).attr(Rs, Ws.attr(Ys)).attr(Os, $s ? Xs + $s : null), Qs.complete && Qs.trigger(ua)
      }
    }

    function v(Ws) {
      var Hs = Ws.getBoundingClientRect(),
        Xs = p.scrollDirection,
        Fs = p.threshold,
        Ns = b() + Fs > Hs.top && -Fs < Hs.bottom,
        Gs = x() + Fs > Hs.left && -Fs < Hs.right;
      return 'vertical' === Xs ? Ns : 'horizontal' === Xs ? Gs : Ns && Gs
    }

    function x() {
      return 0 <= O ? O : O = o(t).width()
    }

    function b() {
      return 0 <= q ? q : q = o(t).height()
    }

    function w(Ws) {
      return Ws.tagName.toLowerCase()
    }

    function T(Ws, Hs) {
      if (Hs) {
        var Xs = Ws.split(',');
        Ws = '';
        for (var Fs = 0, Ns = Xs.length; Fs < Ns; Fs++) Ws += Hs + Xs[Fs].trim() + (Fs === Ns - 1 ? '' : ',')
      }
      return Ws
    }

    function E(Ws, Hs) {
      var Fs, Xs = 0;
      return function(Ns, Gs) {
        function Ys() {
          Xs = +new Date, Hs.call(d, Ns)
        }
        var qs = +new Date - Xs;
        Fs && clearTimeout(Fs), qs > Ws || !p.enableThrottle || Gs ? Ys() : Fs = setTimeout(Ys, Ws - qs)
      }
    }

    function z() {
      --_, m.length || _ || k('onFinishedAll')
    }

    function k(Ws) {
      return !!(Ws = p[Ws]) && (Ws.apply(d, [].slice.call(arguments, 1)), !0)
    }
    var _ = 0,
      O = -1,
      q = -1,
      j = !1,
      pt = 'afterLoad',
      ua = 'load',
      ws = 'error',
      _s = 'img',
      Os = 'src',
      Rs = 'srcset',
      Ms = 'sizes',
      Bs = 'background-image';
    'event' === p.bind || l ? g() : o(t).on(ua + '.' + u, g)
  }

  function r(d, p) {
    var m = this,
      c = o.extend({}, m.config, p),
      u = {},
      g = c.name + '-' + ++n;
    return m.config = function(h, f) {
      return f === a ? c[h] : (c[h] = f, m)
    }, m.addItems = function(h) {
      return u.a && u.a('string' === o.type(h) ? o(h) : h), m
    }, m.getItems = function() {
      return u.g ? u.g() : {}
    }, m.update = function(h) {
      return u.e && u.e({}, !h), m
    }, m.force = function(h) {
      return u.f && u.f('string' === o.type(h) ? o(h) : h), m
    }, m.loadAll = function() {
      return u.e && u.e({
        all: !0
      }, !0), m
    }, m.destroy = function() {
      return o(c.appendScroll).off('.' + g, u.e), o(t).off('.' + g), u = {}, a
    }, s(m, c, d, u, g), c.chainable ? d : m
  }
  var o = t.jQuery || t.Zepto,
    n = 0,
    l = !1;
  o.fn.Lazy = o.fn.lazy = function(d) {
    return new r(this, d)
  }, o.Lazy = o.lazy = function(d, p, m) {
    if (o.isFunction(p) && (m = p, p = []), !!o.isFunction(m)) {
      d = o.isArray(d) ? d : [d], p = o.isArray(p) ? p : [p];
      for (var c = r.prototype.config, u = c._f || (c._f = {}), g = 0, h = d.length; g < h; g++)(c[d[g]] === a || o.isFunction(c[d[g]])) && (c[d[g]] = m);
      for (var f = 0, y = p.length; f < y; f++) u[p[f]] = d[0]
    }
  }, r.prototype.config = {
    name: 'lazy',
    chainable: !0,
    autoDestroy: !0,
    bind: 'load',
    threshold: 500,
    visibleOnly: !1,
    appendScroll: t,
    scrollDirection: 'both',
    imageBase: null,
    defaultImage: 'data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==',
    placeholder: null,
    delay: -1,
    combined: !1,
    attribute: 'data-src',
    srcsetAttribute: 'data-srcset',
    sizesAttribute: 'data-sizes',
    retinaAttribute: 'data-retina',
    loaderAttribute: 'data-loader',
    imageBaseAttribute: 'data-imagebase',
    removeAttribute: !0,
    handledName: 'handled',
    loadedName: 'loaded',
    effect: 'show',
    effectTime: 0,
    enableThrottle: !0,
    throttle: 250,
    beforeLoad: a,
    afterLoad: a,
    onError: a,
    onFinishedAll: a
  }, o(t).on('load', function() {
    l = !0
  })
}(window), ! function(t, a) {
  'function' == typeof define && define.amd ? define('jquery-bridget/jquery-bridget', ['jquery'], function(s) {
    return a(t, s)
  }) : 'object' == typeof module && module.exports ? module.exports = a(t, require('jquery')) : t.jQueryBridget = a(t, t.jQuery)
}(window, function(t, a) {
  'use strict';

  function s(d, p, m) {
    function c(g, h, f) {
      var v, y = '$().' + d + '("' + h + '")';
      return g.each(function(x, b) {
        var w = m.data(b, d);
        if (!w) return void l(d + ' not initialized. Cannot call methods, i.e. ' + y);
        var T = w[h];
        if (!T || '_' == h.charAt(0)) return void l(y + ' is not a valid method');
        var E = T.apply(w, f);
        v = void 0 == v ? E : v
      }), void 0 === v ? g : v
    }

    function u(g, h) {
      g.each(function(f, y) {
        var v = m.data(y, d);
        v ? (v.option(h), v._init()) : (v = new p(y, h), m.data(y, d, v))
      })
    }
    m = m || a || t.jQuery, m && (p.prototype.option || (p.prototype.option = function(g) {
      m.isPlainObject(g) && (this.options = m.extend(!0, this.options, g))
    }), m.fn[d] = function(g) {
      if ('string' == typeof g) {
        var h = o.call(arguments, 1);
        return c(this, g, h)
      }
      return u(this, g), this
    }, r(m))
  }

  function r(d) {
    !d || d && d.bridget || (d.bridget = s)
  }
  var o = Array.prototype.slice,
    n = t.console,
    l = 'undefined' == typeof n ? function() {} : function(d) {
      n.error(d)
    };
  return r(a || t.jQuery), s
}),
function(t, a) {
  'function' == typeof define && define.amd ? define('ev-emitter/ev-emitter', a) : 'object' == typeof module && module.exports ? module.exports = a() : t.EvEmitter = a()
}('undefined' == typeof window ? this : window, function() {
  function t() {}
  var a = t.prototype;
  return a.on = function(s, r) {
    if (s && r) {
      var o = this._events = this._events || {},
        n = o[s] = o[s] || [];
      return -1 == n.indexOf(r) && n.push(r), this
    }
  }, a.once = function(s, r) {
    if (s && r) {
      this.on(s, r);
      var o = this._onceEvents = this._onceEvents || {},
        n = o[s] = o[s] || {};
      return n[r] = !0, this
    }
  }, a.off = function(s, r) {
    var o = this._events && this._events[s];
    if (o && o.length) {
      var n = o.indexOf(r);
      return -1 != n && o.splice(n, 1), this
    }
  }, a.emitEvent = function(s, r) {
    var o = this._events && this._events[s];
    if (o && o.length) {
      var n = 0,
        l = o[n];
      r = r || [];
      for (var p, d = this._onceEvents && this._onceEvents[s]; l;) p = d && d[l], p && (this.off(s, l), delete d[l]), l.apply(this, r), n += p ? 0 : 1, l = o[n];
      return this
    }
  }, t
}),
function(t, a) {
  'use strict';
  'function' == typeof define && define.amd ? define('get-size/get-size', [], function() {
    return a()
  }) : 'object' == typeof module && module.exports ? module.exports = a() : t.getSize = a()
}(window, function() {
  'use strict';

  function t(c) {
    var u = parseFloat(c),
      g = -1 == c.indexOf('%') && !isNaN(u);
    return g && u
  }

  function a() {
    for (var g, c = {
        width: 0,
        height: 0,
        innerWidth: 0,
        innerHeight: 0,
        outerWidth: 0,
        outerHeight: 0
      }, u = 0; d > u; u++) g = l[u], c[g] = 0;
    return c
  }

  function s(c) {
    var u = getComputedStyle(c);
    return u || n('Style returned ' + u + '. Are you running this code in a hidden iframe on Firefox? See http://bit.ly/getsizebug1'), u
  }

  function r() {
    if (!p) {
      p = !0;
      var c = document.createElement('div');
      c.style.width = '200px', c.style.padding = '1px 2px 3px 4px', c.style.borderStyle = 'solid', c.style.borderWidth = '1px 2px 3px 4px', c.style.boxSizing = 'border-box';
      var u = document.body || document.documentElement;
      u.appendChild(c);
      var g = s(c);
      o.isBoxSizeOuter = m = 200 == t(g.width), u.removeChild(c)
    }
  }

  function o(c) {
    if (r(), 'string' == typeof c && (c = document.querySelector(c)), c && 'object' == typeof c && c.nodeType) {
      var u = s(c);
      if ('none' == u.display) return a();
      for (var g = {
          width: c.offsetWidth,
          height: c.offsetHeight
        }, h = g.isBorderBox = 'border-box' == u.boxSizing, f = 0; d > f; f++) {
        var y = l[f],
          v = u[y],
          x = parseFloat(v);
        g[y] = isNaN(x) ? 0 : x
      }
      var b = g.paddingLeft + g.paddingRight,
        w = g.paddingTop + g.paddingBottom,
        T = g.marginLeft + g.marginRight,
        E = g.marginTop + g.marginBottom,
        z = g.borderLeftWidth + g.borderRightWidth,
        k = g.borderTopWidth + g.borderBottomWidth,
        _ = h && m,
        O = t(u.width);
      !1 !== O && (g.width = O + (_ ? 0 : b + z));
      var q = t(u.height);
      return !1 !== q && (g.height = q + (_ ? 0 : w + k)), g.innerWidth = g.width - (b + z), g.innerHeight = g.height - (w + k), g.outerWidth = g.width + T, g.outerHeight = g.height + E, g
    }
  }
  var m, n = 'undefined' == typeof console ? function() {} : function(c) {
      console.error(c)
    },
    l = ['paddingLeft', 'paddingRight', 'paddingTop', 'paddingBottom', 'marginLeft', 'marginRight', 'marginTop', 'marginBottom', 'borderLeftWidth', 'borderRightWidth', 'borderTopWidth', 'borderBottomWidth'],
    d = l.length,
    p = !1;
  return o
}),
function(t, a) {
  'use strict';
  'function' == typeof define && define.amd ? define('desandro-matches-selector/matches-selector', a) : 'object' == typeof module && module.exports ? module.exports = a() : t.matchesSelector = a()
}(window, function() {
  'use strict';
  var t = function() {
    var a = Element.prototype;
    if (a.matches) return 'matches';
    if (a.matchesSelector) return 'matchesSelector';
    for (var s = ['webkit', 'moz', 'ms', 'o'], r = 0; r < s.length; r++) {
      var o = s[r],
        n = o + 'MatchesSelector';
      if (a[n]) return n
    }
  }();
  return function(a, s) {
    return a[t](s)
  }
}),
function(t, a) {
  'function' == typeof define && define.amd ? define('fizzy-ui-utils/utils', ['desandro-matches-selector/matches-selector'], function(s) {
    return a(t, s)
  }) : 'object' == typeof module && module.exports ? module.exports = a(t, require('desandro-matches-selector')) : t.fizzyUIUtils = a(t, t.matchesSelector)
}(window, function(t, a) {
  var s = {};
  s.extend = function(o, n) {
    for (var l in n) o[l] = n[l];
    return o
  }, s.modulo = function(o, n) {
    return (o % n + n) % n
  }, s.makeArray = function(o) {
    var n = [];
    if (Array.isArray(o)) n = o;
    else if (o && 'number' == typeof o.length)
      for (var l = 0; l < o.length; l++) n.push(o[l]);
    else n.push(o);
    return n
  }, s.removeFrom = function(o, n) {
    var l = o.indexOf(n); - 1 != l && o.splice(l, 1)
  }, s.getParent = function(o, n) {
    for (; o != document.body;)
      if (o = o.parentNode, a(o, n)) return o
  }, s.getQueryElement = function(o) {
    return 'string' == typeof o ? document.querySelector(o) : o
  }, s.handleEvent = function(o) {
    var n = 'on' + o.type;
    this[n] && this[n](o)
  }, s.filterFindElements = function(o, n) {
    o = s.makeArray(o);
    var l = [];
    return o.forEach(function(d) {
      if (d instanceof HTMLElement) {
        if (!n) return void l.push(d);
        a(d, n) && l.push(d);
        for (var p = d.querySelectorAll(n), m = 0; m < p.length; m++) l.push(p[m])
      }
    }), l
  }, s.debounceMethod = function(o, n, l) {
    var d = o.prototype[n],
      p = n + 'Timeout';
    o.prototype[n] = function() {
      var m = this[p];
      m && clearTimeout(m);
      var c = arguments,
        u = this;
      this[p] = setTimeout(function() {
        d.apply(u, c), delete u[p]
      }, l || 100)
    }
  }, s.docReady = function(o) {
    var n = document.readyState;
    'complete' == n || 'interactive' == n ? o() : document.addEventListener('DOMContentLoaded', o)
  }, s.toDashed = function(o) {
    return o.replace(/(.)([A-Z])/g, function(n, l, d) {
      return l + '-' + d
    }).toLowerCase()
  };
  var r = t.console;
  return s.htmlInit = function(o, n) {
    s.docReady(function() {
      var l = s.toDashed(n),
        d = 'data-' + l,
        p = document.querySelectorAll('[' + d + ']'),
        m = document.querySelectorAll('.js-' + l),
        c = s.makeArray(p).concat(s.makeArray(m)),
        u = t.jQuery;
      c.forEach(function(g) {
        var f, h = g.getAttribute(d) || g.getAttribute(d + '-options');
        try {
          f = h && JSON.parse(h)
        } catch (v) {
          return void(r && r.error('Error parsing ' + d + ' on ' + g.className + ': ' + v))
        }
        var y = new o(g, f);
        u && u.data(g, n, y)
      })
    })
  }, s
}),
function(t, a) {
  'function' == typeof define && define.amd ? define('outlayer/item', ['ev-emitter/ev-emitter', 'get-size/get-size'], a) : 'object' == typeof module && module.exports ? module.exports = a(require('ev-emitter'), require('get-size')) : (t.Outlayer = {}, t.Outlayer.Item = a(t.EvEmitter, t.getSize))
}(window, function(t, a) {
  'use strict';

  function s(h) {
    for (var f in h) return !1;
    return f = null, !0
  }

  function r(h, f) {
    h && (this.element = h, this.layout = f, this.position = {
      x: 0,
      y: 0
    }, this._create())
  }
  var o = document.documentElement.style,
    n = 'string' == typeof o.transition ? 'transition' : 'WebkitTransition',
    l = 'string' == typeof o.transform ? 'transform' : 'WebkitTransform',
    d = {
      WebkitTransition: 'webkitTransitionEnd',
      transition: 'transitionend'
    }[n],
    p = {
      transform: l,
      transition: n,
      transitionDuration: n + 'Duration',
      transitionProperty: n + 'Property',
      transitionDelay: n + 'Delay'
    },
    m = r.prototype = Object.create(t.prototype);
  m.constructor = r, m._create = function() {
    this._transn = {
      ingProperties: {},
      clean: {},
      onEnd: {}
    }, this.css({
      position: 'absolute'
    })
  }, m.handleEvent = function(h) {
    var f = 'on' + h.type;
    this[f] && this[f](h)
  }, m.getSize = function() {
    this.size = a(this.element)
  }, m.css = function(h) {
    var f = this.element.style;
    for (var y in h) {
      var v = p[y] || y;
      f[v] = h[y]
    }
  }, m.getPosition = function() {
    var h = getComputedStyle(this.element),
      f = this.layout._getOption('originLeft'),
      y = this.layout._getOption('originTop'),
      v = h[f ? 'left' : 'right'],
      x = h[y ? 'top' : 'bottom'],
      b = this.layout.size,
      w = -1 == v.indexOf('%') ? parseInt(v, 10) : parseFloat(v) / 100 * b.width,
      T = -1 == x.indexOf('%') ? parseInt(x, 10) : parseFloat(x) / 100 * b.height;
    w = isNaN(w) ? 0 : w, T = isNaN(T) ? 0 : T, w -= f ? b.paddingLeft : b.paddingRight, T -= y ? b.paddingTop : b.paddingBottom, this.position.x = w, this.position.y = T
  }, m.layoutPosition = function() {
    var h = this.layout.size,
      f = {},
      y = this.layout._getOption('originLeft'),
      v = this.layout._getOption('originTop'),
      x = y ? 'paddingLeft' : 'paddingRight',
      b = y ? 'left' : 'right',
      w = y ? 'right' : 'left',
      T = this.position.x + h[x];
    f[b] = this.getXValue(T), f[w] = '';
    var E = v ? 'paddingTop' : 'paddingBottom',
      z = v ? 'top' : 'bottom',
      k = v ? 'bottom' : 'top',
      _ = this.position.y + h[E];
    f[z] = this.getYValue(_), f[k] = '', this.css(f), this.emitEvent('layout', [this])
  }, m.getXValue = function(h) {
    var f = this.layout._getOption('horizontal');
    return this.layout.options.percentPosition && !f ? 100 * (h / this.layout.size.width) + '%' : h + 'px'
  }, m.getYValue = function(h) {
    var f = this.layout._getOption('horizontal');
    return this.layout.options.percentPosition && f ? 100 * (h / this.layout.size.height) + '%' : h + 'px'
  }, m._transitionTo = function(h, f) {
    this.getPosition();
    var y = this.position.x,
      v = this.position.y,
      x = parseInt(h, 10),
      b = parseInt(f, 10),
      w = x === this.position.x && b === this.position.y;
    if (this.setPosition(h, f), w && !this.isTransitioning) return void this.layoutPosition();
    var T = {};
    T.transform = this.getTranslate(h - y, f - v), this.transition({
      to: T,
      onTransitionEnd: {
        transform: this.layoutPosition
      },
      isCleaning: !0
    })
  }, m.getTranslate = function(h, f) {
    var y = this.layout._getOption('originLeft'),
      v = this.layout._getOption('originTop');
    return h = y ? h : -h, f = v ? f : -f, 'translate3d(' + h + 'px, ' + f + 'px, 0)'
  }, m.goTo = function(h, f) {
    this.setPosition(h, f), this.layoutPosition()
  }, m.moveTo = m._transitionTo, m.setPosition = function(h, f) {
    this.position.x = parseInt(h, 10), this.position.y = parseInt(f, 10)
  }, m._nonTransition = function(h) {
    for (var f in this.css(h.to), h.isCleaning && this._removeStyles(h.to), h.onTransitionEnd) h.onTransitionEnd[f].call(this)
  }, m.transition = function(h) {
    if (!parseFloat(this.layout.options.transitionDuration)) return void this._nonTransition(h);
    var f = this._transn;
    for (var y in h.onTransitionEnd) f.onEnd[y] = h.onTransitionEnd[y];
    for (y in h.to) f.ingProperties[y] = !0, h.isCleaning && (f.clean[y] = !0);
    h.from && (this.css(h.from), this.element.offsetHeight), this.enableTransition(h.to), this.css(h.to), this.isTransitioning = !0
  };
  var c = 'opacity,' + function(h) {
    return h.replace(/([A-Z])/g, function(f) {
      return '-' + f.toLowerCase()
    })
  }(l);
  m.enableTransition = function() {
    if (!this.isTransitioning) {
      var h = this.layout.options.transitionDuration;
      h = 'number' == typeof h ? h + 'ms' : h, this.css({
        transitionProperty: c,
        transitionDuration: h,
        transitionDelay: this.staggerDelay || 0
      }), this.element.addEventListener(d, this, !1)
    }
  }, m.onwebkitTransitionEnd = function(h) {
    this.ontransitionend(h)
  }, m.onotransitionend = function(h) {
    this.ontransitionend(h)
  };
  var u = {
    '-webkit-transform': 'transform'
  };
  m.ontransitionend = function(h) {
    if (h.target === this.element) {
      var f = this._transn,
        y = u[h.propertyName] || h.propertyName;
      if (delete f.ingProperties[y], s(f.ingProperties) && this.disableTransition(), y in f.clean && (this.element.style[h.propertyName] = '', delete f.clean[y]), y in f.onEnd) {
        var v = f.onEnd[y];
        v.call(this), delete f.onEnd[y]
      }
      this.emitEvent('transitionEnd', [this])
    }
  }, m.disableTransition = function() {
    this.removeTransitionStyles(), this.element.removeEventListener(d, this, !1), this.isTransitioning = !1
  }, m._removeStyles = function(h) {
    var f = {};
    for (var y in h) f[y] = '';
    this.css(f)
  };
  var g = {
    transitionProperty: '',
    transitionDuration: '',
    transitionDelay: ''
  };
  return m.removeTransitionStyles = function() {
    this.css(g)
  }, m.stagger = function(h) {
    h = isNaN(h) ? 0 : h, this.staggerDelay = h + 'ms'
  }, m.removeElem = function() {
    this.element.parentNode.removeChild(this.element), this.css({
      display: ''
    }), this.emitEvent('remove', [this])
  }, m.remove = function() {
    return n && parseFloat(this.layout.options.transitionDuration) ? (this.once('transitionEnd', function() {
      this.removeElem()
    }), void this.hide()) : void this.removeElem()
  }, m.reveal = function() {
    delete this.isHidden, this.css({
      display: ''
    });
    var h = this.layout.options,
      f = {},
      y = this.getHideRevealTransitionEndProperty('visibleStyle');
    f[y] = this.onRevealTransitionEnd, this.transition({
      from: h.hiddenStyle,
      to: h.visibleStyle,
      isCleaning: !0,
      onTransitionEnd: f
    })
  }, m.onRevealTransitionEnd = function() {
    this.isHidden || this.emitEvent('reveal')
  }, m.getHideRevealTransitionEndProperty = function(h) {
    var f = this.layout.options[h];
    if (f.opacity) return 'opacity';
    for (var y in f) return y
  }, m.hide = function() {
    this.isHidden = !0, this.css({
      display: ''
    });
    var h = this.layout.options,
      f = {},
      y = this.getHideRevealTransitionEndProperty('hiddenStyle');
    f[y] = this.onHideTransitionEnd, this.transition({
      from: h.visibleStyle,
      to: h.hiddenStyle,
      isCleaning: !0,
      onTransitionEnd: f
    })
  }, m.onHideTransitionEnd = function() {
    this.isHidden && (this.css({
      display: 'none'
    }), this.emitEvent('hide'))
  }, m.destroy = function() {
    this.css({
      position: '',
      left: '',
      right: '',
      top: '',
      bottom: '',
      transition: '',
      transform: ''
    })
  }, r
}),
function(t, a) {
  'use strict';
  'function' == typeof define && define.amd ? define('outlayer/outlayer', ['ev-emitter/ev-emitter', 'get-size/get-size', 'fizzy-ui-utils/utils', './item'], function(s, r, o, n) {
    return a(t, s, r, o, n)
  }) : 'object' == typeof module && module.exports ? module.exports = a(t, require('ev-emitter'), require('get-size'), require('fizzy-ui-utils'), require('./item')) : t.Outlayer = a(t, t.EvEmitter, t.getSize, t.fizzyUIUtils, t.Outlayer.Item)
}(window, function(t, a, s, r, o) {
  'use strict';

  function n(y, v) {
    var x = r.getQueryElement(y);
    if (!x) return void(p && p.error('Bad element for ' + this.constructor.namespace + ': ' + (x || y)));
    this.element = x, m && (this.$element = m(this.element)), this.options = r.extend({}, this.constructor.defaults), this.option(v);
    var b = ++u;
    this.element.outlayerGUID = b, g[b] = this, this._create();
    var w = this._getOption('initLayout');
    w && this.layout()
  }

  function l(y) {
    function v() {
      y.apply(this, arguments)
    }
    return v.prototype = Object.create(y.prototype), v.prototype.constructor = v, v
  }

  function d(y) {
    if ('number' == typeof y) return y;
    var v = y.match(/(^\d*\.?\d*)(\w*)/),
      x = v && v[1],
      b = v && v[2];
    if (!x.length) return 0;
    x = parseFloat(x);
    var w = f[b] || 1;
    return x * w
  }
  var p = t.console,
    m = t.jQuery,
    c = function() {},
    u = 0,
    g = {};
  n.namespace = 'outlayer', n.Item = o, n.defaults = {
    containerStyle: {
      position: 'relative'
    },
    initLayout: !0,
    originLeft: !0,
    originTop: !0,
    resize: !0,
    resizeContainer: !0,
    transitionDuration: '0.4s',
    hiddenStyle: {
      opacity: 0,
      transform: 'scale(0.001)'
    },
    visibleStyle: {
      opacity: 1,
      transform: 'scale(1)'
    }
  };
  var h = n.prototype;
  r.extend(h, a.prototype), h.option = function(y) {
    r.extend(this.options, y)
  }, h._getOption = function(y) {
    var v = this.constructor.compatOptions[y];
    return v && void 0 !== this.options[v] ? this.options[v] : this.options[y]
  }, n.compatOptions = {
    initLayout: 'isInitLayout',
    horizontal: 'isHorizontal',
    layoutInstant: 'isLayoutInstant',
    originLeft: 'isOriginLeft',
    originTop: 'isOriginTop',
    resize: 'isResizeBound',
    resizeContainer: 'isResizingContainer'
  }, h._create = function() {
    this.reloadItems(), this.stamps = [], this.stamp(this.options.stamp), r.extend(this.element.style, this.options.containerStyle);
    var y = this._getOption('resize');
    y && this.bindResize()
  }, h.reloadItems = function() {
    this.items = this._itemize(this.element.children)
  }, h._itemize = function(y) {
    for (var v = this._filterFindItemElements(y), x = this.constructor.Item, b = [], w = 0; w < v.length; w++) {
      var T = v[w],
        E = new x(T, this);
      b.push(E)
    }
    return b
  }, h._filterFindItemElements = function(y) {
    return r.filterFindElements(y, this.options.itemSelector)
  }, h.getItemElements = function() {
    return this.items.map(function(y) {
      return y.element
    })
  }, h.layout = function() {
    this._resetLayout(), this._manageStamps();
    var y = this._getOption('layoutInstant'),
      v = void 0 === y ? !this._isLayoutInited : y;
    this.layoutItems(this.items, v), this._isLayoutInited = !0
  }, h._init = h.layout, h._resetLayout = function() {
    this.getSize()
  }, h.getSize = function() {
    this.size = s(this.element)
  }, h._getMeasurement = function(y, v) {
    var b, x = this.options[y];
    x ? ('string' == typeof x ? b = this.element.querySelector(x) : x instanceof HTMLElement && (b = x), this[y] = b ? s(b)[v] : x) : this[y] = 0
  }, h.layoutItems = function(y, v) {
    y = this._getItemsForLayout(y), this._layoutItems(y, v), this._postLayout()
  }, h._getItemsForLayout = function(y) {
    return y.filter(function(v) {
      return !v.isIgnored
    })
  }, h._layoutItems = function(y, v) {
    if (this._emitCompleteOnItems('layout', y), y && y.length) {
      var x = [];
      y.forEach(function(b) {
        var w = this._getItemLayoutPosition(b);
        w.item = b, w.isInstant = v || b.isLayoutInstant, x.push(w)
      }, this), this._processLayoutQueue(x)
    }
  }, h._getItemLayoutPosition = function() {
    return {
      x: 0,
      y: 0
    }
  }, h._processLayoutQueue = function(y) {
    this.updateStagger(), y.forEach(function(v, x) {
      this._positionItem(v.item, v.x, v.y, v.isInstant, x)
    }, this)
  }, h.updateStagger = function() {
    var y = this.options.stagger;
    return null === y || void 0 === y ? void(this.stagger = 0) : (this.stagger = d(y), this.stagger)
  }, h._positionItem = function(y, v, x, b, w) {
    b ? y.goTo(v, x) : (y.stagger(w * this.stagger), y.moveTo(v, x))
  }, h._postLayout = function() {
    this.resizeContainer()
  }, h.resizeContainer = function() {
    var y = this._getOption('resizeContainer');
    if (y) {
      var v = this._getContainerSize();
      v && (this._setContainerMeasure(v.width, !0), this._setContainerMeasure(v.height, !1))
    }
  }, h._getContainerSize = c, h._setContainerMeasure = function(y, v) {
    if (void 0 !== y) {
      var x = this.size;
      x.isBorderBox && (y += v ? x.paddingLeft + x.paddingRight + x.borderLeftWidth + x.borderRightWidth : x.paddingBottom + x.paddingTop + x.borderTopWidth + x.borderBottomWidth), y = Math.max(y, 0), this.element.style[v ? 'width' : 'height'] = y + 'px'
    }
  }, h._emitCompleteOnItems = function(y, v) {
    function x() {
      w.dispatchEvent(y + 'Complete', null, [v])
    }

    function b() {
      E++, E == T && x()
    }
    var w = this,
      T = v.length;
    if (!v || !T) return void x();
    var E = 0;
    v.forEach(function(z) {
      z.once(y, b)
    })
  }, h.dispatchEvent = function(y, v, x) {
    var b = v ? [v].concat(x) : x;
    if (this.emitEvent(y, b), m)
      if (this.$element = this.$element || m(this.element), v) {
        var w = m.Event(v);
        w.type = y, this.$element.trigger(w, x)
      } else this.$element.trigger(y, x)
  }, h.ignore = function(y) {
    var v = this.getItem(y);
    v && (v.isIgnored = !0)
  }, h.unignore = function(y) {
    var v = this.getItem(y);
    v && delete v.isIgnored
  }, h.stamp = function(y) {
    y = this._find(y), y && (this.stamps = this.stamps.concat(y), y.forEach(this.ignore, this))
  }, h.unstamp = function(y) {
    y = this._find(y), y && y.forEach(function(v) {
      r.removeFrom(this.stamps, v), this.unignore(v)
    }, this)
  }, h._find = function(y) {
    return y ? ('string' == typeof y && (y = this.element.querySelectorAll(y)), y = r.makeArray(y)) : void 0
  }, h._manageStamps = function() {
    this.stamps && this.stamps.length && (this._getBoundingRect(), this.stamps.forEach(this._manageStamp, this))
  }, h._getBoundingRect = function() {
    var y = this.element.getBoundingClientRect(),
      v = this.size;
    this._boundingRect = {
      left: y.left + v.paddingLeft + v.borderLeftWidth,
      top: y.top + v.paddingTop + v.borderTopWidth,
      right: y.right - (v.paddingRight + v.borderRightWidth),
      bottom: y.bottom - (v.paddingBottom + v.borderBottomWidth)
    }
  }, h._manageStamp = c, h._getElementOffset = function(y) {
    var v = y.getBoundingClientRect(),
      x = this._boundingRect,
      b = s(y),
      w = {
        left: v.left - x.left - b.marginLeft,
        top: v.top - x.top - b.marginTop,
        right: x.right - v.right - b.marginRight,
        bottom: x.bottom - v.bottom - b.marginBottom
      };
    return w
  }, h.handleEvent = r.handleEvent, h.bindResize = function() {
    t.addEventListener('resize', this), this.isResizeBound = !0
  }, h.unbindResize = function() {
    t.removeEventListener('resize', this), this.isResizeBound = !1
  }, h.onresize = function() {
    this.resize()
  }, r.debounceMethod(n, 'onresize', 100), h.resize = function() {
    this.isResizeBound && this.needsResizeLayout() && this.layout()
  }, h.needsResizeLayout = function() {
    var y = s(this.element),
      v = this.size && y;
    return v && y.innerWidth !== this.size.innerWidth
  }, h.addItems = function(y) {
    var v = this._itemize(y);
    return v.length && (this.items = this.items.concat(v)), v
  }, h.appended = function(y) {
    var v = this.addItems(y);
    v.length && (this.layoutItems(v, !0), this.reveal(v))
  }, h.prepended = function(y) {
    var v = this._itemize(y);
    if (v.length) {
      var x = this.items.slice(0);
      this.items = v.concat(x), this._resetLayout(), this._manageStamps(), this.layoutItems(v, !0), this.reveal(v), this.layoutItems(x)
    }
  }, h.reveal = function(y) {
    if (this._emitCompleteOnItems('reveal', y), y && y.length) {
      var v = this.updateStagger();
      y.forEach(function(x, b) {
        x.stagger(b * v), x.reveal()
      })
    }
  }, h.hide = function(y) {
    if (this._emitCompleteOnItems('hide', y), y && y.length) {
      var v = this.updateStagger();
      y.forEach(function(x, b) {
        x.stagger(b * v), x.hide()
      })
    }
  }, h.revealItemElements = function(y) {
    var v = this.getItems(y);
    this.reveal(v)
  }, h.hideItemElements = function(y) {
    var v = this.getItems(y);
    this.hide(v)
  }, h.getItem = function(y) {
    for (var x, v = 0; v < this.items.length; v++)
      if (x = this.items[v], x.element == y) return x
  }, h.getItems = function(y) {
    y = r.makeArray(y);
    var v = [];
    return y.forEach(function(x) {
      var b = this.getItem(x);
      b && v.push(b)
    }, this), v
  }, h.remove = function(y) {
    var v = this.getItems(y);
    this._emitCompleteOnItems('remove', v), v && v.length && v.forEach(function(x) {
      x.remove(), r.removeFrom(this.items, x)
    }, this)
  }, h.destroy = function() {
    var y = this.element.style;
    y.height = '', y.position = '', y.width = '', this.items.forEach(function(x) {
      x.destroy()
    }), this.unbindResize();
    var v = this.element.outlayerGUID;
    delete g[v], delete this.element.outlayerGUID, m && m.removeData(this.element, this.constructor.namespace)
  }, n.data = function(y) {
    y = r.getQueryElement(y);
    var v = y && y.outlayerGUID;
    return v && g[v]
  }, n.create = function(y, v) {
    var x = l(n);
    return x.defaults = r.extend({}, n.defaults), r.extend(x.defaults, v), x.compatOptions = r.extend({}, n.compatOptions), x.namespace = y, x.data = n.data, x.Item = l(o), r.htmlInit(x, y), m && m.bridget && m.bridget(y, x), x
  };
  var f = {
    ms: 1,
    s: 1e3
  };
  return n.Item = o, n
}),
function(t, a) {
  'function' == typeof define && define.amd ? define(['outlayer/outlayer', 'get-size/get-size'], a) : 'object' == typeof module && module.exports ? module.exports = a(require('outlayer'), require('get-size')) : t.Masonry = a(t.Outlayer, t.getSize)
}(window, function(t, a) {
  var s = t.create('masonry');
  return s.compatOptions.fitWidth = 'isFitWidth', s.prototype._resetLayout = function() {
    this.getSize(), this._getMeasurement('columnWidth', 'outerWidth'), this._getMeasurement('gutter', 'outerWidth'), this.measureColumns(), this.colYs = [];
    for (var r = 0; r < this.cols; r++) this.colYs.push(0);
    this.maxY = 0
  }, s.prototype.measureColumns = function() {
    if (this.getContainerWidth(), !this.columnWidth) {
      var r = this.items[0],
        o = r && r.element;
      this.columnWidth = o && a(o).outerWidth || this.containerWidth
    }
    var n = this.columnWidth += this.gutter,
      l = this.containerWidth + this.gutter,
      d = l / n,
      p = n - l % n,
      m = p && 1 > p ? 'round' : 'floor';
    d = Math[m](d), this.cols = Math.max(d, 1)
  }, s.prototype.getContainerWidth = function() {
    var r = this._getOption('fitWidth'),
      o = r ? this.element.parentNode : this.element,
      n = a(o);
    this.containerWidth = n && n.innerWidth
  }, s.prototype._getItemLayoutPosition = function(r) {
    r.getSize();
    var o = r.size.outerWidth % this.columnWidth,
      n = o && 1 > o ? 'round' : 'ceil',
      l = Math[n](r.size.outerWidth / this.columnWidth);
    l = Math.min(l, this.cols);
    for (var d = this._getColGroup(l), p = Math.min.apply(Math, d), m = d.indexOf(p), c = {
        x: this.columnWidth * m,
        y: p
      }, u = p + r.size.outerHeight, g = this.cols + 1 - d.length, h = 0; g > h; h++) this.colYs[m + h] = u;
    return c
  }, s.prototype._getColGroup = function(r) {
    if (2 > r) return this.colYs;
    for (var d, o = [], n = this.cols + 1 - r, l = 0; n > l; l++) d = this.colYs.slice(l, l + r), o[l] = Math.max.apply(Math, d);
    return o
  }, s.prototype._manageStamp = function(r) {
    var o = a(r),
      n = this._getElementOffset(r),
      l = this._getOption('originLeft'),
      d = l ? n.left : n.right,
      p = d + o.outerWidth,
      m = Math.floor(d / this.columnWidth);
    m = Math.max(0, m);
    var c = Math.floor(p / this.columnWidth);
    c -= p % this.columnWidth ? 0 : 1, c = Math.min(this.cols - 1, c);
    for (var u = this._getOption('originTop'), g = (u ? n.top : n.bottom) + o.outerHeight, h = m; c >= h; h++) this.colYs[h] = Math.max(g, this.colYs[h])
  }, s.prototype._getContainerSize = function() {
    this.maxY = Math.max.apply(Math, this.colYs);
    var r = {
      height: this.maxY
    };
    return this._getOption('fitWidth') && (r.width = this._getContainerFitWidth()), r
  }, s.prototype._getContainerFitWidth = function() {
    for (var r = 0, o = this.cols; --o && 0 === this.colYs[o];) r++;
    return (this.cols - r) * this.columnWidth - this.gutter
  }, s.prototype.needsResizeLayout = function() {
    var r = this.containerWidth;
    return this.getContainerWidth(), r != this.containerWidth
  }, s
}),
function() {
  var o, n, l, d, p, m, c, u, g, h, f, y, v, x, b, w, T, E, z, k, _, O, q, j, pt, ua, ws, _s, Os, Rs, Ms, Bs, Ws, Hs, Xs, Fs, Ns, Gs, Ys, qs, js, Vs, Zs, Us, Ks, Qs, $s, Js, ei, t = [].slice,
    a = {}.hasOwnProperty,
    s = function(ti, ai) {
      function si() {
        this.constructor = ti
      }
      for (var ii in ai) a.call(ai, ii) && (ti[ii] = ai[ii]);
      return si.prototype = ai.prototype, ti.prototype = new si, ti.__super__ = ai.prototype, ti
    },
    r = [].indexOf || function(ti) {
      for (var ai = 0, si = this.length; ai < si; ai++)
        if (ai in this && this[ai] === ti) return ai;
      return -1
    };
  for (_ = {
      catchupTime: 100,
      initialRate: .03,
      minTime: 250,
      ghostTime: 100,
      maxProgressPerFrame: 20,
      easeFactor: 1.25,
      startOnPageLoad: !0,
      restartOnPushState: !0,
      restartOnRequestAfter: 500,
      target: 'body',
      elements: {
        checkInterval: 100,
        selectors: ['body']
      },
      eventLag: {
        minSamples: 10,
        sampleCount: 3,
        lagThreshold: 3
      },
      ajax: {
        trackMethods: ['GET'],
        trackWebSockets: !0,
        ignoreURLs: []
      }
    }, Os = function() {
      var ti;
      return null == (ti = 'undefined' != typeof performance && null !== performance ? 'function' == typeof performance.now ? performance.now() : void 0 : void 0) ? +new Date : ti
    }, Ms = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame, k = window.cancelAnimationFrame || window.mozCancelAnimationFrame, null == Ms && (Ms = function(ti) {
      return setTimeout(ti, 50)
    }, k = function(ti) {
      return clearTimeout(ti)
    }), Ws = function(ti) {
      var ai, si;
      return ai = Os(), si = function() {
        var ii;
        return ii = Os() - ai, 33 <= ii ? (ai = Os(), ti(ii, function() {
          return Ms(si)
        })) : setTimeout(si, 33 - ii)
      }, si()
    }, Bs = function() {
      var ti, ai, si;
      return si = arguments[0], ai = arguments[1], ti = 3 <= arguments.length ? t.call(arguments, 2) : [], 'function' == typeof si[ai] ? si[ai].apply(si, ti) : si[ai]
    }, O = function() {
      var ti, ai, si, ii, ri, oi, ni;
      for (ai = arguments[0], ii = 2 <= arguments.length ? t.call(arguments, 1) : [], oi = 0, ni = ii.length; oi < ni; oi++)
        if (si = ii[oi], si)
          for (ti in si) a.call(si, ti) && (ri = si[ti], null != ai[ti] && 'object' == typeof ai[ti] && null != ri && 'object' == typeof ri ? O(ai[ti], ri) : ai[ti] = ri);
      return ai
    }, T = function(ti) {
      var ai, si, ii, ri, oi;
      for (si = ai = 0, ri = 0, oi = ti.length; ri < oi; ri++) ii = ti[ri], si += Math.abs(ii), ai++;
      return si / ai
    }, j = function(ti, ai) {
      var si, ii, ri;
      if (null == ti && (ti = 'options'), null == ai && (ai = !0), ri = document.querySelector('[data-pace-' + ti + ']'), !!ri) {
        if (si = ri.getAttribute('data-pace-' + ti), !ai) return si;
        try {
          return JSON.parse(si)
        } catch (oi) {
          return ii = oi, 'undefined' != typeof console && null !== console ? console.error('Error parsing inline pace options', ii) : void 0
        }
      }
    }, c = function() {
      function ti() {}
      return ti.prototype.on = function(ai, si, ii, ri) {
        var oi;
        return null == ri && (ri = !1), null == this.bindings && (this.bindings = {}), null == (oi = this.bindings)[ai] && (oi[ai] = []), this.bindings[ai].push({
          handler: si,
          ctx: ii,
          once: ri
        })
      }, ti.prototype.once = function(ai, si, ii) {
        return this.on(ai, si, ii, !0)
      }, ti.prototype.off = function(ai, si) {
        var ii, ri, oi;
        if (null != (null == (ri = this.bindings) ? void 0 : ri[ai])) {
          if (null == si) return delete this.bindings[ai];
          for (ii = 0, oi = []; ii < this.bindings[ai].length;) this.bindings[ai][ii].handler === si ? oi.push(this.bindings[ai].splice(ii, 1)) : oi.push(ii++);
          return oi
        }
      }, ti.prototype.trigger = function() {
        var ai, si, ii, ri, oi, ni, li, di, pi;
        if (ii = arguments[0], ai = 2 <= arguments.length ? t.call(arguments, 1) : [], null == (li = this.bindings) ? void 0 : li[ii]) {
          for (oi = 0, pi = []; oi < this.bindings[ii].length;) di = this.bindings[ii][oi], ri = di.handler, si = di.ctx, ni = di.once, ri.apply(null == si ? this : si, ai), ni ? pi.push(this.bindings[ii].splice(oi, 1)) : pi.push(oi++);
          return pi
        }
      }, ti
    }(), h = window.Pace || {}, window.Pace = h, O(h, c.prototype), Rs = h.options = O({}, _, window.paceOptions, j()), $s = ['ajax', 'document', 'eventLag', 'elements'], Zs = 0, Ks = $s.length; Zs < Ks; Zs++) Ns = $s[Zs], !0 === Rs[Ns] && (Rs[Ns] = _[Ns]);
  g = function(ti) {
    function ai() {
      return Js = ai.__super__.constructor.apply(this, arguments), Js
    }
    return s(ai, ti), ai
  }(Error), n = function() {
    function ti() {
      this.progress = 0
    }
    return ti.prototype.getElement = function() {
      var ai;
      if (null == this.el) {
        if (ai = document.querySelector(Rs.target), !ai) throw new g;
        this.el = document.createElement('div'), this.el.className = 'pace pace-active', document.body.className = document.body.className.replace(/pace-done/g, ''), document.body.className += ' pace-running', this.el.innerHTML = '<div class="pace-progress">\n  <div class="pace-progress-inner"></div>\n</div>\n<div class="pace-activity"></div>', null == ai.firstChild ? ai.appendChild(this.el) : ai.insertBefore(this.el, ai.firstChild)
      }
      return this.el
    }, ti.prototype.finish = function() {
      var ai;
      return ai = this.getElement(), ai.className = ai.className.replace('pace-active', ''), ai.className += ' pace-inactive', document.body.className = document.body.className.replace('pace-running', ''), document.body.className += ' pace-done'
    }, ti.prototype.update = function(ai) {
      return this.progress = ai, this.render()
    }, ti.prototype.destroy = function() {
      try {
        this.getElement().parentNode.removeChild(this.getElement())
      } catch (ai) {
        g = ai
      }
      return this.el = void 0
    }, ti.prototype.render = function() {
      var ai, si, ii, ri, oi, ni, li;
      if (null == document.querySelector(Rs.target)) return !1;
      for (ai = this.getElement(), ri = 'translate3d(' + this.progress + '%, 0, 0)', li = ['webkitTransform', 'msTransform', 'transform'], oi = 0, ni = li.length; oi < ni; oi++) si = li[oi], ai.children[0].style[si] = ri;
      return (!this.lastRenderedProgress || 0 | (this.lastRenderedProgress | 0 !== this.progress)) && (ai.children[0].setAttribute('data-progress-text', '' + (0 | this.progress) + '%'), 100 <= this.progress ? ii = '99' : (ii = 10 > this.progress ? '0' : '', ii += 0 | this.progress), ai.children[0].setAttribute('data-progress', '' + ii)), this.lastRenderedProgress = this.progress
    }, ti.prototype.done = function() {
      return 100 <= this.progress
    }, ti
  }(), u = function() {
    function ti() {
      this.bindings = {}
    }
    return ti.prototype.trigger = function(ai, si) {
      var ii, ri, oi, ni, li;
      if (null != this.bindings[ai]) {
        for (ni = this.bindings[ai], li = [], ri = 0, oi = ni.length; ri < oi; ri++) ii = ni[ri], li.push(ii.call(this, si));
        return li
      }
    }, ti.prototype.on = function(ai, si) {
      var ii;
      return null == (ii = this.bindings)[ai] && (ii[ai] = []), this.bindings[ai].push(si)
    }, ti
  }(), Vs = window.XMLHttpRequest, js = window.XDomainRequest, qs = window.WebSocket, q = function(ti, ai) {
    var si, ii;
    for (si in ii = [], ai.prototype) try {
      null == ti[si] && 'function' != typeof ai[si] ? 'function' == typeof Object.defineProperty ? ii.push(Object.defineProperty(ti, si, {
        get: function() {
          return ai.prototype[si]
        },
        configurable: !0,
        enumerable: !0
      })) : ii.push(ti[si] = ai.prototype[si]) : ii.push(void 0)
    } catch (ri) {}
    return ii
  }, ws = [], h.ignore = function() {
    var ti, ai, si;
    return ai = arguments[0], ti = 2 <= arguments.length ? t.call(arguments, 1) : [], ws.unshift('ignore'), si = ai.apply(null, ti), ws.shift(), si
  }, h.track = function() {
    var ti, ai, si;
    return ai = arguments[0], ti = 2 <= arguments.length ? t.call(arguments, 1) : [], ws.unshift('track'), si = ai.apply(null, ti), ws.shift(), si
  }, Fs = function(ti) {
    var ai;
    if (null == ti && (ti = 'GET'), 'track' === ws[0]) return 'force';
    if (!ws.length && Rs.ajax) {
      if ('socket' === ti && Rs.ajax.trackWebSockets) return !0;
      if (ai = ti.toUpperCase(), 0 <= r.call(Rs.ajax.trackMethods, ai)) return !0
    }
    return !1
  }, f = function(ti) {
    function ai() {
      var ii, si = this;
      ai.__super__.constructor.apply(this, arguments), ii = function(ri) {
        var oi;
        return oi = ri.open, ri.open = function(ni, li) {
          return Fs(ni) && si.trigger('request', {
            type: ni,
            url: li,
            request: ri
          }), oi.apply(ri, arguments)
        }
      }, window.XMLHttpRequest = function(ri) {
        var oi;
        return oi = new Vs(ri), ii(oi), oi
      };
      try {
        q(window.XMLHttpRequest, Vs)
      } catch (ri) {}
      if (null != js) {
        window.XDomainRequest = function() {
          var ri;
          return ri = new js, ii(ri), ri
        };
        try {
          q(window.XDomainRequest, js)
        } catch (ri) {}
      }
      if (null != qs && Rs.ajax.trackWebSockets) {
        window.WebSocket = function(ri, oi) {
          var ni;
          return ni = null == oi ? new qs(ri) : new qs(ri, oi), Fs('socket') && si.trigger('request', {
            type: 'socket',
            url: ri,
            protocols: oi,
            request: ni
          }), ni
        };
        try {
          q(window.WebSocket, qs)
        } catch (ri) {}
      }
    }
    return s(ai, ti), ai
  }(u), Us = null, pt = function() {
    return null == Us && (Us = new f), Us
  }, Xs = function(ti) {
    var ai, si, ii, ri;
    for (ri = Rs.ajax.ignoreURLs, si = 0, ii = ri.length; si < ii; si++)
      if (ai = ri[si], 'string' == typeof ai) {
        if (-1 !== ti.indexOf(ai)) return !0;
      } else if (ai.test(ti)) return !0;
    return !1
  }, pt().on('request', function(ti) {
    var ai, si, ii, ri, oi;
    return ri = ti.type, ii = ti.request, oi = ti.url, Xs(oi) || h.running || !1 === Rs.restartOnRequestAfter && 'force' !== Fs(ri) ? void 0 : (si = arguments, ai = Rs.restartOnRequestAfter || 0, 'boolean' == typeof ai && (ai = 0), setTimeout(function() {
      var ni, li, di, pi, mi, ci;
      if (ni = 'socket' === ri ? 2 > ii.readyState : 0 < (pi = ii.readyState) && 4 > pi, ni) {
        for (h.restart(), mi = h.sources, ci = [], li = 0, di = mi.length; li < di; li++)
          if (Ns = mi[li], Ns instanceof o) {
            Ns.watch.apply(Ns, si);
            break
          } else ci.push(void 0);
        return ci
      }
    }, ai))
  }), o = function() {
    function ti() {
      var ai = this;
      this.elements = [], pt().on('request', function() {
        return ai.watch.apply(ai, arguments)
      })
    }
    return ti.prototype.watch = function(ai) {
      var si, ii, ri, oi;
      if (ri = ai.type, si = ai.request, oi = ai.url, !Xs(oi)) return ii = 'socket' === ri ? new x(si) : new b(si), this.elements.push(ii)
    }, ti
  }(), b = function() {
    return function(ti) {
      var si, ii, ri, oi, ni, li, ai = this;
      if (this.progress = 0, null != window.ProgressEvent)
        for (ii = null, ti.addEventListener('progress', function(di) {
            return di.lengthComputable ? ai.progress = 100 * di.loaded / di.total : ai.progress += (100 - ai.progress) / 2
          }, !1), li = ['load', 'abort', 'timeout', 'error'], ri = 0, oi = li.length; ri < oi; ri++) si = li[ri], ti.addEventListener(si, function() {
          return ai.progress = 100
        }, !1);
      else ni = ti.onreadystatechange, ti.onreadystatechange = function() {
        var di;
        return 0 === (di = ti.readyState) || 4 === di ? ai.progress = 100 : 3 === ti.readyState && (ai.progress = 50), 'function' == typeof ni ? ni.apply(null, arguments) : void 0
      }
    }
  }(), x = function() {
    return function(ti) {
      var si, ii, ri, oi, ai = this;
      for (this.progress = 0, oi = ['error', 'open'], ii = 0, ri = oi.length; ii < ri; ii++) si = oi[ii], ti.addEventListener(si, function() {
        return ai.progress = 100
      }, !1)
    }
  }(), d = function() {
    return function(ti) {
      var ai, si, ii, ri;
      for (null == ti && (ti = {}), this.elements = [], null == ti.selectors && (ti.selectors = []), ri = ti.selectors, si = 0, ii = ri.length; si < ii; si++) ai = ri[si], this.elements.push(new p(ai))
    }
  }(), p = function() {
    function ti(ai) {
      this.selector = ai, this.progress = 0, this.check()
    }
    return ti.prototype.check = function() {
      var ai = this;
      return document.querySelector(this.selector) ? this.done() : setTimeout(function() {
        return ai.check()
      }, Rs.elements.checkInterval)
    }, ti.prototype.done = function() {
      return this.progress = 100
    }, ti
  }(), l = function() {
    function ti() {
      var si, ii, ai = this;
      this.progress = null == (ii = this.states[document.readyState]) ? 100 : ii, si = document.onreadystatechange, document.onreadystatechange = function() {
        return null != ai.states[document.readyState] && (ai.progress = ai.states[document.readyState]), 'function' == typeof si ? si.apply(null, arguments) : void 0
      }
    }
    return ti.prototype.states = {
      loading: 0,
      interactive: 50,
      complete: 100
    }, ti
  }(), m = function() {
    return function() {
      var ai, si, ii, ri, oi, ti = this;
      this.progress = 0, ai = 0, oi = [], ri = 0, ii = Os(), si = setInterval(function() {
        var ni;
        return ni = Os() - ii - 50, ii = Os(), oi.push(ni), oi.length > Rs.eventLag.sampleCount && oi.shift(), ai = T(oi), ++ri >= Rs.eventLag.minSamples && ai < Rs.eventLag.lagThreshold ? (ti.progress = 100, clearInterval(si)) : ti.progress = 100 * (3 / (ai + 3))
      }, 50)
    }
  }(), v = function() {
    function ti(ai) {
      this.source = ai, this.last = this.sinceLastUpdate = 0, this.rate = Rs.initialRate, this.catchup = 0, this.progress = this.lastProgress = 0, null != this.source && (this.progress = Bs(this.source, 'progress'))
    }
    return ti.prototype.tick = function(ai, si) {
      var ii;
      return null == si && (si = Bs(this.source, 'progress')), 100 <= si && (this.done = !0), si === this.last ? this.sinceLastUpdate += ai : (this.sinceLastUpdate && (this.rate = (si - this.last) / this.sinceLastUpdate), this.catchup = (si - this.progress) / Rs.catchupTime, this.sinceLastUpdate = 0, this.last = si), si > this.progress && (this.progress += this.catchup * ai), ii = 1 - Math.pow(this.progress / 100, Rs.easeFactor), this.progress += ii * this.rate * ai, this.progress = Math.min(this.lastProgress + Rs.maxProgressPerFrame, this.progress), this.progress = Math.max(0, this.progress), this.progress = Math.min(100, this.progress), this.lastProgress = this.progress, this.progress
    }, ti
  }(), Gs = null, Hs = null, E = null, Ys = null, w = null, z = null, h.running = !1, ua = function() {
    if (Rs.restartOnPushState) return h.restart()
  }, null != window.history.pushState && (Qs = window.history.pushState, window.history.pushState = function() {
    return ua(), Qs.apply(window.history, arguments)
  }), null != window.history.replaceState && (ei = window.history.replaceState, window.history.replaceState = function() {
    return ua(), ei.apply(window.history, arguments)
  }), y = {
    ajax: o,
    elements: d,
    document: l,
    eventLag: m
  }, (_s = function() {
    var ti, ai, si, ii, ri, oi, ni, li;
    for (h.sources = Gs = [], oi = ['ajax', 'elements', 'document', 'eventLag'], ai = 0, ii = oi.length; ai < ii; ai++) ti = oi[ai], !1 !== Rs[ti] && Gs.push(new y[ti](Rs[ti]));
    for (li = null == (ni = Rs.extraSources) ? [] : ni, si = 0, ri = li.length; si < ri; si++) Ns = li[si], Gs.push(new Ns(Rs));
    return h.bar = E = new n, Hs = [], Ys = new v
  })(), h.stop = function() {
    return h.trigger('stop'), h.running = !1, E.destroy(), z = !0, null != w && ('function' == typeof k && k(w), w = null), _s()
  }, h.restart = function() {
    return h.trigger('restart'), h.stop(), h.start()
  }, h.go = function() {
    var ti;
    return h.running = !0, E.render(), ti = Os(), z = !1, w = Ws(function(ai, si) {
      var ii, ri, oi, ni, li, di, pi, mi, ci, ui, gi, hi, fi, yi, vi, xi;
      for (mi = 100 - E.progress, ri = gi = 0, oi = !0, di = hi = 0, yi = Gs.length; hi < yi; di = ++hi)
        for (Ns = Gs[di], ui = null == Hs[di] ? Hs[di] = [] : Hs[di], li = null == (xi = Ns.elements) ? [Ns] : xi, pi = fi = 0, vi = li.length; fi < vi; pi = ++fi)(ni = li[pi], ci = null == ui[pi] ? ui[pi] = new v(ni) : ui[pi], oi &= ci.done, !ci.done) && (ri++, gi += ci.tick(ai));
      return ii = gi / ri, E.update(Ys.tick(ai, ii)), E.done() || oi || z ? (E.update(100), h.trigger('done'), setTimeout(function() {
        return E.finish(), h.running = !1, h.trigger('hide')
      }, Math.max(Rs.ghostTime, Math.max(Rs.minTime - (Os() - ti), 0)))) : si()
    })
  }, h.start = function(ti) {
    O(Rs, ti), h.running = !0;
    try {
      E.render()
    } catch (ai) {
      g = ai
    }
    return document.querySelector('.pace') ? (h.trigger('start'), h.go()) : setTimeout(h.start, 50)
  }, 'function' == typeof define && define.amd ? define(['pace'], function() {
    return h
  }) : 'object' == typeof exports ? module.exports = h : Rs.startOnPageLoad && h.start()
}.call(this),
  function(t, a) {
    'function' == typeof define && define.amd ? define(a) : 'object' == typeof exports ? module.exports = a() : t.PhotoSwipeUI_Default = a()
  }(this, function() {
    'use strict';
    return function(t, a) {
      var n, l, d, p, m, c, u, h, f, y, v, x, b, w, T, z, Os, Rs, s = this,
        r = !1,
        o = !0,
        g = !0,
        E = {
          barsSize: {
            top: 44,
            bottom: 'auto'
          },
          closeElClasses: ['item', 'caption', 'zoom-wrap', 'ui', 'top-bar'],
          timeToIdle: 4e3,
          timeToIdleOutside: 1e3,
          loadingIndicatorDelay: 1e3,
          addCaptionHTMLFn: function(Vs, Zs) {
            return Vs.title ? (Zs.children[0].innerHTML = Vs.title, !0) : (Zs.children[0].innerHTML = '', !1)
          },
          closeEl: !0,
          captionEl: !0,
          fullscreenEl: !0,
          zoomEl: !0,
          shareEl: !0,
          counterEl: !0,
          arrowEl: !0,
          preloaderEl: !0,
          tapToClose: !1,
          tapToToggleControls: !0,
          clickToCloseNonZoomable: !0,
          shareButtons: [{
            id: 'facebook',
            label: 'Share on Facebook',
            url: 'https://www.facebook.com/sharer/sharer.php?u={{url}}'
          }, {
            id: 'twitter',
            label: 'Tweet',
            url: 'https://twitter.com/intent/tweet?text={{text}}&url={{url}}'
          }, {
            id: 'pinterest',
            label: 'Pin it',
            url: 'http://www.pinterest.com/pin/create/button/?url={{url}}&media={{image_url}}&description={{text}}'
          }, {
            id: 'download',
            label: 'Download image',
            url: '{{raw_image_url}}',
            download: !0
          }],
          getImageURLForShare: function() {
            return t.currItem.src || ''
          },
          getPageURLForShare: function() {
            return window.location.href
          },
          getTextForShare: function() {
            return t.currItem.title || ''
          },
          indexIndicatorSep: ' / ',
          fitControlsWidth: 1200
        },
        k = function(Vs) {
          if (z) return !0;
          Vs = Vs || window.event, T.timeToIdle && T.mouseUsed && !f && Bs();
          for (var Qs, $s, Zs = Vs.target || Vs.srcElement, Us = Zs.getAttribute('class') || '', Ks = 0; Ks < qs.length; Ks++) Qs = qs[Ks], Qs.onTap && -1 < Us.indexOf('pswp__' + Qs.name) && (Qs.onTap(), $s = !0);
          if ($s) {
            Vs.stopPropagation && Vs.stopPropagation(), z = !0;
            var Js = a.features.isOldAndroid ? 600 : 30;
            setTimeout(function() {
              z = !1
            }, Js)
          }
        },
        _ = function() {
          return !t.likelyTouchDevice || T.mouseUsed || screen.width > T.fitControlsWidth
        },
        O = function(Vs, Zs, Us) {
          a[(Us ? 'add' : 'remove') + 'Class'](Vs, 'pswp__' + Zs)
        },
        q = function() {
          var Vs = 1 === T.getNumItemsFn();
          Vs !== w && (O(l, 'ui--one-slide', Vs), w = Vs)
        },
        j = function() {
          O(u, 'share-modal--hidden', g)
        },
        pt = function() {
          return g = !g, g ? (a.removeClass(u, 'pswp__share-modal--fade-in'), setTimeout(function() {
            g && j()
          }, 300)) : (j(), setTimeout(function() {
            g || a.addClass(u, 'pswp__share-modal--fade-in')
          }, 30)), g || ws(), !1
        },
        ua = function(Vs) {
          Vs = Vs || window.event;
          var Zs = Vs.target || Vs.srcElement;
          return (t.shout('shareLinkClick', Vs, Zs), !!Zs.href) && (!!Zs.hasAttribute('download') || (window.open(Zs.href, 'pswp_share', 'scrollbars=yes,resizable=yes,toolbar=no,location=yes,width=550,height=420,top=100,left=' + (window.screen ? Math.round(screen.width / 2 - 275) : 100)), g || pt(), !1))
        },
        ws = function() {
          for (var Us, Ks, Qs, $s, Js, Vs = '', Zs = 0; Zs < T.shareButtons.length; Zs++) Us = T.shareButtons[Zs], Qs = T.getImageURLForShare(Us), $s = T.getPageURLForShare(Us), Js = T.getTextForShare(Us), Ks = Us.url.replace('{{url}}', encodeURIComponent($s)).replace('{{image_url}}', encodeURIComponent(Qs)).replace('{{raw_image_url}}', Qs).replace('{{text}}', encodeURIComponent(Js)), Vs += '<a href="' + Ks + '" target="_blank" class="pswp__share--' + Us.id + '"' + (Us.download ? 'download' : '') + '>' + Us.label + '</a>', T.parseShareButtonOut && (Vs = T.parseShareButtonOut(Us, Vs));
          u.children[0].innerHTML = Vs, u.children[0].onclick = ua
        },
        _s = function(Vs) {
          for (var Zs = 0; Zs < T.closeElClasses.length; Zs++)
            if (a.hasClass(Vs, 'pswp__' + T.closeElClasses[Zs])) return !0
        },
        Ms = 0,
        Bs = function() {
          clearTimeout(Rs), Ms = 0, f && s.setIdle(!1)
        },
        Ws = function(Vs) {
          Vs = Vs ? Vs : window.event;
          var Zs = Vs.relatedTarget || Vs.toElement;
          Zs && 'HTML' !== Zs.nodeName || (clearTimeout(Rs), Rs = setTimeout(function() {
            s.setIdle(!0)
          }, T.timeToIdleOutside))
        },
        Hs = function() {
          T.fullscreenEl && !a.features.isOldAndroid && (!n && (n = s.getFullscreenAPI()), n ? (a.bind(document, n.eventK, s.updateFullscreen), s.updateFullscreen(), a.addClass(t.template, 'pswp--supports-fs')) : a.removeClass(t.template, 'pswp--supports-fs'))
        },
        Xs = function() {
          T.preloaderEl && (Fs(!0), y('beforeChange', function() {
            clearTimeout(b), b = setTimeout(function() {
              t.currItem && t.currItem.loading ? (!t.allowProgressiveImg() || t.currItem.img && !t.currItem.img.naturalWidth) && Fs(!1) : Fs(!0)
            }, T.loadingIndicatorDelay)
          }), y('imageLoadComplete', function(Vs, Zs) {
            t.currItem === Zs && Fs(!0)
          }))
        },
        Fs = function(Vs) {
          x !== Vs && (O(v, 'preloader--active', !Vs), x = Vs)
        },
        Ns = function(Vs) {
          var Zs = Vs.vGap;
          if (_()) {
            var Us = T.barsSize;
            if (!(T.captionEl && 'auto' === Us.bottom)) Zs.bottom = 'auto' === Us.bottom ? 0 : Us.bottom;
            else if (p || (p = a.createEl('pswp__caption pswp__caption--fake'), p.appendChild(a.createEl('pswp__caption__center')), l.insertBefore(p, d), a.addClass(l, 'pswp__ui--fit')), T.addCaptionHTMLFn(Vs, p, !0)) {
              var Ks = p.clientHeight;
              Zs.bottom = parseInt(Ks, 10) || 44
            } else Zs.bottom = Us.top;
            Zs.top = Us.top
          } else Zs.top = Zs.bottom = 0
        },
        Gs = function() {
          T.timeToIdle && y('mouseUsed', function() {
            a.bind(document, 'mousemove', Bs), a.bind(document, 'mouseout', Ws), Os = setInterval(function() {
              Ms++, 2 == Ms && s.setIdle(!0)
            }, T.timeToIdle / 2)
          })
        },
        Ys = function() {
          y('onVerticalDrag', function(Zs) {
            o && 0.95 > Zs ? s.hideControls() : !o && 0.95 <= Zs && s.showControls()
          });
          var Vs;
          y('onPinchClose', function(Zs) {
            o && 0.9 > Zs ? (s.hideControls(), Vs = !0) : Vs && !o && 0.9 < Zs && s.showControls()
          }), y('zoomGestureEnded', function() {
            Vs = !1, Vs && !o && s.showControls()
          })
        },
        qs = [{
          name: 'caption',
          option: 'captionEl',
          onInit: function(Vs) {
            d = Vs
          }
        }, {
          name: 'share-modal',
          option: 'shareEl',
          onInit: function(Vs) {
            u = Vs
          },
          onTap: function() {
            pt()
          }
        }, {
          name: 'button--share',
          option: 'shareEl',
          onInit: function(Vs) {
            c = Vs
          },
          onTap: function() {
            pt()
          }
        }, {
          name: 'button--zoom',
          option: 'zoomEl',
          onTap: t.toggleDesktopZoom
        }, {
          name: 'counter',
          option: 'counterEl',
          onInit: function(Vs) {
            m = Vs
          }
        }, {
          name: 'button--close',
          option: 'closeEl',
          onTap: t.close
        }, {
          name: 'button--arrow--left',
          option: 'arrowEl',
          onTap: t.prev
        }, {
          name: 'button--arrow--right',
          option: 'arrowEl',
          onTap: t.next
        }, {
          name: 'button--fs',
          option: 'fullscreenEl',
          onTap: function() {
            n.isFullscreen() ? n.exit() : n.enter()
          }
        }, {
          name: 'preloader',
          option: 'preloaderEl',
          onInit: function(Vs) {
            v = Vs
          }
        }],
        js = function() {
          var Vs, Zs, Us, Ks = function($s) {
            if ($s)
              for (var Js = $s.length, ei = 0; ei < Js; ei++) {
                Vs = $s[ei], Zs = Vs.className;
                for (var ti = 0; ti < qs.length; ti++) Us = qs[ti], -1 < Zs.indexOf('pswp__' + Us.name) && (T[Us.option] ? (a.removeClass(Vs, 'pswp__element--disabled'), Us.onInit && Us.onInit(Vs)) : a.addClass(Vs, 'pswp__element--disabled'))
              }
          };
          Ks(l.children);
          var Qs = a.getChildByClass(l, 'pswp__top-bar');
          Qs && Ks(Qs.children)
        };
      s.init = function() {
        a.extend(t.options, E, !0), T = t.options, l = a.getChildByClass(t.scrollWrap, 'pswp__ui'), y = t.listen, Ys(), y('beforeChange', s.update), y('doubleTap', function(Vs) {
          var Zs = t.currItem.initialZoomLevel;
          t.getZoomLevel() === Zs ? t.zoomTo(T.getDoubleTapZoom(!1, t.currItem), Vs, 333) : t.zoomTo(Zs, Vs, 333)
        }), y('preventDragEvent', function(Vs, Zs, Us) {
          var Ks = Vs.target || Vs.srcElement;
          Ks && Ks.getAttribute('class') && -1 < Vs.type.indexOf('mouse') && (0 < Ks.getAttribute('class').indexOf('__caption') || /(SMALL|STRONG|EM)/i.test(Ks.tagName)) && (Us.prevent = !1)
        }), y('bindEvents', function() {
          a.bind(l, 'pswpTap click', k), a.bind(t.scrollWrap, 'pswpTap', s.onGlobalTap), t.likelyTouchDevice || a.bind(t.scrollWrap, 'mouseover', s.onMouseOver)
        }), y('unbindEvents', function() {
          g || pt(), Os && clearInterval(Os), a.unbind(document, 'mouseout', Ws), a.unbind(document, 'mousemove', Bs), a.unbind(l, 'pswpTap click', k), a.unbind(t.scrollWrap, 'pswpTap', s.onGlobalTap), a.unbind(t.scrollWrap, 'mouseover', s.onMouseOver), n && (a.unbind(document, n.eventK, s.updateFullscreen), n.isFullscreen() && (T.hideAnimationDuration = 0, n.exit()), n = null)
        }), y('destroy', function() {
          T.captionEl && (p && l.removeChild(p), a.removeClass(d, 'pswp__caption--empty')), u && (u.children[0].onclick = null), a.removeClass(l, 'pswp__ui--over-close'), a.addClass(l, 'pswp__ui--hidden'), s.setIdle(!1)
        }), T.showAnimationDuration || a.removeClass(l, 'pswp__ui--hidden'), y('initialZoomIn', function() {
          T.showAnimationDuration && a.removeClass(l, 'pswp__ui--hidden')
        }), y('initialZoomOut', function() {
          a.addClass(l, 'pswp__ui--hidden')
        }), y('parseVerticalMargin', Ns), js(), T.shareEl && c && u && (g = !0), q(), Gs(), Hs(), Xs()
      }, s.setIdle = function(Vs) {
        f = Vs, O(l, 'ui--idle', Vs)
      }, s.update = function() {
        o && t.currItem ? (s.updateIndexIndicator(), T.captionEl && (T.addCaptionHTMLFn(t.currItem, d), O(d, 'caption--empty', !t.currItem.title)), r = !0) : r = !1, g || pt(), q()
      }, s.updateFullscreen = function(Vs) {
        Vs && setTimeout(function() {
          t.setScrollOffset(0, a.getScrollY())
        }, 50), a[(n.isFullscreen() ? 'add' : 'remove') + 'Class'](t.template, 'pswp--fs')
      }, s.updateIndexIndicator = function() {
        T.counterEl && (m.innerHTML = t.getCurrentIndex() + 1 + T.indexIndicatorSep + T.getNumItemsFn())
      }, s.onGlobalTap = function(Vs) {
        Vs = Vs || window.event;
        var Zs = Vs.target || Vs.srcElement;
        if (!z)
          if (Vs.detail && 'mouse' === Vs.detail.pointerType) {
            if (_s(Zs)) return void t.close();
            a.hasClass(Zs, 'pswp__img') && (1 === t.getZoomLevel() && t.getZoomLevel() <= t.currItem.fitRatio ? T.clickToCloseNonZoomable && t.close() : t.toggleDesktopZoom(Vs.detail.releasePoint))
          } else if (T.tapToToggleControls && (o ? s.hideControls() : s.showControls()), T.tapToClose && (a.hasClass(Zs, 'pswp__img') || _s(Zs))) return void t.close()
      }, s.onMouseOver = function(Vs) {
        Vs = Vs || window.event;
        var Zs = Vs.target || Vs.srcElement;
        O(l, 'ui--over-close', _s(Zs))
      }, s.hideControls = function() {
        a.addClass(l, 'pswp__ui--hidden'), o = !1
      }, s.showControls = function() {
        o = !0, r || s.update(), a.removeClass(l, 'pswp__ui--hidden')
      }, s.supportsFullscreen = function() {
        var Vs = document;
        return !!(Vs.exitFullscreen || Vs.mozCancelFullScreen || Vs.webkitExitFullscreen || Vs.msExitFullscreen)
      }, s.getFullscreenAPI = function() {
        var Us, Vs = document.documentElement,
          Zs = 'fullscreenchange';
        return Vs.requestFullscreen ? Us = {
          enterK: 'requestFullscreen',
          exitK: 'exitFullscreen',
          elementK: 'fullscreenElement',
          eventK: Zs
        } : Vs.mozRequestFullScreen ? Us = {
          enterK: 'mozRequestFullScreen',
          exitK: 'mozCancelFullScreen',
          elementK: 'mozFullScreenElement',
          eventK: 'moz' + Zs
        } : Vs.webkitRequestFullscreen ? Us = {
          enterK: 'webkitRequestFullscreen',
          exitK: 'webkitExitFullscreen',
          elementK: 'webkitFullscreenElement',
          eventK: 'webkit' + Zs
        } : Vs.msRequestFullscreen && (Us = {
          enterK: 'msRequestFullscreen',
          exitK: 'msExitFullscreen',
          elementK: 'msFullscreenElement',
          eventK: 'MSFullscreenChange'
        }), Us && (Us.enter = function() {
          return h = T.closeOnScroll, T.closeOnScroll = !1, 'webkitRequestFullscreen' === this.enterK ? void t.template[this.enterK](Element.ALLOW_KEYBOARD_INPUT) : t.template[this.enterK]()
        }, Us.exit = function() {
          return T.closeOnScroll = h, document[this.exitK]()
        }, Us.isFullscreen = function() {
          return document[this.elementK]
        }), Us
      }
    }
  }),
  function(t, a) {
    'function' == typeof define && define.amd ? define(a) : 'object' == typeof exports ? module.exports = a() : t.PhotoSwipe = a()
  }(this, function() {
    'use strict';
    return function(t, a, s, r) {
      var o = {
        features: null,
        bind: function(Bo, Wo, Ho, Xo) {
          var Fo = (Xo ? 'remove' : 'add') + 'EventListener';
          Wo = Wo.split(' ');
          for (var No = 0; No < Wo.length; No++) Wo[No] && Bo[Fo](Wo[No], Ho, !1)
        },
        isArray: function(Bo) {
          return Bo instanceof Array
        },
        createEl: function(Bo, Wo) {
          var Ho = document.createElement(Wo || 'div');
          return Bo && (Ho.className = Bo), Ho
        },
        getScrollY: function() {
          var Bo = window.pageYOffset;
          return void 0 === Bo ? document.documentElement.scrollTop : Bo
        },
        unbind: function(Bo, Wo, Ho) {
          o.bind(Bo, Wo, Ho, !0)
        },
        removeClass: function(Bo, Wo) {
          var Ho = new RegExp('(\\s|^)' + Wo + '(\\s|$)');
          Bo.className = Bo.className.replace(Ho, ' ').replace(/^\s\s*/, '').replace(/\s\s*$/, '')
        },
        addClass: function(Bo, Wo) {
          o.hasClass(Bo, Wo) || (Bo.className += (Bo.className ? ' ' : '') + Wo)
        },
        hasClass: function(Bo, Wo) {
          return Bo.className && new RegExp('(^|\\s)' + Wo + '(\\s|$)').test(Bo.className)
        },
        getChildByClass: function(Bo, Wo) {
          for (var Ho = Bo.firstChild; Ho;) {
            if (o.hasClass(Ho, Wo)) return Ho;
            Ho = Ho.nextSibling
          }
        },
        arraySearch: function(Bo, Wo, Ho) {
          for (var Xo = Bo.length; Xo--;)
            if (Bo[Xo][Ho] === Wo) return Xo;
          return -1
        },
        extend: function(Bo, Wo, Ho) {
          for (var Xo in Wo)
            if (Wo.hasOwnProperty(Xo)) {
              if (Ho && Bo.hasOwnProperty(Xo)) continue;
              Bo[Xo] = Wo[Xo]
            }
        },
        easing: {
          sine: {
            out: function(Bo) {
              return Math.sin(Bo * (Math.PI / 2))
            },
            inOut: function(Bo) {
              return -(Math.cos(Math.PI * Bo) - 1) / 2
            }
          },
          cubic: {
            out: function(Bo) {
              return --Bo * Bo * Bo + 1
            }
          }
        },
        detectFeatures: function() {
          if (o.features) return o.features;
          var Bo = o.createEl(),
            Wo = Bo.style,
            Ho = '',
            Xo = {};
          if (Xo.oldIE = document.all && !document.addEventListener, Xo.touch = 'ontouchstart' in window, window.requestAnimationFrame && (Xo.raf = window.requestAnimationFrame, Xo.caf = window.cancelAnimationFrame), Xo.pointerEvent = navigator.pointerEnabled || navigator.msPointerEnabled, !Xo.pointerEvent) {
            var Fo = navigator.userAgent;
            if (/iP(hone|od)/.test(navigator.platform)) {
              var No = navigator.appVersion.match(/OS (\d+)_(\d+)_?(\d+)?/);
              No && 0 < No.length && (No = parseInt(No[1], 10), 1 <= No && 8 > No && (Xo.isOldIOSPhone = !0))
            }
            var Go = Fo.match(/Android\s([0-9\.]*)/),
              Yo = Go ? Go[1] : 0;
            Yo = parseFloat(Yo), 1 <= Yo && (4.4 > Yo && (Xo.isOldAndroid = !0), Xo.androidVersion = Yo), Xo.isMobileOpera = /opera mini|opera mobi/i.test(Fo)
          }
          for (var Zo, Uo, qo = ['transform', 'perspective', 'animationName'], jo = ['', 'webkit', 'Moz', 'ms', 'O'], Vo = 0; 4 > Vo; Vo++) {
            Ho = jo[Vo];
            for (var Ko = 0; 3 > Ko; Ko++) Zo = qo[Ko], Uo = Ho + (Ho ? Zo.charAt(0).toUpperCase() + Zo.slice(1) : Zo), !Xo[Zo] && Uo in Wo && (Xo[Zo] = Uo);
            Ho && !Xo.raf && (Ho = Ho.toLowerCase(), Xo.raf = window[Ho + 'RequestAnimationFrame'], Xo.raf && (Xo.caf = window[Ho + 'CancelAnimationFrame'] || window[Ho + 'CancelRequestAnimationFrame']))
          }
          if (!Xo.raf) {
            var Qo = 0;
            Xo.raf = function($o) {
              var Jo = new Date().getTime(),
                en = Math.max(0, 16 - (Jo - Qo)),
                tn = window.setTimeout(function() {
                  $o(Jo + en)
                }, en);
              return Qo = Jo + en, tn
            }, Xo.caf = function($o) {
              clearTimeout($o)
            }
          }
          return Xo.svg = !!document.createElementNS && !!document.createElementNS('http://www.w3.org/2000/svg', 'svg').createSVGRect, o.features = Xo, Xo
        }
      };
      o.detectFeatures(), o.features.oldIE && (o.bind = function(Bo, Wo, Ho, Xo) {
        Wo = Wo.split(' ');
        for (var Yo, Fo = (Xo ? 'detach' : 'attach') + 'Event', No = function() {
            Ho.handleEvent.call(Ho)
          }, Go = 0; Go < Wo.length; Go++)
          if (Yo = Wo[Go], Yo)
            if ('object' == typeof Ho && Ho.handleEvent) {
              if (!Xo) Ho['oldIE' + Yo] = No;
              else if (!Ho['oldIE' + Yo]) return !1;
              Bo[Fo]('on' + Yo, Ho['oldIE' + Yo])
            } else Bo[Fo]('on' + Yo, Ho)
      });
      var n = this,
        l = 25,
        d = 3,
        p = {
          allowPanToNext: !0,
          spacing: 0.12,
          bgOpacity: 1,
          mouseUsed: !1,
          loop: !0,
          pinchToClose: !0,
          closeOnScroll: !0,
          closeOnVerticalDrag: !0,
          verticalDragRange: 0.75,
          hideAnimationDuration: 333,
          showAnimationDuration: 333,
          showHideOpacity: !1,
          focus: !0,
          escKey: !0,
          arrowKeys: !0,
          mainScrollEndFriction: 0.35,
          panEndFriction: 0.35,
          isClickableElement: function(Bo) {
            return 'A' === Bo.tagName
          },
          getDoubleTapZoom: function(Bo, Wo) {
            return Bo ? 1 : 0.7 > Wo.initialZoomLevel ? 1 : 1.33
          },
          maxSpreadZoom: 1.33,
          modal: !0,
          scaleMode: 'fit'
        };
      o.extend(p, r);
      var c, u, g, h, f, y, w, T, E, k, _, O, q, j, pt, Os, Rs, Bs, Ws, Hs, Xs, Fs, Ns, Ys, js, Vs, Zs, Us, Ks, Qs, $s, Mi, Bi, qi, Zi, Ui, Ki, er, tr, ar, ir, rr, or, nr, lr, dr, pr, mr, ur, gr, yr, vr, xr, br, wr, Wr, m = function() {
          return {
            x: 0,
            y: 0
          }
        },
        v = m(),
        x = m(),
        b = m(),
        z = {},
        ua = 0,
        ws = {},
        _s = m(),
        Ms = 0,
        Gs = !0,
        qs = [],
        Js = {},
        ei = !1,
        ti = function(Bo, Wo) {
          o.extend(n, Wo.publicMethods), qs.push(Bo)
        },
        ai = function(Bo) {
          var Wo = ro();
          return Bo > Wo - 1 ? Bo - Wo : 0 > Bo ? Wo + Bo : Bo
        },
        si = {},
        ii = function(Bo, Wo) {
          return si[Bo] || (si[Bo] = []), si[Bo].push(Wo)
        },
        ri = function(Bo) {
          var Wo = si[Bo];
          if (Wo) {
            var Ho = Array.prototype.slice.call(arguments);
            Ho.shift();
            for (var Xo = 0; Xo < Wo.length; Xo++) Wo[Xo].apply(n, Ho)
          }
        },
        oi = function() {
          return new Date().getTime()
        },
        ni = function(Bo) {
          br = Bo, n.bg.style.opacity = Bo * p.bgOpacity
        },
        li = function(Bo, Wo, Ho, Xo, Fo) {
          (!ei || Fo && Fo !== n.currItem) && (Xo /= Fo ? Fo.fitRatio : n.currItem.fitRatio), Bo[Fs] = O + Wo + 'px, ' + Ho + 'px' + q + ' scale(' + Xo + ')'
        },
        di = function(Bo) {
          ur && (Bo && (k > n.currItem.fitRatio ? !ei && (go(n.currItem, !1, !0), ei = !0) : ei && (go(n.currItem), ei = !1)), li(ur, b.x, b.y, k))
        },
        pi = function(Bo) {
          Bo.container && li(Bo.container.style, Bo.initialPosition.x, Bo.initialPosition.y, Bo.initialZoomLevel, Bo)
        },
        mi = function(Bo, Wo) {
          Wo[Fs] = O + Bo + 'px, 0px' + q
        },
        ci = function(Bo, Wo) {
          if (!p.loop && Wo) {
            var Ho = h + (_s.x * ua - Bo) / _s.x,
              Xo = Math.round(Bo - cr.x);
            (0 > Ho && 0 < Xo || Ho >= ro() - 1 && 0 > Xo) && (Bo = cr.x + Xo * p.mainScrollEndFriction)
          }
          cr.x = Bo, mi(Bo, f)
        },
        ui = function(Bo, Wo) {
          var Ho = hr[Bo] - ws[Bo];
          return x[Bo] + v[Bo] + Ho - Ho * (Wo / _)
        },
        gi = function(Bo, Wo) {
          Bo.x = Wo.x, Bo.y = Wo.y, Wo.id && (Bo.id = Wo.id)
        },
        hi = function(Bo) {
          Bo.x = Math.round(Bo.x), Bo.y = Math.round(Bo.y)
        },
        fi = null,
        yi = function() {
          fi && (o.unbind(document, 'mousemove', yi), o.addClass(t, 'pswp--has_mouse'), p.mouseUsed = !0, ri('mouseUsed')), fi = setTimeout(function() {
            fi = null
          }, 100)
        },
        vi = function() {
          o.bind(document, 'keydown', n), $s.transform && o.bind(n.scrollWrap, 'click', n), p.mouseUsed || o.bind(document, 'mousemove', yi), o.bind(window, 'resize scroll', n), ri('bindEvents')
        },
        xi = function() {
          o.unbind(window, 'resize', n), o.unbind(window, 'scroll', E.scroll), o.unbind(document, 'keydown', n), o.unbind(document, 'mousemove', yi), $s.transform && o.unbind(n.scrollWrap, 'click', n), er && o.unbind(window, w, n), ri('unbindEvents')
        },
        bi = function(Bo, Wo) {
          var Ho = po(n.currItem, z, Bo);
          return Wo && (mr = Ho), Ho
        },
        wi = function(Bo) {
          return Bo || (Bo = n.currItem), Bo.initialZoomLevel
        },
        Ci = function(Bo) {
          return Bo || (Bo = n.currItem), 0 < Bo.w ? p.maxSpreadZoom : 1
        },
        Ti = function(Bo, Wo, Ho, Xo) {
          return Xo === n.currItem.initialZoomLevel ? (Ho[Bo] = n.currItem.initialPosition[Bo], !0) : (Ho[Bo] = ui(Bo, Xo), Ho[Bo] > Wo.min[Bo]) ? (Ho[Bo] = Wo.min[Bo], !0) : !!(Ho[Bo] < Wo.max[Bo]) && (Ho[Bo] = Wo.max[Bo], !0)
        },
        Si = function() {
          if (Fs) {
            var Bo = $s.perspective && !Ys;
            return O = 'translate' + (Bo ? '3d(' : '('), void(q = $s.perspective ? ', 0px)' : ')')
          }
          Fs = 'left', o.addClass(t, 'pswp--ie'), mi = function(Wo, Ho) {
            Ho.left = Wo + 'px'
          }, pi = function(Wo) {
            var Ho = 1 < Wo.fitRatio ? 1 : Wo.fitRatio,
              Xo = Wo.container.style,
              Fo = Ho * Wo.w,
              No = Ho * Wo.h;
            Xo.width = Fo + 'px', Xo.height = No + 'px', Xo.left = Wo.initialPosition.x + 'px', Xo.top = Wo.initialPosition.y + 'px'
          }, di = function() {
            if (ur) {
              var Wo = ur,
                Ho = n.currItem,
                Xo = 1 < Ho.fitRatio ? 1 : Ho.fitRatio,
                Fo = Xo * Ho.w,
                No = Xo * Ho.h;
              Wo.width = Fo + 'px', Wo.height = No + 'px', Wo.left = b.x + 'px', Wo.top = b.y + 'px'
            }
          }
        },
        Ei = function(Bo) {
          var Wo = '';
          p.escKey && 27 === Bo.keyCode ? Wo = 'close' : p.arrowKeys && (37 === Bo.keyCode ? Wo = 'prev' : 39 === Bo.keyCode && (Wo = 'next')), !Wo || Bo.ctrlKey || Bo.altKey || Bo.shiftKey || Bo.metaKey || (Bo.preventDefault ? Bo.preventDefault() : Bo.returnValue = !1, n[Wo]())
        },
        zi = function(Bo) {
          !Bo || (ir || ar || gr || Ui) && (Bo.preventDefault(), Bo.stopPropagation())
        },
        Ii = function() {
          n.setScrollOffset(0, o.getScrollY())
        },
        ki = {},
        Pi = 0,
        Li = function(Bo) {
          ki[Bo] && (ki[Bo].raf && Vs(ki[Bo].raf), Pi--, delete ki[Bo])
        },
        Di = function(Bo) {
          ki[Bo] && Li(Bo), ki[Bo] || (Pi++, ki[Bo] = {})
        },
        Ai = function() {
          for (var Bo in ki) ki.hasOwnProperty(Bo) && Li(Bo)
        },
        _i = function(Bo, Wo, Ho, Xo, Fo, No, Go) {
          var qo, Yo = oi();
          Di(Bo);
          var jo = function() {
            if (ki[Bo]) {
              if (qo = oi() - Yo, qo >= Xo) return Li(Bo), No(Ho), void(Go && Go());
              No((Ho - Wo) * Fo(qo / Xo) + Wo), ki[Bo].raf = js(jo)
            }
          };
          jo()
        },
        Oi = 30,
        Ri = 10,
        Wi = {},
        Hi = {},
        Xi = {},
        Fi = {},
        Ni = {},
        Gi = [],
        Yi = {},
        ji = [],
        Vi = {},
        Qi = 0,
        $i = m(),
        Ji = 0,
        cr = m(),
        hr = m(),
        fr = m(),
        Cr = function(Bo, Wo) {
          return Bo.x === Wo.x && Bo.y === Wo.y
        },
        Tr = function(Bo, Wo) {
          return Math.abs(Bo.x - Wo.x) < l && Math.abs(Bo.y - Wo.y) < l
        },
        Sr = function(Bo, Wo) {
          return Vi.x = Math.abs(Bo.x - Wo.x), Vi.y = Math.abs(Bo.y - Wo.y), Math.sqrt(Vi.x * Vi.x + Vi.y * Vi.y)
        },
        Er = function() {
          rr && (Vs(rr), rr = null)
        },
        zr = function() {
          er && (rr = js(zr), Gr())
        },
        Ir = function() {
          return 'fit' !== p.scaleMode || k !== n.currItem.initialZoomLevel
        },
        kr = function(Bo, Wo) {
          return Bo && Bo !== document && (Bo.getAttribute('class') && -1 < Bo.getAttribute('class').indexOf('pswp__scroll-wrap') ? !1 : Wo(Bo) ? Bo : kr(Bo.parentNode, Wo))
        },
        Pr = {},
        Lr = function(Bo, Wo) {
          return Pr.prevent = !kr(Bo.target, p.isClickableElement), ri('preventDragEvent', Bo, Wo, Pr), Pr.prevent
        },
        Dr = function(Bo, Wo) {
          return Wo.x = Bo.pageX, Wo.y = Bo.pageY, Wo.id = Bo.identifier, Wo
        },
        Ar = function(Bo, Wo, Ho) {
          Ho.x = 0.5 * (Bo.x + Wo.x), Ho.y = 0.5 * (Bo.y + Wo.y)
        },
        _r = function(Bo, Wo, Ho) {
          if (50 < Bo - Bi) {
            var Xo = 2 < ji.length ? ji.shift() : {};
            Xo.x = Wo, Xo.y = Ho, ji.push(Xo), Bi = Bo
          }
        },
        Or = function() {
          var Bo = b.y - n.currItem.initialPosition.y;
          return 1 - Math.abs(Bo / (z.y / 2))
        },
        Rr = {},
        Mr = {},
        Br = [],
        Hr = function(Bo) {
          for (; 0 < Br.length;) Br.pop();
          return Ns ? (Wr = 0, Gi.forEach(function(Wo) {
            0 === Wr ? Br[0] = Wo : 1 == Wr && (Br[1] = Wo), Wr++
          })) : -1 < Bo.type.indexOf('touch') ? Bo.touches && 0 < Bo.touches.length && (Br[0] = Dr(Bo.touches[0], Rr), 1 < Bo.touches.length && (Br[1] = Dr(Bo.touches[1], Mr))) : (Rr.x = Bo.pageX, Rr.y = Bo.pageY, Rr.id = '', Br[0] = Rr), Br
        },
        Xr = function(Bo, Wo) {
          var Yo, qo, jo, Vo, Ho = 0,
            Xo = b[Bo] + Wo[Bo],
            Fo = 0 < Wo[Bo],
            No = cr.x + Wo.x,
            Go = cr.x - Yi.x;
          return Yo = Xo > mr.min[Bo] || Xo < mr.max[Bo] ? p.panEndFriction : 1, Xo = b[Bo] + Wo[Bo] * Yo, (p.allowPanToNext || k === n.currItem.initialZoomLevel) && (ur ? 'h' === yr && 'x' === Bo && !ar && (Fo ? (Xo > mr.min[Bo] && (Yo = p.panEndFriction, Ho = mr.min[Bo] - Xo, qo = mr.min[Bo] - x[Bo]), (0 >= qo || 0 > Go) && 1 < ro() ? (Vo = No, 0 > Go && No > Yi.x && (Vo = Yi.x)) : mr.min.x !== mr.max.x && (jo = Xo)) : (Xo < mr.max[Bo] && (Yo = p.panEndFriction, Ho = Xo - mr.max[Bo], qo = x[Bo] - mr.max[Bo]), (0 >= qo || 0 < Go) && 1 < ro() ? (Vo = No, 0 < Go && No < Yi.x && (Vo = Yi.x)) : mr.min.x !== mr.max.x && (jo = Xo))) : Vo = No, 'x' === Bo) ? (void 0 != Vo && (ci(Vo, !0), or = Vo !== Yi.x), mr.min.x !== mr.max.x && (void 0 == jo ? !or && (b.x += Wo.x * Yo) : b.x = jo), void 0 != Vo) : void(!gr && !or && k > n.currItem.fitRatio && (b[Bo] += Wo[Bo] * Yo))
        },
        Fr = function(Bo) {
          if (!('mousedown' === Bo.type && 0 < Bo.button)) {
            if (ao) return void Bo.preventDefault();
            if (!(Ki && 'mousedown' === Bo.type)) {
              if (Lr(Bo, !0) && Bo.preventDefault(), ri('pointerDown'), Ns) {
                var Wo = o.arraySearch(Gi, Bo.pointerId, 'id');
                0 > Wo && (Wo = Gi.length), Gi[Wo] = {
                  x: Bo.pageX,
                  y: Bo.pageY,
                  id: Bo.pointerId
                }
              }
              var Ho = Hr(Bo),
                Xo = Ho.length;
              nr = null, Ai(), er && 1 !== Xo || (er = vr = !0, o.bind(window, w, n), Zi = wr = xr = Ui = or = ir = tr = ar = !1, yr = null, ri('firstTouchStart', Ho), gi(x, b), v.x = v.y = 0, gi(Fi, Ho[0]), gi(Ni, Fi), Yi.x = _s.x * ua, ji = [{
                x: Fi.x,
                y: Fi.y
              }], Bi = Mi = oi(), bi(k, !0), Er(), zr()), lr || !(1 < Xo) || gr || or || (_ = k, ar = !1, lr = tr = !0, v.y = v.x = 0, gi(x, b), gi(Wi, Ho[0]), gi(Hi, Ho[1]), Ar(Wi, Hi, fr), hr.x = Math.abs(fr.x) - b.x, hr.y = Math.abs(fr.y) - b.y, dr = pr = Sr(Wi, Hi))
            }
          }
        },
        Nr = function(Bo) {
          if (Bo.preventDefault(), Ns) {
            var Wo = o.arraySearch(Gi, Bo.pointerId, 'id');
            if (-1 < Wo) {
              var Ho = Gi[Wo];
              Ho.x = Bo.pageX, Ho.y = Bo.pageY
            }
          }
          if (er) {
            var Xo = Hr(Bo);
            if (yr || ir || lr) nr = Xo;
            else if (cr.x !== _s.x * ua) yr = 'h';
            else {
              var Fo = Math.abs(Xo[0].x - Fi.x) - Math.abs(Xo[0].y - Fi.y);
              Math.abs(Fo) >= Ri && (yr = 0 < Fo ? 'h' : 'v', nr = Xo)
            }
          }
        },
        Gr = function() {
          if (nr) {
            var Bo = nr.length;
            if (0 !== Bo)
              if (gi(Wi, nr[0]), Xi.x = Wi.x - Fi.x, Xi.y = Wi.y - Fi.y, lr && 1 < Bo) {
                if (Fi.x = Wi.x, Fi.y = Wi.y, !Xi.x && !Xi.y && Cr(nr[1], Hi)) return;
                gi(Hi, nr[1]), ar || (ar = !0, ri('zoomGestureStarted'));
                var Wo = Sr(Wi, Hi),
                  Ho = Zr(Wo);
                Ho > n.currItem.initialZoomLevel + n.currItem.initialZoomLevel / 15 && (wr = !0);
                var Xo = 1,
                  Fo = wi(),
                  No = Ci();
                if (!(Ho < Fo)) Ho > No && (Xo = (Ho - No) / (6 * Fo), 1 < Xo && (Xo = 1), Ho = No + Xo * Fo);
                else if (p.pinchToClose && !wr && _ <= n.currItem.initialZoomLevel) {
                  var Go = Fo - Ho,
                    Yo = 1 - Go / (Fo / 1.2);
                  ni(Yo), ri('onPinchClose', Yo), xr = !0
                } else Xo = (Fo - Ho) / Fo, 1 < Xo && (Xo = 1), Ho = Fo - Xo * (Fo / 3);
                0 > Xo && (Xo = 0), dr = Wo, Ar(Wi, Hi, $i), v.x += $i.x - fr.x, v.y += $i.y - fr.y, gi(fr, $i), b.x = ui('x', Ho), b.y = ui('y', Ho), Zi = Ho > k, k = Ho, di()
              } else {
                if (!yr) return;
                if (vr && (vr = !1, Math.abs(Xi.x) >= Ri && (Xi.x -= nr[0].x - Ni.x), Math.abs(Xi.y) >= Ri && (Xi.y -= nr[0].y - Ni.y)), Fi.x = Wi.x, Fi.y = Wi.y, 0 === Xi.x && 0 === Xi.y) return;
                if ('v' === yr && p.closeOnVerticalDrag && !Ir()) {
                  v.y += Xi.y, b.y += Xi.y;
                  var qo = Or();
                  return Ui = !0, ri('onVerticalDrag', qo), ni(qo), void di()
                }
                _r(oi(), Wi.x, Wi.y), ir = !0, mr = n.currItem.bounds;
                var jo = Xr('x', Xi);
                jo || (Xr('y', Xi), hi(b), di())
              }
          }
        },
        Yr = function(Bo) {
          if ($s.isOldAndroid) {
            if (Ki && 'mouseup' === Bo.type) return; - 1 < Bo.type.indexOf('touch') && (clearTimeout(Ki), Ki = setTimeout(function() {
              Ki = 0
            }, 600))
          }
          ri('pointerUp'), Lr(Bo, !1) && Bo.preventDefault();
          var Wo;
          if (Ns) {
            var Ho = o.arraySearch(Gi, Bo.pointerId, 'id'); - 1 < Ho && ((Wo = Gi.splice(Ho, 1)[0], navigator.pointerEnabled) ? Wo.type = Bo.pointerType || 'mouse' : (Wo.type = {
              2: 'touch',
              3: 'pen',
              4: 'mouse'
            }[Bo.pointerType], Wo.type || (Wo.type = Bo.pointerType || 'mouse')))
          }
          var No, Xo = Hr(Bo),
            Fo = Xo.length;
          if ('mouseup' === Bo.type && (Fo = 0), 2 === Fo) return nr = null, !0;
          1 === Fo && gi(Ni, Xo[0]), 0 !== Fo || yr || gr || (!Wo && ('mouseup' === Bo.type ? Wo = {
            x: Bo.pageX,
            y: Bo.pageY,
            type: 'mouse'
          } : Bo.changedTouches && Bo.changedTouches[0] && (Wo = {
            x: Bo.changedTouches[0].pageX,
            y: Bo.changedTouches[0].pageY,
            type: 'touch'
          })), ri('touchRelease', Bo, Wo));
          var Go = -1;
          if (0 === Fo && (er = !1, o.unbind(window, w, n), Er(), lr ? Go = 0 : -1 != Ji && (Go = oi() - Ji)), Ji = 1 === Fo ? oi() : -1, No = -1 != Go && 150 > Go ? 'zoom' : 'swipe', lr && 2 > Fo && (lr = !1, 1 === Fo && (No = 'zoomPointerUp'), ri('zoomGestureEnded')), nr = null, ir || ar || gr || Ui) {
            if (Ai(), qi || (qi = qr()), qi.calculateSwipeSpeed('x'), Ui) {
              var Yo = Or();
              if (Yo < p.verticalDragRange) n.close();
              else {
                var qo = b.y,
                  jo = br;
                _i('verticalDrag', 0, 1, 300, o.easing.cubic.out, function(Zo) {
                  b.y = (n.currItem.initialPosition.y - qo) * Zo + qo, ni((1 - jo) * Zo + jo), di()
                }), ri('onVerticalDrag', 1)
              }
              return
            }
            if ((or || gr) && 0 === Fo) {
              var Vo = Vr(No, qi);
              if (Vo) return;
              No = 'zoomPointerUp'
            }
            return gr ? void 0 : 'swipe' === No ? void(!or && k > n.currItem.fitRatio && jr(qi)) : void Ur()
          }
        },
        qr = function() {
          var Bo, Wo, Ho = {
            lastFlickOffset: {},
            lastFlickDist: {},
            lastFlickSpeed: {},
            slowDownRatio: {},
            slowDownRatioReverse: {},
            speedDecelerationRatio: {},
            speedDecelerationRatioAbs: {},
            distanceOffset: {},
            backAnimDestination: {},
            backAnimStarted: {},
            calculateSwipeSpeed: function(Xo) {
              1 < ji.length ? (Bo = oi() - Bi + 50, Wo = ji[ji.length - 2][Xo]) : (Bo = oi() - Mi, Wo = Ni[Xo]), Ho.lastFlickOffset[Xo] = Fi[Xo] - Wo, Ho.lastFlickDist[Xo] = Math.abs(Ho.lastFlickOffset[Xo]), Ho.lastFlickSpeed[Xo] = 20 < Ho.lastFlickDist[Xo] ? Ho.lastFlickOffset[Xo] / Bo : 0, 0.1 > Math.abs(Ho.lastFlickSpeed[Xo]) && (Ho.lastFlickSpeed[Xo] = 0), Ho.slowDownRatio[Xo] = 0.95, Ho.slowDownRatioReverse[Xo] = 1 - Ho.slowDownRatio[Xo], Ho.speedDecelerationRatio[Xo] = 1
            },
            calculateOverBoundsAnimOffset: function(Xo, Fo) {
              Ho.backAnimStarted[Xo] || (b[Xo] > mr.min[Xo] ? Ho.backAnimDestination[Xo] = mr.min[Xo] : b[Xo] < mr.max[Xo] && (Ho.backAnimDestination[Xo] = mr.max[Xo]), void 0 !== Ho.backAnimDestination[Xo] && (Ho.slowDownRatio[Xo] = 0.7, Ho.slowDownRatioReverse[Xo] = 1 - Ho.slowDownRatio[Xo], 0.05 > Ho.speedDecelerationRatioAbs[Xo] && (Ho.lastFlickSpeed[Xo] = 0, Ho.backAnimStarted[Xo] = !0, _i('bounceZoomPan' + Xo, b[Xo], Ho.backAnimDestination[Xo], Fo || 300, o.easing.sine.out, function(No) {
                b[Xo] = No, di()
              }))))
            },
            calculateAnimOffset: function(Xo) {
              Ho.backAnimStarted[Xo] || (Ho.speedDecelerationRatio[Xo] *= Ho.slowDownRatio[Xo] + Ho.slowDownRatioReverse[Xo] - Ho.slowDownRatioReverse[Xo] * Ho.timeDiff / 10, Ho.speedDecelerationRatioAbs[Xo] = Math.abs(Ho.lastFlickSpeed[Xo] * Ho.speedDecelerationRatio[Xo]), Ho.distanceOffset[Xo] = Ho.lastFlickSpeed[Xo] * Ho.speedDecelerationRatio[Xo] * Ho.timeDiff, b[Xo] += Ho.distanceOffset[Xo])
            },
            panAnimLoop: function() {
              if (ki.zoomPan && (ki.zoomPan.raf = js(Ho.panAnimLoop), Ho.now = oi(), Ho.timeDiff = Ho.now - Ho.lastNow, Ho.lastNow = Ho.now, Ho.calculateAnimOffset('x'), Ho.calculateAnimOffset('y'), di(), Ho.calculateOverBoundsAnimOffset('x'), Ho.calculateOverBoundsAnimOffset('y'), 0.05 > Ho.speedDecelerationRatioAbs.x && 0.05 > Ho.speedDecelerationRatioAbs.y)) return b.x = Math.round(b.x), b.y = Math.round(b.y), di(), void Li('zoomPan')
            }
          };
          return Ho
        },
        jr = function(Bo) {
          return Bo.calculateSwipeSpeed('y'), mr = n.currItem.bounds, Bo.backAnimDestination = {}, Bo.backAnimStarted = {}, 0.05 >= Math.abs(Bo.lastFlickSpeed.x) && 0.05 >= Math.abs(Bo.lastFlickSpeed.y) ? (Bo.speedDecelerationRatioAbs.x = Bo.speedDecelerationRatioAbs.y = 0, Bo.calculateOverBoundsAnimOffset('x'), Bo.calculateOverBoundsAnimOffset('y'), !0) : void(Di('zoomPan'), Bo.lastNow = oi(), Bo.panAnimLoop())
        },
        Vr = function(Bo, Wo) {
          var Ho;
          gr || (Qi = h);
          var Xo;
          if ('swipe' === Bo) {
            var Fo = Fi.x - Ni.x,
              No = 10 > Wo.lastFlickDist.x;
            Fo > Oi && (No || 20 < Wo.lastFlickOffset.x) ? Xo = -1 : Fo < -Oi && (No || -20 > Wo.lastFlickOffset.x) && (Xo = 1)
          }
          var Go;
          Xo && (h += Xo, 0 > h ? (h = p.loop ? ro() - 1 : 0, Go = !0) : h >= ro() && (h = p.loop ? 0 : ro() - 1, Go = !0), (!Go || p.loop) && (Ms += Xo, ua -= Xo, Ho = !0));
          var jo, Yo = _s.x * ua,
            qo = Math.abs(Yo - cr.x);
          return Ho || Yo > cr.x == 0 < Wo.lastFlickSpeed.x ? (jo = 0 < Math.abs(Wo.lastFlickSpeed.x) ? qo / Math.abs(Wo.lastFlickSpeed.x) : 333, jo = Math.min(jo, 400), jo = Math.max(jo, 250)) : jo = 333, Qi === h && (Ho = !1), gr = !0, ri('mainScrollAnimStart'), _i('mainScroll', cr.x, Yo, jo, o.easing.cubic.out, ci, function() {
            Ai(), gr = !1, Qi = -1, (Ho || Qi !== h) && n.updateCurrItem(), ri('mainScrollAnimComplete')
          }), Ho && n.updateCurrItem(!0), Ho
        },
        Zr = function(Bo) {
          return 1 / pr * Bo * _
        },
        Ur = function() {
          var Bo = k,
            Wo = wi(),
            Ho = Ci();
          k < Wo ? Bo = Wo : k > Ho && (Bo = Ho);
          var Fo, Xo = br;
          return xr && !Zi && !wr && k < Wo ? (n.close(), !0) : (xr && (Fo = function(No) {
            ni((1 - Xo) * No + Xo)
          }), n.zoomTo(Bo, 0, 200, o.easing.cubic.out, Fo), !0)
        };
      ti('Gestures', {
        publicMethods: {
          initGestures: function() {
            var Bo = function(Wo, Ho, Xo, Fo, No) {
              Bs = Wo + Ho, Ws = Wo + Xo, Hs = Wo + Fo, Xs = No ? Wo + No : ''
            };
            Ns = $s.pointerEvent, Ns && $s.touch && ($s.touch = !1), Ns ? navigator.pointerEnabled ? Bo('pointer', 'down', 'move', 'up', 'cancel') : Bo('MSPointer', 'Down', 'Move', 'Up', 'Cancel') : $s.touch ? (Bo('touch', 'start', 'move', 'end', 'cancel'), Ys = !0) : Bo('mouse', 'down', 'move', 'up'), w = Ws + ' ' + Hs + ' ' + Xs, T = Bs, Ns && !Ys && (Ys = 1 < navigator.maxTouchPoints || 1 < navigator.msMaxTouchPoints), n.likelyTouchDevice = Ys, E[Bs] = Fr, E[Ws] = Nr, E[Hs] = Yr, Xs && (E[Xs] = E[Hs]), $s.touch && (T += ' mousedown', w += ' mousemove mouseup', E.mousedown = E[Bs], E.mousemove = E[Ws], E.mouseup = E[Hs]), Ys || (p.allowPanToNext = !1)
          }
        }
      });
      var Qr, $r, to, ao, io, ro, oo, Kr = function(Bo, Wo, Ho, Xo) {
          Qr && clearTimeout(Qr), ao = !0, to = !0;
          var Fo;
          Bo.initialLayout ? (Fo = Bo.initialLayout, Bo.initialLayout = null) : Fo = p.getThumbBoundsFn && p.getThumbBoundsFn(h);
          var No = Ho ? p.hideAnimationDuration : p.showAnimationDuration,
            Go = function() {
              Li('initialZoom'), Ho ? (n.template.removeAttribute('style'), n.bg.removeAttribute('style')) : (ni(1), Wo && (Wo.style.display = 'block'), o.addClass(t, 'pswp--animated-in'), ri('initialZoom' + (Ho ? 'OutEnd' : 'InEnd'))), Xo && Xo(), ao = !1
            };
          return No && Fo && void 0 !== Fo.x ? void
          function() {
            var Yo = g,
              qo = !n.currItem.src || n.currItem.loadError || p.showHideOpacity;
            Bo.miniImg && (Bo.miniImg.style.webkitBackfaceVisibility = 'hidden'), Ho || (k = Fo.w / Bo.w, b.x = Fo.x, b.y = Fo.y - Us, n[qo ? 'template' : 'bg'].style.opacity = 1e-3, di()), Di('initialZoom'), Ho && !Yo && o.removeClass(t, 'pswp--animated-in'), qo && (Ho ? o[(Yo ? 'remove' : 'add') + 'Class'](t, 'pswp--animate_opacity') : setTimeout(function() {
              o.addClass(t, 'pswp--animate_opacity')
            }, 30)), Qr = setTimeout(function() {
              if (ri('initialZoom' + (Ho ? 'Out' : 'In')), !Ho) k = Bo.initialZoomLevel, gi(b, Bo.initialPosition), di(), ni(1), qo ? t.style.opacity = 1 : ni(1), Qr = setTimeout(Go, No + 20);
              else {
                var jo = Fo.w / Bo.w,
                  Vo = {
                    x: b.x,
                    y: b.y
                  },
                  Zo = k,
                  Uo = br,
                  Ko = function(Qo) {
                    1 === Qo ? (k = jo, b.x = Fo.x, b.y = Fo.y - Qs) : (k = (jo - Zo) * Qo + Zo, b.x = (Fo.x - Vo.x) * Qo + Vo.x, b.y = (Fo.y - Qs - Vo.y) * Qo + Vo.y), di(), qo ? t.style.opacity = 1 - Qo : ni(Uo - Qo * Uo)
                  };
                Yo ? _i('initialZoom', 0, 1, No, o.easing.cubic.out, Ko, Go) : (Ko(1), Qr = setTimeout(Go, No + 20))
              }
            }, Ho ? 25 : 90)
          }(): (ri('initialZoom' + (Ho ? 'Out' : 'In')), k = Bo.initialZoomLevel, gi(b, Bo.initialPosition), di(), t.style.opacity = Ho ? 0 : 1, ni(1), void(No ? setTimeout(function() {
            Go()
          }, No) : Go()))
        },
        Jr = {},
        eo = [],
        so = {
          index: 0,
          errorMsg: '<div class="pswp__error-msg"><a href="%url%" target="_blank">The image</a> could not be loaded.</div>',
          forceProgressiveLoading: !1,
          preload: [1, 1],
          getNumItemsFn: function() {
            return $r.length
          }
        },
        no = function() {
          return {
            center: {
              x: 0,
              y: 0
            },
            max: {
              x: 0,
              y: 0
            },
            min: {
              x: 0,
              y: 0
            }
          }
        },
        lo = function(Bo, Wo, Ho) {
          var Xo = Bo.bounds;
          Xo.center.x = Math.round((Jr.x - Wo) / 2), Xo.center.y = Math.round((Jr.y - Ho) / 2) + Bo.vGap.top, Xo.max.x = Wo > Jr.x ? Math.round(Jr.x - Wo) : Xo.center.x, Xo.max.y = Ho > Jr.y ? Math.round(Jr.y - Ho) + Bo.vGap.top : Xo.center.y, Xo.min.x = Wo > Jr.x ? 0 : Xo.center.x, Xo.min.y = Ho > Jr.y ? Bo.vGap.top : Xo.center.y
        },
        po = function(Bo, Wo, Ho) {
          if (Bo.src && !Bo.loadError) {
            var Xo = !Ho;
            if (Xo && (!Bo.vGap && (Bo.vGap = {
                top: 0,
                bottom: 0
              }), ri('parseVerticalMargin', Bo)), Jr.x = Wo.x, Jr.y = Wo.y - Bo.vGap.top - Bo.vGap.bottom, Xo) {
              var Fo = Jr.x / Bo.w,
                No = Jr.y / Bo.h;
              Bo.fitRatio = Fo < No ? Fo : No;
              var Go = p.scaleMode;
              'orig' === Go ? Ho = 1 : 'fit' === Go && (Ho = Bo.fitRatio), 1 < Ho && (Ho = 1), Bo.initialZoomLevel = Ho, Bo.bounds || (Bo.bounds = no())
            }
            return Ho ? (lo(Bo, Bo.w * Ho, Bo.h * Ho), Xo && Ho === Bo.initialZoomLevel && (Bo.initialPosition = Bo.bounds.center), Bo.bounds) : void 0
          }
          return Bo.w = Bo.h = 0, Bo.initialZoomLevel = Bo.fitRatio = 1, Bo.bounds = no(), Bo.initialPosition = Bo.bounds.center, Bo.bounds
        },
        mo = function(Bo, Wo, Ho, Xo, Fo, No) {
          Wo.loadError || Xo && (Wo.imageAppended = !0, go(Wo, Xo, Wo === n.currItem && ei), Ho.appendChild(Xo), No && setTimeout(function() {
            Wo && Wo.loaded && Wo.placeholder && (Wo.placeholder.style.display = 'none', Wo.placeholder = null)
          }, 500))
        },
        co = function(Bo) {
          Bo.loading = !0, Bo.loaded = !1;
          var Wo = Bo.img = o.createEl('pswp__img', 'img'),
            Ho = function() {
              Bo.loading = !1, Bo.loaded = !0, Bo.loadComplete ? Bo.loadComplete(Bo) : Bo.img = null, Wo.onload = Wo.onerror = null, Wo = null
            };
          return Wo.onload = Ho, Wo.onerror = function() {
            Bo.loadError = !0, Ho()
          }, Wo.src = Bo.src, Wo
        },
        uo = function(Bo, Wo) {
          if (Bo.src && Bo.loadError && Bo.container) return Wo && (Bo.container.innerHTML = ''), Bo.container.innerHTML = p.errorMsg.replace('%url%', Bo.src), !0
        },
        go = function(Bo, Wo, Ho) {
          if (Bo.src) {
            Wo || (Wo = Bo.container.lastChild);
            var Xo = Ho ? Bo.w : Math.round(Bo.w * Bo.fitRatio),
              Fo = Ho ? Bo.h : Math.round(Bo.h * Bo.fitRatio);
            Bo.placeholder && !Bo.loaded && (Bo.placeholder.style.width = Xo + 'px', Bo.placeholder.style.height = Fo + 'px'), Wo.style.width = Xo + 'px', Wo.style.height = Fo + 'px'
          }
        },
        ho = function() {
          if (eo.length) {
            for (var Wo, Bo = 0; Bo < eo.length; Bo++) Wo = eo[Bo], Wo.holder.index === Wo.index && mo(Wo.index, Wo.item, Wo.baseDiv, Wo.img, !1, Wo.clearPlaceholder);
            eo = []
          }
        };
      ti('Controller', {
        publicMethods: {
          lazyLoadItem: function(Bo) {
            Bo = ai(Bo);
            var Wo = io(Bo);
            Wo && (!Wo.loaded && !Wo.loading || pt) && (ri('gettingData', Bo, Wo), Wo.src && co(Wo))
          },
          initController: function() {
            o.extend(p, so, !0), n.items = $r = s, io = n.getItemAt, ro = p.getNumItemsFn, oo = p.loop, 3 > ro() && (p.loop = !1), ii('beforeChange', function(Bo) {
              var No, Wo = p.preload,
                Ho = null === Bo || 0 <= Bo,
                Xo = Math.min(Wo[0], ro()),
                Fo = Math.min(Wo[1], ro());
              for (No = 1; No <= (Ho ? Fo : Xo); No++) n.lazyLoadItem(h + No);
              for (No = 1; No <= (Ho ? Xo : Fo); No++) n.lazyLoadItem(h - No)
            }), ii('initialLayout', function() {
              n.currItem.initialLayout = p.getThumbBoundsFn && p.getThumbBoundsFn(h)
            }), ii('mainScrollAnimComplete', ho), ii('initialZoomInEnd', ho), ii('destroy', function() {
              for (var Wo, Bo = 0; Bo < $r.length; Bo++) Wo = $r[Bo], Wo.container && (Wo.container = null), Wo.placeholder && (Wo.placeholder = null), Wo.img && (Wo.img = null), Wo.preloader && (Wo.preloader = null), Wo.loadError && (Wo.loaded = Wo.loadError = !1);
              eo = null
            })
          },
          getItemAt: function(Bo) {
            return !!(0 <= Bo) && void 0 !== $r[Bo] && $r[Bo]
          },
          allowProgressiveImg: function() {
            return p.forceProgressiveLoading || !Ys || p.mouseUsed || 1200 < screen.width
          },
          setContent: function(Bo, Wo) {
            p.loop && (Wo = ai(Wo));
            var Ho = n.getItemAt(Bo.index);
            Ho && (Ho.container = null);
            var Fo, Xo = n.getItemAt(Wo);
            if (!Xo) return void(Bo.el.innerHTML = '');
            ri('gettingData', Wo, Xo), Bo.index = Wo, Bo.item = Xo;
            var No = Xo.container = o.createEl('pswp__zoom-wrap');
            if (!Xo.src && Xo.html && (Xo.html.tagName ? No.appendChild(Xo.html) : No.innerHTML = Xo.html), uo(Xo), po(Xo, z), Xo.src && !Xo.loadError && !Xo.loaded) {
              if (Xo.loadComplete = function(qo) {
                  if (c) {
                    if (Bo && Bo.index === Wo) {
                      if (uo(qo, !0)) return qo.loadComplete = qo.img = null, po(qo, z), pi(qo), void(Bo.index === h && n.updateCurrZoomItem());
                      qo.imageAppended ? !ao && qo.placeholder && (qo.placeholder.style.display = 'none', qo.placeholder = null) : $s.transform && (gr || ao) ? eo.push({
                        item: qo,
                        baseDiv: No,
                        img: qo.img,
                        index: Wo,
                        holder: Bo,
                        clearPlaceholder: !0
                      }) : mo(Wo, qo, No, qo.img, gr || ao, !0)
                    }
                    qo.loadComplete = null, qo.img = null, ri('imageLoadComplete', Wo, qo)
                  }
                }, o.features.transform) {
                var Go = 'pswp__img pswp__img--placeholder';
                Go += Xo.msrc ? '' : ' pswp__img--placeholder--blank';
                var Yo = o.createEl(Go, Xo.msrc ? 'img' : '');
                Xo.msrc && (Yo.src = Xo.msrc), go(Xo, Yo), No.appendChild(Yo), Xo.placeholder = Yo
              }
              Xo.loading || co(Xo), n.allowProgressiveImg() && (!to && $s.transform ? eo.push({
                item: Xo,
                baseDiv: No,
                img: Xo.img,
                index: Wo,
                holder: Bo
              }) : mo(Wo, Xo, No, Xo.img, !0, !0))
            } else Xo.src && !Xo.loadError && (Fo = o.createEl('pswp__img', 'img'), Fo.style.opacity = 1, Fo.src = Xo.src, go(Xo, Fo), mo(Wo, Xo, No, Fo, !0));
            to || Wo !== h ? pi(Xo) : (ur = No.style, Kr(Xo, Fo || Xo.img)), Bo.el.innerHTML = '', Bo.el.appendChild(No)
          },
          cleanSlide: function(Bo) {
            Bo.img && (Bo.img.onload = Bo.img.onerror = null), Bo.loaded = Bo.loading = Bo.img = Bo.imageAppended = !1
          }
        }
      });
      var vo, fo = {},
        yo = function(Bo, Wo, Ho) {
          var Xo = document.createEvent('CustomEvent'),
            Fo = {
              origEvent: Bo,
              target: Bo.target,
              releasePoint: Wo,
              pointerType: Ho || 'touch'
            };
          Xo.initCustomEvent('pswpTap', !0, !0, Fo), Bo.target.dispatchEvent(Xo)
        };
      ti('Tap', {
        publicMethods: {
          initTap: function() {
            ii('firstTouchStart', n.onTapStart), ii('touchRelease', n.onTapRelease), ii('destroy', function() {
              fo = {}, vo = null
            })
          },
          onTapStart: function(Bo) {
            1 < Bo.length && (clearTimeout(vo), vo = null)
          },
          onTapRelease: function(Bo, Wo) {
            if (Wo && !ir && !tr && !Pi) {
              var Ho = Wo;
              if (vo && (clearTimeout(vo), vo = null, Tr(Ho, fo))) return void ri('doubleTap', Ho);
              if ('mouse' === Wo.type) return void yo(Bo, Wo, 'mouse');
              var Xo = Bo.target.tagName.toUpperCase();
              if ('BUTTON' === Xo || o.hasClass(Bo.target, 'pswp__single-tap')) return void yo(Bo, Wo);
              gi(fo, Ho), vo = setTimeout(function() {
                yo(Bo, Wo), vo = null
              }, 300)
            }
          }
        }
      });
      var xo;
      ti('DesktopZoom', {
        publicMethods: {
          initDesktopZoom: function() {
            Ks || (Ys ? ii('mouseUsed', function() {
              n.setupDesktopZoom()
            }) : n.setupDesktopZoom(!0))
          },
          setupDesktopZoom: function(Bo) {
            xo = {};
            var Wo = 'wheel mousewheel DOMMouseScroll';
            ii('bindEvents', function() {
              o.bind(t, Wo, n.handleMouseWheel)
            }), ii('unbindEvents', function() {
              xo && o.unbind(t, Wo, n.handleMouseWheel)
            }), n.mouseZoomedIn = !1;
            var Fo, Ho = function() {
                n.mouseZoomedIn && (o.removeClass(t, 'pswp--zoomed-in'), n.mouseZoomedIn = !1), 1 > k ? o.addClass(t, 'pswp--zoom-allowed') : o.removeClass(t, 'pswp--zoom-allowed'), Xo()
              },
              Xo = function() {
                Fo && (o.removeClass(t, 'pswp--dragging'), Fo = !1)
              };
            ii('resize', Ho), ii('afterChange', Ho), ii('pointerDown', function() {
              n.mouseZoomedIn && (Fo = !0, o.addClass(t, 'pswp--dragging'))
            }), ii('pointerUp', Xo), Bo || Ho()
          },
          handleMouseWheel: function(Bo) {
            if (k <= n.currItem.fitRatio) return p.modal && (!p.closeOnScroll || Pi || er ? Bo.preventDefault() : Fs && 2 < Math.abs(Bo.deltaY) && (g = !0, n.close())), !0;
            if (Bo.stopPropagation(), xo.x = 0, 'deltaX' in Bo) 1 === Bo.deltaMode ? (xo.x = 18 * Bo.deltaX, xo.y = 18 * Bo.deltaY) : (xo.x = Bo.deltaX, xo.y = Bo.deltaY);
            else if ('wheelDelta' in Bo) Bo.wheelDeltaX && (xo.x = -0.16 * Bo.wheelDeltaX), xo.y = Bo.wheelDeltaY ? -0.16 * Bo.wheelDeltaY : -0.16 * Bo.wheelDelta;
            else if ('detail' in Bo) xo.y = Bo.detail;
            else return;
            bi(k, !0);
            var Wo = b.x - xo.x,
              Ho = b.y - xo.y;
            (p.modal || Wo <= mr.min.x && Wo >= mr.max.x && Ho <= mr.min.y && Ho >= mr.max.y) && Bo.preventDefault(), n.panTo(Wo, Ho)
          },
          toggleDesktopZoom: function(Bo) {
            Bo = Bo || {
              x: z.x / 2 + ws.x,
              y: z.y / 2 + ws.y
            };
            var Wo = p.getDoubleTapZoom(!0, n.currItem),
              Ho = k === Wo;
            n.mouseZoomedIn = !Ho, n.zoomTo(Ho ? n.currItem.initialZoomLevel : Wo, Bo, 333), o[(Ho ? 'remove' : 'add') + 'Class'](t, 'pswp--zoomed-in')
          }
        }
      });
      var wo, Co, To, So, Eo, zo, Io, ko, Po, Lo, Do, Ao, bo = {
          history: !0,
          galleryUID: 1
        },
        _o = function() {
          return Do.hash.substring(1)
        },
        Oo = function() {
          wo && clearTimeout(wo), To && clearTimeout(To)
        },
        Ro = function() {
          var Bo = _o(),
            Wo = {};
          if (5 > Bo.length) return Wo;
          var Xo, Ho = Bo.split('&');
          for (Xo = 0; Xo < Ho.length; Xo++)
            if (Ho[Xo]) {
              var Fo = Ho[Xo].split('=');
              2 > Fo.length || (Wo[Fo[0]] = Fo[1])
            }
          if (p.galleryPIDs) {
            var No = Wo.pid;
            for (Wo.pid = 0, Xo = 0; Xo < $r.length; Xo++)
              if ($r[Xo].pid === No) {
                Wo.pid = Xo;
                break
              }
          } else Wo.pid = parseInt(Wo.pid, 10) - 1;
          return 0 > Wo.pid && (Wo.pid = 0), Wo
        },
        Mo = function() {
          if (To && clearTimeout(To), Pi || er) return void(To = setTimeout(Mo, 500));
          So ? clearTimeout(Co) : So = !0;
          var Bo = h + 1,
            Wo = io(h);
          Wo.hasOwnProperty('pid') && (Bo = Wo.pid);
          var Ho = Io + '&gid=' + p.galleryUID + '&pid=' + Bo;
          ko || -1 !== Do.hash.indexOf(Ho) || (Lo = !0);
          var Xo = Do.href.split('#')[0] + '#' + Ho;
          Ao ? '#' + Ho !== window.location.hash && history[ko ? 'replaceState' : 'pushState']('', document.title, Xo) : ko ? Do.replace(Xo) : Do.hash = Ho, ko = !0, Co = setTimeout(function() {
            So = !1
          }, 60)
        };
      ti('History', {
        publicMethods: {
          initHistory: function() {
            if (o.extend(p, bo, !0), !!p.history) {
              Do = window.location, Lo = !1, Po = !1, ko = !1, Io = _o(), Ao = 'pushState' in history, -1 < Io.indexOf('gid=') && (Io = Io.split('&gid=')[0], Io = Io.split('?gid=')[0]), ii('afterChange', n.updateURL), ii('unbindEvents', function() {
                o.unbind(window, 'hashchange', n.onHashChange)
              });
              var Bo = function() {
                zo = !0, Po || (Lo ? history.back() : Io ? Do.hash = Io : Ao ? history.pushState('', document.title, Do.pathname + Do.search) : Do.hash = ''), Oo()
              };
              ii('unbindEvents', function() {
                g && Bo()
              }), ii('destroy', function() {
                zo || Bo()
              }), ii('firstUpdate', function() {
                h = Ro().pid
              });
              var Wo = Io.indexOf('pid='); - 1 < Wo && (Io = Io.substring(0, Wo), '&' === Io.slice(-1) && (Io = Io.slice(0, -1))), setTimeout(function() {
                c && o.bind(window, 'hashchange', n.onHashChange)
              }, 40)
            }
          },
          onHashChange: function() {
            return _o() === Io ? (Po = !0, void n.close()) : void(!So && (Eo = !0, n.goTo(Ro().pid), Eo = !1))
          },
          updateURL: function() {
            Oo(), Eo || (ko ? wo = setTimeout(Mo, 800) : Mo())
          }
        }
      }), o.extend(n, {
        shout: ri,
        listen: ii,
        viewportSize: z,
        options: p,
        isMainScrollAnimating: function() {
          return gr
        },
        getZoomLevel: function() {
          return k
        },
        getCurrentIndex: function() {
          return h
        },
        isDragging: function() {
          return er
        },
        isZooming: function() {
          return lr
        },
        setScrollOffset: function(Bo, Wo) {
          ws.x = Bo, Qs = ws.y = Wo, ri('updateScrollOffset', ws)
        },
        applyZoomPan: function(Bo, Wo, Ho, Xo) {
          b.x = Wo, b.y = Ho, k = Bo, di(Xo)
        },
        init: function() {
          if (!(c || u)) {
            var Bo;
            n.framework = o, n.template = t, n.bg = o.getChildByClass(t, 'pswp__bg'), Zs = t.className, c = !0, $s = o.detectFeatures(), js = $s.raf, Vs = $s.caf, Fs = $s.transform, Ks = $s.oldIE, n.scrollWrap = o.getChildByClass(t, 'pswp__scroll-wrap'), n.container = o.getChildByClass(n.scrollWrap, 'pswp__container'), f = n.container.style, n.itemHolders = Os = [{
              el: n.container.children[0],
              wrap: 0,
              index: -1
            }, {
              el: n.container.children[1],
              wrap: 0,
              index: -1
            }, {
              el: n.container.children[2],
              wrap: 0,
              index: -1
            }], Os[0].el.style.display = Os[2].el.style.display = 'none', Si(), E = {
              resize: n.updateSize,
              scroll: Ii,
              keydown: Ei,
              click: zi
            };
            var Wo = $s.isOldIOSPhone || $s.isOldAndroid || $s.isMobileOpera;
            for ($s.animationName && $s.transform && !Wo || (p.showAnimationDuration = p.hideAnimationDuration = 0), Bo = 0; Bo < qs.length; Bo++) n['init' + qs[Bo]]();
            if (a) {
              var Ho = n.ui = new a(n, o);
              Ho.init()
            }
            ri('firstUpdate'), h = h || p.index || 0, (isNaN(h) || 0 > h || h >= ro()) && (h = 0), n.currItem = io(h), ($s.isOldIOSPhone || $s.isOldAndroid) && (Gs = !1), t.setAttribute('aria-hidden', 'false'), p.modal && (Gs ? t.style.position = 'fixed' : (t.style.position = 'absolute', t.style.top = o.getScrollY() + 'px')), void 0 === Qs && (ri('initialLayout'), Qs = Us = o.getScrollY());
            var Xo = 'pswp--open ';
            for (p.mainClass && (Xo += p.mainClass + ' '), p.showHideOpacity && (Xo += 'pswp--animate_opacity '), Xo += Ys ? 'pswp--touch' : 'pswp--notouch', Xo += $s.animationName ? ' pswp--css_animation' : '', Xo += $s.svg ? ' pswp--svg' : '', o.addClass(t, Xo), n.updateSize(), y = -1, Ms = null, Bo = 0; Bo < d; Bo++) mi((Bo + y) * _s.x, Os[Bo].el.style);
            Ks || o.bind(n.scrollWrap, T, n), ii('initialZoomInEnd', function() {
              n.setContent(Os[0], h - 1), n.setContent(Os[2], h + 1), Os[0].el.style.display = Os[2].el.style.display = 'block', p.focus && t.focus(), vi()
            }), n.setContent(Os[1], h), n.updateCurrItem(), ri('afterInit'), Gs || (j = setInterval(function() {
              Pi || er || lr || k !== n.currItem.initialZoomLevel || n.updateSize()
            }, 1e3)), o.addClass(t, 'pswp--visible')
          }
        },
        close: function() {
          c && (c = !1, u = !0, ri('close'), xi(), Kr(n.currItem, null, !0, n.destroy))
        },
        destroy: function() {
          ri('destroy'), Qr && clearTimeout(Qr), t.setAttribute('aria-hidden', 'true'), t.className = Zs, j && clearInterval(j), o.unbind(n.scrollWrap, T, n), o.unbind(window, 'scroll', n), Er(), Ai(), si = null
        },
        panTo: function(Bo, Wo, Ho) {
          Ho || (Bo > mr.min.x ? Bo = mr.min.x : Bo < mr.max.x && (Bo = mr.max.x), Wo > mr.min.y ? Wo = mr.min.y : Wo < mr.max.y && (Wo = mr.max.y)), b.x = Bo, b.y = Wo, di()
        },
        handleEvent: function(Bo) {
          Bo = Bo || window.event, E[Bo.type] && E[Bo.type](Bo)
        },
        goTo: function(Bo) {
          Bo = ai(Bo);
          var Wo = Bo - h;
          Ms = Wo, h = Bo, n.currItem = io(h), ua -= Wo, ci(_s.x * ua), Ai(), gr = !1, n.updateCurrItem()
        },
        next: function() {
          n.goTo(h + 1)
        },
        prev: function() {
          n.goTo(h - 1)
        },
        updateCurrZoomItem: function(Bo) {
          if (Bo && ri('beforeChange', 0), Os[1].el.children.length) {
            var Wo = Os[1].el.children[0];
            ur = o.hasClass(Wo, 'pswp__zoom-wrap') ? Wo.style : null
          } else ur = null;
          mr = n.currItem.bounds, _ = k = n.currItem.initialZoomLevel, b.x = mr.center.x, b.y = mr.center.y, Bo && ri('afterChange')
        },
        invalidateCurrItems: function() {
          pt = !0;
          for (var Bo = 0; Bo < d; Bo++) Os[Bo].item && (Os[Bo].item.needsUpdate = !0)
        },
        updateCurrItem: function(Bo) {
          if (0 !== Ms) {
            var Ho, Wo = Math.abs(Ms);
            if (!(Bo && 2 > Wo)) {
              n.currItem = io(h), ei = !1, ri('beforeChange', Ms), Wo >= d && (y += Ms + (0 < Ms ? -d : d), Wo = d);
              for (var Xo = 0; Xo < Wo; Xo++) 0 < Ms ? (Ho = Os.shift(), Os[d - 1] = Ho, y++, mi((y + 2) * _s.x, Ho.el.style), n.setContent(Ho, h - Wo + Xo + 1 + 1)) : (Ho = Os.pop(), Os.unshift(Ho), y--, mi(y * _s.x, Ho.el.style), n.setContent(Ho, h + Wo - Xo - 1 - 1));
              if (ur && 1 === Math.abs(Ms)) {
                var Fo = io(Rs);
                Fo.initialZoomLevel !== k && (po(Fo, z), go(Fo), pi(Fo))
              }
              Ms = 0, n.updateCurrZoomItem(), Rs = h, ri('afterChange')
            }
          }
        },
        updateSize: function(Bo) {
          if (!Gs && p.modal) {
            var Wo = o.getScrollY();
            if (Qs !== Wo && (t.style.top = Wo + 'px', Qs = Wo), !Bo && Js.x === window.innerWidth && Js.y === window.innerHeight) return;
            Js.x = window.innerWidth, Js.y = window.innerHeight, t.style.height = Js.y + 'px'
          }
          if (z.x = n.scrollWrap.clientWidth, z.y = n.scrollWrap.clientHeight, Ii(), _s.x = z.x + Math.round(z.x * p.spacing), _s.y = z.y, ci(_s.x * ua), ri('beforeResize'), void 0 !== y) {
            for (var Xo, Fo, No, Ho = 0; Ho < d; Ho++) Xo = Os[Ho], mi((Ho + y) * _s.x, Xo.el.style), No = h + Ho - 1, p.loop && 2 < ro() && (No = ai(No)), Fo = io(No), Fo && (pt || Fo.needsUpdate || !Fo.bounds) ? (n.cleanSlide(Fo), n.setContent(Xo, No), 1 === Ho && (n.currItem = Fo, n.updateCurrZoomItem(!0)), Fo.needsUpdate = !1) : -1 === Xo.index && 0 <= No && n.setContent(Xo, No), Fo && Fo.container && (po(Fo, z), go(Fo), pi(Fo));
            pt = !1
          }
          _ = k = n.currItem.initialZoomLevel, mr = n.currItem.bounds, mr && (b.x = mr.center.x, b.y = mr.center.y, di(!0)), ri('resize')
        },
        zoomTo: function(Bo, Wo, Ho, Xo, Fo) {
          Wo && (_ = k, hr.x = Math.abs(Wo.x) - b.x, hr.y = Math.abs(Wo.y) - b.y, gi(x, b));
          var No = bi(Bo, !1),
            Go = {};
          Ti('x', No, Go, Bo), Ti('y', No, Go, Bo);
          var Yo = k,
            qo = {
              x: b.x,
              y: b.y
            };
          hi(Go);
          var jo = function(Vo) {
            1 === Vo ? (k = Bo, b.x = Go.x, b.y = Go.y) : (k = (Bo - Yo) * Vo + Yo, b.x = (Go.x - qo.x) * Vo + qo.x, b.y = (Go.y - qo.y) * Vo + qo.y), Fo && Fo(Vo), di(1 === Vo)
          };
          Ho ? _i('customZoomTo', 0, 1, Ho, Xo || o.easing.sine.inOut, jo) : jo(1)
        }
      })
    }
  }), ! function() {
    'use strict';

    function t(q) {
      return 'undefined' == typeof this || Object.getPrototypeOf(this) !== t.prototype ? new t(q) : (_ = this, _.version = '3.3.4', _.tools = new k, _.isSupported() ? (_.tools.extend(_.defaults, q || {}), _.defaults.container = a(_.defaults), _.store = {
        elements: {},
        containers: []
      }, _.sequences = {}, _.history = [], _.uid = 0, _.initialized = !1) : 'undefined' != typeof console && null !== console, _)
    }

    function a(q) {
      if (q && q.container) {
        if ('string' == typeof q.container) return window.document.documentElement.querySelector(q.container);
        if (_.tools.isNode(q.container)) return q.container
      }
      return _.defaults.container
    }

    function s(q, j) {
      return 'string' == typeof q ? Array.prototype.slice.call(j.querySelectorAll(q)) : _.tools.isNode(q) ? [q] : _.tools.isNodeList(q) ? Array.prototype.slice.call(q) : []
    }

    function r() {
      return ++_.uid
    }

    function o(q, j, pt) {
      j.container && (j.container = pt), q.config = q.config ? _.tools.extendClone(q.config, j) : _.tools.extendClone(_.defaults, j), q.config.axis = 'top' === q.config.origin || 'bottom' === q.config.origin ? 'Y' : 'X'
    }

    function n(q) {
      var j = window.getComputedStyle(q.domEl);
      q.styles || (q.styles = {
        transition: {},
        transform: {},
        computed: {}
      }, q.styles.inline = q.domEl.getAttribute('style') || '', q.styles.inline += '; visibility: visible; ', q.styles.computed.opacity = j.opacity, q.styles.computed.transition = j.transition && 'all 0s ease 0s' !== j.transition ? j.transition + ', ' : ''), q.styles.transition.instant = l(q, 0), q.styles.transition.delayed = l(q, q.config.delay), q.styles.transform.initial = ' -webkit-transform:', q.styles.transform.target = ' -webkit-transform:', d(q), q.styles.transform.initial += 'transform:', q.styles.transform.target += 'transform:', d(q)
    }

    function l(q, j) {
      var pt = q.config;
      return '-webkit-transition: ' + q.styles.computed.transition + '-webkit-transform ' + pt.duration / 1e3 + 's ' + pt.easing + ' ' + j / 1e3 + 's, opacity ' + pt.duration / 1e3 + 's ' + pt.easing + ' ' + j / 1e3 + 's; transition: ' + q.styles.computed.transition + 'transform ' + pt.duration / 1e3 + 's ' + pt.easing + ' ' + j / 1e3 + 's, opacity ' + pt.duration / 1e3 + 's ' + pt.easing + ' ' + j / 1e3 + 's; '
    }

    function d(q) {
      var ua, j = q.config,
        pt = q.styles.transform;
      ua = 'top' === j.origin || 'left' === j.origin ? /^-/.test(j.distance) ? j.distance.substr(1) : '-' + j.distance : j.distance, parseInt(j.distance) && (pt.initial += ' translate' + j.axis + '(' + ua + ')', pt.target += ' translate' + j.axis + '(0)'), j.scale && (pt.initial += ' scale(' + j.scale + ')', pt.target += ' scale(1)'), j.rotate.x && (pt.initial += ' rotateX(' + j.rotate.x + 'deg)', pt.target += ' rotateX(0)'), j.rotate.y && (pt.initial += ' rotateY(' + j.rotate.y + 'deg)', pt.target += ' rotateY(0)'), j.rotate.z && (pt.initial += ' rotateZ(' + j.rotate.z + 'deg)', pt.target += ' rotateZ(0)'), pt.initial += '; opacity: ' + j.opacity + ';', pt.target += '; opacity: ' + q.styles.computed.opacity + ';'
    }

    function p(q) {
      var j = q.config.container;
      j && -1 === _.store.containers.indexOf(j) && _.store.containers.push(q.config.container), _.store.elements[q.id] = q
    }

    function m(q, j, pt) {
      _.history.push({
        target: q,
        config: j,
        interval: pt
      })
    }

    function c() {
      if (_.isSupported()) {
        h();
        for (var q = 0; q < _.store.containers.length; q++) _.store.containers[q].addEventListener('scroll', u), _.store.containers[q].addEventListener('resize', u);
        _.initialized || (window.addEventListener('scroll', u), window.addEventListener('resize', u), _.initialized = !0)
      }
      return _
    }

    function u() {
      O(h)
    }

    function g() {
      var q, j, pt, ua;
      _.tools.forOwn(_.sequences, function(ws) {
        ua = _.sequences[ws], q = !1;
        for (var _s = 0; _s < ua.elemIds.length; _s++) pt = ua.elemIds[_s], j = _.store.elements[pt], z(j) && !q && (q = !0);
        ua.active = q
      })
    }

    function h() {
      var q, j;
      g(), _.tools.forOwn(_.store.elements, function(pt) {
        j = _.store.elements[pt], q = x(j), v(j) ? (j.config.beforeReveal(j.domEl), q ? j.domEl.setAttribute('style', j.styles.inline + j.styles.transform.target + j.styles.transition.delayed) : j.domEl.setAttribute('style', j.styles.inline + j.styles.transform.target + j.styles.transition.instant), y('reveal', j, q), j.revealing = !0, j.seen = !0, j.sequence && f(j, q)) : b(j) && (j.config.beforeReset(j.domEl), j.domEl.setAttribute('style', j.styles.inline + j.styles.transform.initial + j.styles.transition.instant), y('reset', j), j.revealing = !1)
      })
    }

    function f(q, j) {
      var pt = 0,
        ua = 0,
        ws = _.sequences[q.sequence.id];
      ws.blocked = !0, j && 'onload' === q.config.useDelay && (ua = q.config.delay), q.sequence.timer && (pt = Math.abs(q.sequence.timer.started - new Date), window.clearTimeout(q.sequence.timer)), q.sequence.timer = {
        started: new Date
      }, q.sequence.timer.clock = window.setTimeout(function() {
        ws.blocked = !1, q.sequence.timer = null, u()
      }, Math.abs(ws.interval) + ua - pt)
    }

    function y(q, j, pt) {
      var ua = 0,
        ws = 0,
        _s = 'after';
      'reveal' === q ? (ws = j.config.duration, pt && (ws += j.config.delay), _s += 'Reveal') : 'reset' === q ? (ws = j.config.duration, _s += 'Reset') : void 0, j.timer && (ua = Math.abs(j.timer.started - new Date), window.clearTimeout(j.timer.clock)), j.timer = {
        started: new Date
      }, j.timer.clock = window.setTimeout(function() {
        j.config[_s](j.domEl), j.timer = null
      }, ws - ua)
    }

    function v(q) {
      if (q.sequence) {
        var j = _.sequences[q.sequence.id];
        return j.active && !j.blocked && !q.revealing && !q.disabled
      }
      return z(q) && !q.revealing && !q.disabled
    }

    function x(q) {
      var j = q.config.useDelay;
      return 'always' === j || 'onload' === j && !_.initialized || 'once' === j && !q.seen
    }

    function b(q) {
      if (q.sequence) {
        var j = _.sequences[q.sequence.id];
        return !j.active && q.config.reset && q.revealing && !q.disabled
      }
      return !z(q) && q.config.reset && q.revealing && !q.disabled
    }

    function w(q) {
      return {
        width: q.clientWidth,
        height: q.clientHeight
      }
    }

    function T(q) {
      if (q && q !== window.document.documentElement) {
        var j = E(q);
        return {
          x: q.scrollLeft + j.left,
          y: q.scrollTop + j.top
        }
      }
      return {
        x: window.pageXOffset,
        y: window.pageYOffset
      }
    }

    function E(q) {
      var j = 0,
        pt = 0,
        ua = q.offsetHeight,
        ws = q.offsetWidth;
      do isNaN(q.offsetTop) || (j += q.offsetTop), isNaN(q.offsetLeft) || (pt += q.offsetLeft), q = q.offsetParent; while (q);
      return {
        top: j,
        left: pt,
        height: ua,
        width: ws
      }
    }

    function z(q) {
      var j = E(q.domEl),
        pt = w(q.config.container),
        ua = T(q.config.container),
        ws = q.config.viewFactor,
        _s = j.height,
        Os = j.width,
        Rs = j.top,
        Ms = j.left;
      return function() {
        var Bs = ua.y + q.config.viewOffset.top,
          Ws = ua.x + q.config.viewOffset.left,
          Hs = ua.y - q.config.viewOffset.bottom + pt.height,
          Xs = ua.x - q.config.viewOffset.right + pt.width;
        return Rs + _s * ws < Hs && Rs + _s - _s * ws > Bs && Ms + Os * ws > Ws && Ms + Os - Os * ws < Xs
      }() || function() {
        return 'fixed' === window.getComputedStyle(q.domEl).position
      }()
    }

    function k() {}
    var _, O;
    t.prototype.defaults = {
      origin: 'bottom',
      distance: '20px',
      duration: 500,
      delay: 0,
      rotate: {
        x: 0,
        y: 0,
        z: 0
      },
      opacity: 0,
      scale: .9,
      easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
      container: window.document.documentElement,
      mobile: !0,
      reset: !1,
      useDelay: 'always',
      viewFactor: .2,
      viewOffset: {
        top: 0,
        right: 0,
        bottom: 0,
        left: 0
      },
      beforeReveal: function() {},
      beforeReset: function() {},
      afterReveal: function() {},
      afterReset: function() {}
    }, t.prototype.isSupported = function() {
      var q = document.documentElement.style;
      return 'WebkitTransition' in q && 'WebkitTransform' in q || 'transition' in q && 'transform' in q
    }, t.prototype.reveal = function(q, j, pt, ua) {
      var ws, _s, Os, Rs, Ms, Bs;
      if (void 0 !== j && 'number' == typeof j ? (pt = j, j = {}) : void 0 !== j && null !== j || (j = {}), ws = a(j), _s = s(q, ws), !_s.length) return _;
      pt && 'number' == typeof pt && (Bs = r(), Ms = _.sequences[Bs] = {
        id: Bs,
        interval: pt,
        elemIds: [],
        active: !1
      });
      for (var Ws = 0; Ws < _s.length; Ws++) Rs = _s[Ws].getAttribute('data-sr-id'), Rs ? Os = _.store.elements[Rs] : (Os = {
        id: r(),
        domEl: _s[Ws],
        seen: !1,
        revealing: !1
      }, Os.domEl.setAttribute('data-sr-id', Os.id)), Ms && (Os.sequence = {
        id: Ms.id,
        index: Ms.elemIds.length
      }, Ms.elemIds.push(Os.id)), o(Os, j, ws), n(Os), p(Os), (!_.tools.isMobile() || Os.config.mobile) && _.isSupported() ? Os.revealing || Os.domEl.setAttribute('style', Os.styles.inline + Os.styles.transform.initial) : (Os.domEl.setAttribute('style', Os.styles.inline), Os.disabled = !0);
      return !ua && _.isSupported() && (m(q, j, pt), _.initTimeout && window.clearTimeout(_.initTimeout), _.initTimeout = window.setTimeout(c, 0)), _
    }, t.prototype.sync = function() {
      if (_.history.length && _.isSupported()) {
        for (var j, q = 0; q < _.history.length; q++) j = _.history[q], _.reveal(j.target, j.config, j.interval, !0);
        c()
      }
      return _
    }, k.prototype.isObject = function(q) {
      return null !== q && 'object' == typeof q && q.constructor === Object
    }, k.prototype.isNode = function(q) {
      return 'object' == typeof window.Node ? q instanceof window.Node : q && 'object' == typeof q && 'number' == typeof q.nodeType && 'string' == typeof q.nodeName
    }, k.prototype.isNodeList = function(q) {
      var j = Object.prototype.toString.call(q),
        pt = /^\[object (HTMLCollection|NodeList|Object)\]$/;
      return 'object' == typeof window.NodeList ? q instanceof window.NodeList : q && 'object' == typeof q && pt.test(j) && 'number' == typeof q.length && (0 === q.length || this.isNode(q[0]))
    }, k.prototype.forOwn = function(q, j) {
      if (!this.isObject(q)) throw new TypeError('Expected "object", but received "' + typeof q + '".');
      for (var pt in q) q.hasOwnProperty(pt) && j(pt)
    }, k.prototype.extend = function(q, j) {
      return this.forOwn(j, function(pt) {
        this.isObject(j[pt]) ? (q[pt] && this.isObject(q[pt]) || (q[pt] = {}), this.extend(q[pt], j[pt])) : q[pt] = j[pt]
      }.bind(this)), q
    }, k.prototype.extendClone = function(q, j) {
      return this.extend(this.extend({}, q), j)
    }, k.prototype.isMobile = function() {
      return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)
    }, O = window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || function(q) {
      window.setTimeout(q, 1e3 / 60)
    }, 'function' == typeof define && 'object' == typeof define.amd && define.amd ? define(function() {
      return t
    }) : 'undefined' != typeof module && module.exports ? module.exports = t : window.ScrollReveal = t
  }(),
  function() {
    var t, a;
    t = this.jQuery || window.jQuery, a = t(window), t.fn.stick_in_parent = function(s) {
      var r, o, n, l, d, p, m, c, u, g, h, f;
      for (null == s && (s = {}), f = s.sticky_class, p = s.inner_scrolling, h = s.recalc_every, g = s.parent, u = s.offset_top, c = s.spacer, n = s.bottoming, null == u && (u = 0), null == g && (g = void 0), null == p && (p = !0), null == f && (f = 'is_stuck'), r = t(document), null == n && (n = !0), l = function(y, v, x, b, w, T, E, z) {
          var k, _, O, q, j, pt, ua, ws, _s, Os, Rs, Ms;
          if (!y.data('sticky_kit')) {
            if (y.data('sticky_kit', !0), j = r.height(), ua = y.parent(), null != g && (ua = ua.closest(g)), !ua.length) throw 'failed to find stick parent';
            return (O = !1, k = !1, Rs = null == c ? t('<div />') : c && y.closest(c), Rs && Rs.css('position', y.css('position')), ws = function() {
              var Bs, Ws, Hs;
              if (!z && (j = r.height(), Bs = parseInt(ua.css('border-top-width'), 10), Ws = parseInt(ua.css('padding-top'), 10), v = parseInt(ua.css('padding-bottom'), 10), x = ua.offset().top + Bs + Ws, b = ua.height(), O && (O = !1, k = !1, null == c && (y.insertAfter(Rs), Rs.detach()), y.css({
                  position: '',
                  top: '',
                  width: '',
                  bottom: ''
                }).removeClass(f), Hs = !0), w = y.offset().top - (parseInt(y.css('margin-top'), 10) || 0) - u, T = y.outerHeight(!0), E = y.css('float'), Rs && Rs.css({
                  width: y.outerWidth(!0),
                  height: T,
                  display: y.css('display'),
                  'vertical-align': y.css('vertical-align'),
                  float: E
                }), Hs)) return Ms()
            }, ws(), T !== b) ? (q = void 0, pt = u, Os = h, Ms = function() {
              var Bs, Ws, Hs, Xs, Fs, Ns;
              if (!z) return (Hs = !1, null != Os && (Os -= 1, 0 >= Os && (Os = h, ws(), Hs = !0)), Hs || r.height() === j || (ws(), Hs = !0), Xs = a.scrollTop(), null != q && (Ws = Xs - q), q = Xs, O ? (n && (Fs = Xs + T + pt > b + x, k && !Fs && (k = !1, y.css({
                position: 'fixed',
                bottom: '',
                top: pt
              }).trigger('sticky_kit:unbottom'))), Xs < w && (O = !1, pt = u, null == c && (('left' === E || 'right' === E) && y.insertAfter(Rs), Rs.detach()), Bs = {
                position: '',
                width: '',
                top: ''
              }, y.css(Bs).removeClass(f).trigger('sticky_kit:unstick')), p && (Ns = a.height(), T + u > Ns && !k && (pt -= Ws, pt = Math.max(Ns - T, pt), pt = Math.min(u, pt), O && y.css({
                top: pt + 'px'
              })))) : Xs > w && (O = !0, Bs = {
                position: 'fixed',
                top: pt
              }, Bs.width = 'border-box' === y.css('box-sizing') ? y.outerWidth() + 'px' : y.width() + 'px', y.css(Bs).addClass(f), null == c && (y.after(Rs), ('left' === E || 'right' === E) && Rs.append(y)), y.trigger('sticky_kit:stick')), O && n && (null == Fs && (Fs = Xs + T + pt > b + x), !k && Fs)) ? (k = !0, 'static' === ua.css('position') && ua.css({
                position: 'relative'
              }), y.css({
                position: 'absolute',
                bottom: v,
                top: 'auto'
              }).trigger('sticky_kit:bottom')) : void 0
            }, _s = function() {
              return ws(), Ms()
            }, _ = function() {
              if (z = !0, a.off('touchmove', Ms), a.off('scroll', Ms), a.off('resize', _s), t(document.body).off('sticky_kit:recalc', _s), y.off('sticky_kit:detach', _), y.removeData('sticky_kit'), y.css({
                  position: '',
                  bottom: '',
                  top: '',
                  width: ''
                }), ua.position('position', ''), O) return null == c && (('left' === E || 'right' === E) && y.insertAfter(Rs), Rs.remove()), y.removeClass(f)
            }, a.on('touchmove', Ms), a.on('scroll', Ms), a.on('resize', _s), t(document.body).on('sticky_kit:recalc', _s), y.on('sticky_kit:detach', _), setTimeout(Ms, 0)) : void 0
          }
        }, d = 0, m = this.length; d < m; d++) o = this[d], l(t(o));
      return this
    }
  }.call(this),
  function() {
    'use strict';

    function t(l) {
      l.fn.swiper = function(d) {
        var p;
        return l(this).each(function() {
          var m = new s(this, d);
          p || (p = m)
        }), p
      }
    }
    var a, s = function(l, d) {
      function p(js) {
        return Math.floor(js)
      }

      function m() {
        var js = O.params.autoplay,
          Vs = O.slides.eq(O.activeIndex);
        Vs.attr('data-swiper-autoplay') && (js = Vs.attr('data-swiper-autoplay') || O.params.autoplay), O.autoplayTimeoutId = setTimeout(function() {
          O.params.loop ? (O.fixLoop(), O._slideNext(), O.emit('onAutoplay', O)) : O.isEnd ? d.autoplayStopOnLast ? O.stopAutoplay() : (O._slideTo(0), O.emit('onAutoplay', O)) : (O._slideNext(), O.emit('onAutoplay', O))
        }, js)
      }

      function c(js, Vs) {
        var Zs = a(js.target);
        if (!Zs.is(Vs))
          if ('string' == typeof Vs) Zs = Zs.parents(Vs);
          else if (Vs.nodeType) {
          var Us;
          return Zs.parents().each(function(Ks, Qs) {
            Qs === Vs && (Us = Vs)
          }), Us ? Vs : void 0
        }
        return 0 === Zs.length ? void 0 : Zs[0]
      }

      function u(js, Vs) {
        Vs = Vs || {};
        var Zs = window.MutationObserver || window.WebkitMutationObserver,
          Us = new Zs(function(Ks) {
            Ks.forEach(function(Qs) {
              O.onResize(!0), O.emit('onObserverUpdate', O, Qs)
            })
          });
        Us.observe(js, {
          attributes: 'undefined' == typeof Vs.attributes || Vs.attributes,
          childList: 'undefined' == typeof Vs.childList || Vs.childList,
          characterData: 'undefined' == typeof Vs.characterData || Vs.characterData
        }), O.observers.push(Us)
      }

      function g(js) {
        js.originalEvent && (js = js.originalEvent);
        var Vs = js.keyCode || js.charCode;
        if (!O.params.allowSwipeToNext && (O.isHorizontal() && 39 === Vs || !O.isHorizontal() && 40 === Vs)) return !1;
        if (!O.params.allowSwipeToPrev && (O.isHorizontal() && 37 === Vs || !O.isHorizontal() && 38 === Vs)) return !1;
        if (!(js.shiftKey || js.altKey || js.ctrlKey || js.metaKey) && !(document.activeElement && document.activeElement.nodeName && ('input' === document.activeElement.nodeName.toLowerCase() || 'textarea' === document.activeElement.nodeName.toLowerCase()))) {
          if (37 === Vs || 39 === Vs || 38 === Vs || 40 === Vs) {
            var Zs = !1;
            if (0 < O.container.parents('.' + O.params.slideClass).length && 0 === O.container.parents('.' + O.params.slideActiveClass).length) return;
            var Us = {
                left: window.pageXOffset,
                top: window.pageYOffset
              },
              Ks = window.innerWidth,
              Qs = window.innerHeight,
              $s = O.container.offset();
            O.rtl && ($s.left -= O.container[0].scrollLeft);
            for (var ti, Js = [
                [$s.left, $s.top],
                [$s.left + O.width, $s.top],
                [$s.left, $s.top + O.height],
                [$s.left + O.width, $s.top + O.height]
              ], ei = 0; ei < Js.length; ei++) ti = Js[ei], ti[0] >= Us.left && ti[0] <= Us.left + Ks && ti[1] >= Us.top && ti[1] <= Us.top + Qs && (Zs = !0);
            if (!Zs) return
          }
          O.isHorizontal() ? ((37 === Vs || 39 === Vs) && (js.preventDefault ? js.preventDefault() : js.returnValue = !1), (39 === Vs && !O.rtl || 37 === Vs && O.rtl) && O.slideNext(), (37 === Vs && !O.rtl || 39 === Vs && O.rtl) && O.slidePrev()) : ((38 === Vs || 40 === Vs) && (js.preventDefault ? js.preventDefault() : js.returnValue = !1), 40 === Vs && O.slideNext(), 38 === Vs && O.slidePrev()), O.emit('onKeyPress', O, Vs)
        }
      }

      function h() {
        var js = 'onwheel',
          Vs = js in document;
        if (!Vs) {
          var Zs = document.createElement('div');
          Zs.setAttribute(js, 'return;'), Vs = 'function' == typeof Zs[js]
        }
        return !Vs && document.implementation && document.implementation.hasFeature && !0 !== document.implementation.hasFeature('', '') && (Vs = document.implementation.hasFeature('Events.wheel', '3.0')), Vs
      }

      function f(js) {
        var Vs = 10,
          Zs = 40,
          Us = 800,
          Ks = 0,
          Qs = 0,
          $s = 0,
          Js = 0;
        return 'detail' in js && (Qs = js.detail), 'wheelDelta' in js && (Qs = -js.wheelDelta / 120), 'wheelDeltaY' in js && (Qs = -js.wheelDeltaY / 120), 'wheelDeltaX' in js && (Ks = -js.wheelDeltaX / 120), 'axis' in js && js.axis === js.HORIZONTAL_AXIS && (Ks = Qs, Qs = 0), $s = Ks * Vs, Js = Qs * Vs, 'deltaY' in js && (Js = js.deltaY), 'deltaX' in js && ($s = js.deltaX), ($s || Js) && js.deltaMode && (1 === js.deltaMode ? ($s *= Zs, Js *= Zs) : ($s *= Us, Js *= Us)), $s && !Ks && (Ks = 1 > $s ? -1 : 1), Js && !Qs && (Qs = 1 > Js ? -1 : 1), {
          spinX: Ks,
          spinY: Qs,
          pixelX: $s,
          pixelY: Js
        }
      }

      function y(js) {
        js.originalEvent && (js = js.originalEvent);
        var Vs = 0,
          Zs = O.rtl ? -1 : 1,
          Us = f(js);
        if (!O.params.mousewheelForceToAxis) Vs = Math.abs(Us.pixelX) > Math.abs(Us.pixelY) ? -Us.pixelX * Zs : -Us.pixelY;
        else if (O.isHorizontal()) {
          if (Math.abs(Us.pixelX) > Math.abs(Us.pixelY)) Vs = Us.pixelX * Zs;
          else return;
        } else if (Math.abs(Us.pixelY) > Math.abs(Us.pixelX)) Vs = Us.pixelY;
        else return;
        if (0 !== Vs) {
          if (O.params.mousewheelInvert && (Vs = -Vs), !O.params.freeMode) {
            if (60 < new window.Date().getTime() - O.mousewheel.lastScrollTime)
              if (0 > Vs) {
                if ((!O.isEnd || O.params.loop) && !O.animating) O.slideNext(), O.emit('onScroll', O, js);
                else if (O.params.mousewheelReleaseOnEdges) return !0;
              } else if ((!O.isBeginning || O.params.loop) && !O.animating) O.slidePrev(), O.emit('onScroll', O, js);
            else if (O.params.mousewheelReleaseOnEdges) return !0;
            O.mousewheel.lastScrollTime = new window.Date().getTime()
          } else {
            var Ks = O.getWrapperTranslate() + Vs * O.params.mousewheelSensitivity,
              Qs = O.isBeginning,
              $s = O.isEnd;
            if (Ks >= O.minTranslate() && (Ks = O.minTranslate()), Ks <= O.maxTranslate() && (Ks = O.maxTranslate()), O.setWrapperTransition(0), O.setWrapperTranslate(Ks), O.updateProgress(), O.updateActiveIndex(), (!Qs && O.isBeginning || !$s && O.isEnd) && O.updateClasses(), O.params.freeModeSticky ? (clearTimeout(O.mousewheel.timeout), O.mousewheel.timeout = setTimeout(function() {
                O.slideReset()
              }, 300)) : O.params.lazyLoading && O.lazy && O.lazy.load(), O.emit('onScroll', O, js), O.params.autoplay && O.params.autoplayDisableOnInteraction && O.stopAutoplay(), 0 === Ks || Ks === O.maxTranslate()) return
          }
          return js.preventDefault ? js.preventDefault() : js.returnValue = !1, !1
        }
      }

      function v(js, Vs) {
        js = a(js);
        var Us, Ks, Qs, Zs = O.rtl ? -1 : 1;
        Us = js.attr('data-swiper-parallax') || '0', Ks = js.attr('data-swiper-parallax-x'), Qs = js.attr('data-swiper-parallax-y'), Ks || Qs ? (Ks = Ks || '0', Qs = Qs || '0') : O.isHorizontal() ? (Ks = Us, Qs = '0') : (Qs = Us, Ks = '0'), Ks = 0 <= Ks.indexOf('%') ? parseInt(Ks, 10) * Vs * Zs + '%' : Ks * Vs * Zs + 'px', Qs = 0 <= Qs.indexOf('%') ? parseInt(Qs, 10) * Vs + '%' : Qs * Vs + 'px', js.transform('translate3d(' + Ks + ', ' + Qs + ',0px)')
      }

      function x(js) {
        return 0 !== js.indexOf('on') && (js[0] === js[0].toUpperCase() ? js = 'on' + js : js = 'on' + js[0].toUpperCase() + js.substring(1)), js
      }
      if (!(this instanceof s)) return new s(l, d);
      var b = {
          direction: 'horizontal',
          touchEventsTarget: 'container',
          initialSlide: 0,
          speed: 300,
          autoplay: !1,
          autoplayDisableOnInteraction: !0,
          autoplayStopOnLast: !1,
          iOSEdgeSwipeDetection: !1,
          iOSEdgeSwipeThreshold: 20,
          freeMode: !1,
          freeModeMomentum: !0,
          freeModeMomentumRatio: 1,
          freeModeMomentumBounce: !0,
          freeModeMomentumBounceRatio: 1,
          freeModeMomentumVelocityRatio: 1,
          freeModeSticky: !1,
          freeModeMinimumVelocity: 0.02,
          autoHeight: !1,
          setWrapperSize: !1,
          virtualTranslate: !1,
          effect: 'slide',
          coverflow: {
            rotate: 50,
            stretch: 0,
            depth: 100,
            modifier: 1,
            slideShadows: !0
          },
          flip: {
            slideShadows: !0,
            limitRotation: !0
          },
          cube: {
            slideShadows: !0,
            shadow: !0,
            shadowOffset: 20,
            shadowScale: 0.94
          },
          fade: {
            crossFade: !1
          },
          parallax: !1,
          zoom: !1,
          zoomMax: 3,
          zoomMin: 1,
          zoomToggle: !0,
          scrollbar: null,
          scrollbarHide: !0,
          scrollbarDraggable: !1,
          scrollbarSnapOnRelease: !1,
          keyboardControl: !1,
          mousewheelControl: !1,
          mousewheelReleaseOnEdges: !1,
          mousewheelInvert: !1,
          mousewheelForceToAxis: !1,
          mousewheelSensitivity: 1,
          mousewheelEventsTarged: 'container',
          hashnav: !1,
          hashnavWatchState: !1,
          history: !1,
          replaceState: !1,
          breakpoints: void 0,
          spaceBetween: 0,
          slidesPerView: 1,
          slidesPerColumn: 1,
          slidesPerColumnFill: 'column',
          slidesPerGroup: 1,
          centeredSlides: !1,
          slidesOffsetBefore: 0,
          slidesOffsetAfter: 0,
          roundLengths: !1,
          touchRatio: 1,
          touchAngle: 45,
          simulateTouch: !0,
          shortSwipes: !0,
          longSwipes: !0,
          longSwipesRatio: 0.5,
          longSwipesMs: 300,
          followFinger: !0,
          onlyExternal: !1,
          threshold: 0,
          touchMoveStopPropagation: !0,
          touchReleaseOnEdges: !1,
          uniqueNavElements: !0,
          pagination: null,
          paginationElement: 'span',
          paginationClickable: !1,
          paginationHide: !1,
          paginationBulletRender: null,
          paginationProgressRender: null,
          paginationFractionRender: null,
          paginationCustomRender: null,
          paginationType: 'bullets',
          resistance: !0,
          resistanceRatio: 0.85,
          nextButton: null,
          prevButton: null,
          watchSlidesProgress: !1,
          watchSlidesVisibility: !1,
          grabCursor: !1,
          preventClicks: !0,
          preventClicksPropagation: !0,
          slideToClickedSlide: !1,
          lazyLoading: !1,
          lazyLoadingInPrevNext: !1,
          lazyLoadingInPrevNextAmount: 1,
          lazyLoadingOnTransitionStart: !1,
          preloadImages: !0,
          updateOnImagesReady: !0,
          loop: !1,
          loopAdditionalSlides: 0,
          loopedSlides: null,
          control: void 0,
          controlInverse: !1,
          controlBy: 'slide',
          normalizeSlideIndex: !0,
          allowSwipeToPrev: !0,
          allowSwipeToNext: !0,
          swipeHandler: null,
          noSwiping: !0,
          noSwipingClass: 'swiper-no-swiping',
          passiveListeners: !0,
          containerModifierClass: 'swiper-container-',
          slideClass: 'swiper-slide',
          slideActiveClass: 'swiper-slide-active',
          slideDuplicateActiveClass: 'swiper-slide-duplicate-active',
          slideVisibleClass: 'swiper-slide-visible',
          slideDuplicateClass: 'swiper-slide-duplicate',
          slideNextClass: 'swiper-slide-next',
          slideDuplicateNextClass: 'swiper-slide-duplicate-next',
          slidePrevClass: 'swiper-slide-prev',
          slideDuplicatePrevClass: 'swiper-slide-duplicate-prev',
          wrapperClass: 'swiper-wrapper',
          bulletClass: 'swiper-pagination-bullet',
          bulletActiveClass: 'swiper-pagination-bullet-active',
          buttonDisabledClass: 'swiper-button-disabled',
          paginationCurrentClass: 'swiper-pagination-current',
          paginationTotalClass: 'swiper-pagination-total',
          paginationHiddenClass: 'swiper-pagination-hidden',
          paginationProgressbarClass: 'swiper-pagination-progressbar',
          paginationClickableClass: 'swiper-pagination-clickable',
          paginationModifierClass: 'swiper-pagination-',
          lazyLoadingClass: 'swiper-lazy',
          lazyStatusLoadingClass: 'swiper-lazy-loading',
          lazyStatusLoadedClass: 'swiper-lazy-loaded',
          lazyPreloaderClass: 'swiper-lazy-preloader',
          notificationClass: 'swiper-notification',
          preloaderClass: 'preloader',
          zoomContainerClass: 'swiper-zoom-container',
          observer: !1,
          observeParents: !1,
          a11y: !1,
          prevSlideMessage: 'Previous slide',
          nextSlideMessage: 'Next slide',
          firstSlideMessage: 'This is the first slide',
          lastSlideMessage: 'This is the last slide',
          paginationBulletMessage: 'Go to slide {{index}}',
          runCallbacksOnInit: !0
        },
        w = d && d.virtualTranslate;
      d = d || {};
      var T = {};
      for (var E in d)
        if ('object' == typeof d[E] && null !== d[E] && !(d[E].nodeType || d[E] === window || d[E] === document || 'undefined' != typeof Dom7 && d[E] instanceof Dom7 || 'undefined' != typeof jQuery && d[E] instanceof jQuery))
          for (var z in T[E] = {}, d[E]) T[E][z] = d[E][z];
        else T[E] = d[E];
      for (var k in b)
        if ('undefined' == typeof d[k]) d[k] = b[k];
        else if ('object' == typeof d[k])
        for (var _ in b[k]) 'undefined' == typeof d[k][_] && (d[k][_] = b[k][_]);
      var O = this;
      if ((O.params = d, O.originalParams = T, O.classNames = [], 'undefined' != typeof a && 'undefined' != typeof Dom7 && (a = Dom7), !('undefined' == typeof a && (a = 'undefined' == typeof Dom7 ? window.Dom7 || window.Zepto || window.jQuery : Dom7, !a))) && (O.$ = a, O.currentBreakpoint = void 0, O.getActiveBreakpoint = function() {
          if (!O.params.breakpoints) return !1;
          var Zs, js = !1,
            Vs = [];
          for (Zs in O.params.breakpoints) O.params.breakpoints.hasOwnProperty(Zs) && Vs.push(Zs);
          Vs.sort(function(Ks, Qs) {
            return parseInt(Ks, 10) > parseInt(Qs, 10)
          });
          for (var Us = 0; Us < Vs.length; Us++) Zs = Vs[Us], Zs >= window.innerWidth && !js && (js = Zs);
          return js || 'max'
        }, O.setBreakpoint = function() {
          var js = O.getActiveBreakpoint();
          if (js && O.currentBreakpoint !== js) {
            var Vs = js in O.params.breakpoints ? O.params.breakpoints[js] : O.originalParams,
              Zs = O.params.loop && Vs.slidesPerView !== O.params.slidesPerView;
            for (var Us in Vs) O.params[Us] = Vs[Us];
            O.currentBreakpoint = js, Zs && O.destroyLoop && O.reLoop(!0)
          }
        }, O.params.breakpoints && O.setBreakpoint(), O.container = a(l), 0 !== O.container.length)) {
        if (1 < O.container.length) {
          var q = [];
          return O.container.each(function() {
            this,
            q.push(new s(this, d))
          }), q
        }
        O.container[0].swiper = O, O.container.data('swiper', O), O.classNames.push(O.params.containerModifierClass + O.params.direction), O.params.freeMode && O.classNames.push(O.params.containerModifierClass + 'free-mode'), O.support.flexbox || (O.classNames.push(O.params.containerModifierClass + 'no-flexbox'), O.params.slidesPerColumn = 1), O.params.autoHeight && O.classNames.push(O.params.containerModifierClass + 'autoheight'), (O.params.parallax || O.params.watchSlidesVisibility) && (O.params.watchSlidesProgress = !0), O.params.touchReleaseOnEdges && (O.params.resistanceRatio = 0), 0 <= ['cube', 'coverflow', 'flip'].indexOf(O.params.effect) && (O.support.transforms3d ? (O.params.watchSlidesProgress = !0, O.classNames.push(O.params.containerModifierClass + '3d')) : O.params.effect = 'slide'), 'slide' !== O.params.effect && O.classNames.push(O.params.containerModifierClass + O.params.effect), 'cube' === O.params.effect && (O.params.resistanceRatio = 0, O.params.slidesPerView = 1, O.params.slidesPerColumn = 1, O.params.slidesPerGroup = 1, O.params.centeredSlides = !1, O.params.spaceBetween = 0, O.params.virtualTranslate = !0), ('fade' === O.params.effect || 'flip' === O.params.effect) && (O.params.slidesPerView = 1, O.params.slidesPerColumn = 1, O.params.slidesPerGroup = 1, O.params.watchSlidesProgress = !0, O.params.spaceBetween = 0, 'undefined' == typeof w && (O.params.virtualTranslate = !0)), O.params.grabCursor && O.support.touch && (O.params.grabCursor = !1), O.wrapper = O.container.children('.' + O.params.wrapperClass), O.params.pagination && (O.paginationContainer = a(O.params.pagination), O.params.uniqueNavElements && 'string' == typeof O.params.pagination && 1 < O.paginationContainer.length && 1 === O.container.find(O.params.pagination).length && (O.paginationContainer = O.container.find(O.params.pagination)), 'bullets' === O.params.paginationType && O.params.paginationClickable ? O.paginationContainer.addClass(O.params.paginationModifierClass + 'clickable') : O.params.paginationClickable = !1, O.paginationContainer.addClass(O.params.paginationModifierClass + O.params.paginationType)), (O.params.nextButton || O.params.prevButton) && (O.params.nextButton && (O.nextButton = a(O.params.nextButton), O.params.uniqueNavElements && 'string' == typeof O.params.nextButton && 1 < O.nextButton.length && 1 === O.container.find(O.params.nextButton).length && (O.nextButton = O.container.find(O.params.nextButton))), O.params.prevButton && (O.prevButton = a(O.params.prevButton), O.params.uniqueNavElements && 'string' == typeof O.params.prevButton && 1 < O.prevButton.length && 1 === O.container.find(O.params.prevButton).length && (O.prevButton = O.container.find(O.params.prevButton)))), O.isHorizontal = function() {
          return 'horizontal' === O.params.direction
        }, O.rtl = O.isHorizontal() && ('rtl' === O.container[0].dir.toLowerCase() || 'rtl' === O.container.css('direction')), O.rtl && O.classNames.push(O.params.containerModifierClass + 'rtl'), O.rtl && (O.wrongRTL = '-webkit-box' === O.wrapper.css('display')), 1 < O.params.slidesPerColumn && O.classNames.push(O.params.containerModifierClass + 'multirow'), O.device.android && O.classNames.push(O.params.containerModifierClass + 'android'), O.container.addClass(O.classNames.join(' ')), O.translate = 0, O.progress = 0, O.velocity = 0, O.lockSwipeToNext = function() {
          O.params.allowSwipeToNext = !1, !1 === O.params.allowSwipeToPrev && O.params.grabCursor && O.unsetGrabCursor()
        }, O.lockSwipeToPrev = function() {
          O.params.allowSwipeToPrev = !1, !1 === O.params.allowSwipeToNext && O.params.grabCursor && O.unsetGrabCursor()
        }, O.lockSwipes = function() {
          O.params.allowSwipeToNext = O.params.allowSwipeToPrev = !1, O.params.grabCursor && O.unsetGrabCursor()
        }, O.unlockSwipeToNext = function() {
          O.params.allowSwipeToNext = !0, !0 === O.params.allowSwipeToPrev && O.params.grabCursor && O.setGrabCursor()
        }, O.unlockSwipeToPrev = function() {
          O.params.allowSwipeToPrev = !0, !0 === O.params.allowSwipeToNext && O.params.grabCursor && O.setGrabCursor()
        }, O.unlockSwipes = function() {
          O.params.allowSwipeToNext = O.params.allowSwipeToPrev = !0, O.params.grabCursor && O.setGrabCursor()
        }, O.setGrabCursor = function(js) {
          O.container[0].style.cursor = 'move', O.container[0].style.cursor = js ? '-webkit-grabbing' : '-webkit-grab', O.container[0].style.cursor = js ? '-moz-grabbin' : '-moz-grab', O.container[0].style.cursor = js ? 'grabbing' : 'grab'
        }, O.unsetGrabCursor = function() {
          O.container[0].style.cursor = ''
        }, O.params.grabCursor && O.setGrabCursor(), O.imagesToLoad = [], O.imagesLoaded = 0, O.loadImage = function(js, Vs, Zs, Us, Ks, Qs) {
          function $s() {
            Qs && Qs()
          }
          var Js;
          js.complete && Ks ? $s() : Vs ? (Js = new window.Image, Js.onload = $s, Js.onerror = $s, Us && (Js.sizes = Us), Zs && (Js.srcset = Zs), Vs && (Js.src = Vs)) : $s()
        }, O.preloadImages = function() {
          function js() {
            'undefined' != typeof O && null !== O && O && (void 0 !== O.imagesLoaded && O.imagesLoaded++, O.imagesLoaded === O.imagesToLoad.length && (O.params.updateOnImagesReady && O.update(), O.emit('onImagesReady', O)))
          }
          O.imagesToLoad = O.container.find('img');
          for (var Vs = 0; Vs < O.imagesToLoad.length; Vs++) O.loadImage(O.imagesToLoad[Vs], O.imagesToLoad[Vs].currentSrc || O.imagesToLoad[Vs].getAttribute('src'), O.imagesToLoad[Vs].srcset || O.imagesToLoad[Vs].getAttribute('srcset'), O.imagesToLoad[Vs].sizes || O.imagesToLoad[Vs].getAttribute('sizes'), !0, js)
        }, O.autoplayTimeoutId = void 0, O.autoplaying = !1, O.autoplayPaused = !1, O.startAutoplay = function() {
          return 'undefined' == typeof O.autoplayTimeoutId && !!O.params.autoplay && !O.autoplaying && void(O.autoplaying = !0, O.emit('onAutoplayStart', O), m())
        }, O.stopAutoplay = function() {
          O.autoplayTimeoutId && (O.autoplayTimeoutId && clearTimeout(O.autoplayTimeoutId), O.autoplaying = !1, O.autoplayTimeoutId = void 0, O.emit('onAutoplayStop', O))
        }, O.pauseAutoplay = function(js) {
          O.autoplayPaused || (O.autoplayTimeoutId && clearTimeout(O.autoplayTimeoutId), O.autoplayPaused = !0, 0 === js ? (O.autoplayPaused = !1, m()) : O.wrapper.transitionEnd(function() {
            O && (O.autoplayPaused = !1, O.autoplaying ? m() : O.stopAutoplay())
          }))
        }, O.minTranslate = function() {
          return -O.snapGrid[0]
        }, O.maxTranslate = function() {
          return -O.snapGrid[O.snapGrid.length - 1]
        }, O.updateAutoHeight = function() {
          var Zs, js = [],
            Vs = 0;
          if ('auto' !== O.params.slidesPerView && 1 < O.params.slidesPerView)
            for (Zs = 0; Zs < Math.ceil(O.params.slidesPerView); Zs++) {
              var Us = O.activeIndex + Zs;
              if (Us > O.slides.length) break;
              js.push(O.slides.eq(Us)[0])
            } else js.push(O.slides.eq(O.activeIndex)[0]);
          for (Zs = 0; Zs < js.length; Zs++)
            if ('undefined' != typeof js[Zs]) {
              var Ks = js[Zs].offsetHeight;
              Vs = Ks > Vs ? Ks : Vs
            }
          Vs && O.wrapper.css('height', Vs + 'px')
        }, O.updateContainerSize = function() {
          var js, Vs;
          js = 'undefined' == typeof O.params.width ? O.container[0].clientWidth : O.params.width, Vs = 'undefined' == typeof O.params.height ? O.container[0].clientHeight : O.params.height, 0 === js && O.isHorizontal() || 0 === Vs && !O.isHorizontal() || (js = js - parseInt(O.container.css('padding-left'), 10) - parseInt(O.container.css('padding-right'), 10), Vs = Vs - parseInt(O.container.css('padding-top'), 10) - parseInt(O.container.css('padding-bottom'), 10), O.width = js, O.height = Vs, O.size = O.isHorizontal() ? O.width : O.height)
        }, O.updateSlidesSize = function() {
          O.slides = O.wrapper.children('.' + O.params.slideClass), O.snapGrid = [], O.slidesGrid = [], O.slidesSizesGrid = [];
          var Ks, js = O.params.spaceBetween,
            Vs = -O.params.slidesOffsetBefore,
            Zs = 0,
            Us = 0;
          if ('undefined' != typeof O.size) {
            'string' == typeof js && 0 <= js.indexOf('%') && (js = parseFloat(js.replace('%', '')) / 100 * O.size), O.virtualSize = -js, O.rtl ? O.slides.css({
              marginLeft: '',
              marginTop: ''
            }) : O.slides.css({
              marginRight: '',
              marginBottom: ''
            });
            var Qs;
            1 < O.params.slidesPerColumn && (Qs = Math.floor(O.slides.length / O.params.slidesPerColumn) === O.slides.length / O.params.slidesPerColumn ? O.slides.length : Math.ceil(O.slides.length / O.params.slidesPerColumn) * O.params.slidesPerColumn, 'auto' !== O.params.slidesPerView && 'row' === O.params.slidesPerColumnFill && (Qs = Math.max(Qs, O.params.slidesPerView * O.params.slidesPerColumn)));
            var ti, $s = O.params.slidesPerColumn,
              Js = Qs / $s,
              ei = Js - (O.params.slidesPerColumn * Js - O.slides.length);
            for (Ks = 0; Ks < O.slides.length; Ks++) {
              ti = 0;
              var ai = O.slides.eq(Ks);
              if (1 < O.params.slidesPerColumn) {
                var si, ii, ri;
                'column' === O.params.slidesPerColumnFill ? (ii = Math.floor(Ks / $s), ri = Ks - ii * $s, (ii > ei || ii === ei && ri === $s - 1) && ++ri >= $s && (ri = 0, ii++), si = ii + ri * Qs / $s, ai.css({
                  '-webkit-box-ordinal-group': si,
                  '-moz-box-ordinal-group': si,
                  '-ms-flex-order': si,
                  '-webkit-order': si,
                  order: si
                })) : (ri = Math.floor(Ks / Js), ii = Ks - ri * Js), ai.css('margin-' + (O.isHorizontal() ? 'top' : 'left'), 0 !== ri && O.params.spaceBetween && O.params.spaceBetween + 'px').attr('data-swiper-column', ii).attr('data-swiper-row', ri)
              }
              'none' === ai.css('display') || ('auto' === O.params.slidesPerView ? (ti = O.isHorizontal() ? ai.outerWidth(!0) : ai.outerHeight(!0), O.params.roundLengths && (ti = p(ti))) : (ti = (O.size - (O.params.slidesPerView - 1) * js) / O.params.slidesPerView, O.params.roundLengths && (ti = p(ti)), O.isHorizontal() ? O.slides[Ks].style.width = ti + 'px' : O.slides[Ks].style.height = ti + 'px'), O.slides[Ks].swiperSlideSize = ti, O.slidesSizesGrid.push(ti), O.params.centeredSlides ? (Vs = Vs + ti / 2 + Zs / 2 + js, 0 == Zs && 0 !== Ks && (Vs = Vs - O.size / 2 - js), 0 === Ks && (Vs = Vs - O.size / 2 - js), Math.abs(Vs) < 1 / 1e3 && (Vs = 0), 0 == Us % O.params.slidesPerGroup && O.snapGrid.push(Vs), O.slidesGrid.push(Vs)) : (0 == Us % O.params.slidesPerGroup && O.snapGrid.push(Vs), O.slidesGrid.push(Vs), Vs = Vs + ti + js), O.virtualSize += ti + js, Zs = ti, Us++)
            }
            O.virtualSize = Math.max(O.virtualSize, O.size) + O.params.slidesOffsetAfter;
            var oi;
            if (O.rtl && O.wrongRTL && ('slide' === O.params.effect || 'coverflow' === O.params.effect) && O.wrapper.css({
                width: O.virtualSize + O.params.spaceBetween + 'px'
              }), (!O.support.flexbox || O.params.setWrapperSize) && (O.isHorizontal() ? O.wrapper.css({
                width: O.virtualSize + O.params.spaceBetween + 'px'
              }) : O.wrapper.css({
                height: O.virtualSize + O.params.spaceBetween + 'px'
              })), 1 < O.params.slidesPerColumn && (O.virtualSize = (ti + O.params.spaceBetween) * Qs, O.virtualSize = Math.ceil(O.virtualSize / O.params.slidesPerColumn) - O.params.spaceBetween, O.isHorizontal() ? O.wrapper.css({
                width: O.virtualSize + O.params.spaceBetween + 'px'
              }) : O.wrapper.css({
                height: O.virtualSize + O.params.spaceBetween + 'px'
              }), O.params.centeredSlides)) {
              for (oi = [], Ks = 0; Ks < O.snapGrid.length; Ks++) O.snapGrid[Ks] < O.virtualSize + O.snapGrid[0] && oi.push(O.snapGrid[Ks]);
              O.snapGrid = oi
            }
            if (!O.params.centeredSlides) {
              for (oi = [], Ks = 0; Ks < O.snapGrid.length; Ks++) O.snapGrid[Ks] <= O.virtualSize - O.size && oi.push(O.snapGrid[Ks]);
              O.snapGrid = oi, 1 < Math.floor(O.virtualSize - O.size) - Math.floor(O.snapGrid[O.snapGrid.length - 1]) && O.snapGrid.push(O.virtualSize - O.size)
            }
            0 === O.snapGrid.length && (O.snapGrid = [0]), 0 !== O.params.spaceBetween && (O.isHorizontal() ? O.rtl ? O.slides.css({
              marginLeft: js + 'px'
            }) : O.slides.css({
              marginRight: js + 'px'
            }) : O.slides.css({
              marginBottom: js + 'px'
            })), O.params.watchSlidesProgress && O.updateSlidesOffset()
          }
        }, O.updateSlidesOffset = function() {
          for (var js = 0; js < O.slides.length; js++) O.slides[js].swiperSlideOffset = O.isHorizontal() ? O.slides[js].offsetLeft : O.slides[js].offsetTop
        }, O.currentSlidesPerView = function() {
          var Vs, Zs, js = 1;
          if (O.params.centeredSlides) {
            var Ks, Us = O.slides[O.activeIndex].swiperSlideSize;
            for (Vs = O.activeIndex + 1; Vs < O.slides.length; Vs++) O.slides[Vs] && !Ks && (Us += O.slides[Vs].swiperSlideSize, js++, Us > O.size && (Ks = !0));
            for (Zs = O.activeIndex - 1; 0 <= Zs; Zs--) O.slides[Zs] && !Ks && (Us += O.slides[Zs].swiperSlideSize, js++, Us > O.size && (Ks = !0))
          } else
            for (Vs = O.activeIndex + 1; Vs < O.slides.length; Vs++) O.slidesGrid[Vs] - O.slidesGrid[O.activeIndex] < O.size && js++;
          return js
        }, O.updateSlidesProgress = function(js) {
          if ('undefined' == typeof js && (js = O.translate || 0), 0 !== O.slides.length) {
            'undefined' == typeof O.slides[0].swiperSlideOffset && O.updateSlidesOffset();
            var Vs = -js;
            O.rtl && (Vs = js), O.slides.removeClass(O.params.slideVisibleClass);
            for (var Zs = 0; Zs < O.slides.length; Zs++) {
              var Us = O.slides[Zs],
                Ks = (Vs + (O.params.centeredSlides ? O.minTranslate() : 0) - Us.swiperSlideOffset) / (Us.swiperSlideSize + O.params.spaceBetween);
              if (O.params.watchSlidesVisibility) {
                var Qs = -(Vs - Us.swiperSlideOffset),
                  $s = Qs + O.slidesSizesGrid[Zs],
                  Js = 0 <= Qs && Qs < O.size || 0 < $s && $s <= O.size || 0 >= Qs && $s >= O.size;
                Js && O.slides.eq(Zs).addClass(O.params.slideVisibleClass)
              }
              Us.progress = O.rtl ? -Ks : Ks
            }
          }
        }, O.updateProgress = function(js) {
          'undefined' == typeof js && (js = O.translate || 0);
          var Vs = O.maxTranslate() - O.minTranslate(),
            Zs = O.isBeginning,
            Us = O.isEnd;
          0 == Vs ? (O.progress = 0, O.isBeginning = O.isEnd = !0) : (O.progress = (js - O.minTranslate()) / Vs, O.isBeginning = 0 >= O.progress, O.isEnd = 1 <= O.progress), O.isBeginning && !Zs && O.emit('onReachBeginning', O), O.isEnd && !Us && O.emit('onReachEnd', O), O.params.watchSlidesProgress && O.updateSlidesProgress(js), O.emit('onProgress', O, O.progress)
        }, O.updateActiveIndex = function() {
          var Vs, Zs, Us, js = O.rtl ? O.translate : -O.translate;
          for (Zs = 0; Zs < O.slidesGrid.length; Zs++) 'undefined' == typeof O.slidesGrid[Zs + 1] ? js >= O.slidesGrid[Zs] && (Vs = Zs) : js >= O.slidesGrid[Zs] && js < O.slidesGrid[Zs + 1] - (O.slidesGrid[Zs + 1] - O.slidesGrid[Zs]) / 2 ? Vs = Zs : js >= O.slidesGrid[Zs] && js < O.slidesGrid[Zs + 1] && (Vs = Zs + 1);
          O.params.normalizeSlideIndex && (0 > Vs || 'undefined' == typeof Vs) && (Vs = 0), Us = Math.floor(Vs / O.params.slidesPerGroup), Us >= O.snapGrid.length && (Us = O.snapGrid.length - 1), Vs === O.activeIndex || (O.snapIndex = Us, O.previousIndex = O.activeIndex, O.activeIndex = Vs, O.updateClasses(), O.updateRealIndex())
        }, O.updateRealIndex = function() {
          O.realIndex = parseInt(O.slides.eq(O.activeIndex).attr('data-swiper-slide-index') || O.activeIndex, 10)
        }, O.updateClasses = function() {
          O.slides.removeClass(O.params.slideActiveClass + ' ' + O.params.slideNextClass + ' ' + O.params.slidePrevClass + ' ' + O.params.slideDuplicateActiveClass + ' ' + O.params.slideDuplicateNextClass + ' ' + O.params.slideDuplicatePrevClass);
          var js = O.slides.eq(O.activeIndex);
          js.addClass(O.params.slideActiveClass), d.loop && (js.hasClass(O.params.slideDuplicateClass) ? O.wrapper.children('.' + O.params.slideClass + ':not(.' + O.params.slideDuplicateClass + ')[data-swiper-slide-index="' + O.realIndex + '"]').addClass(O.params.slideDuplicateActiveClass) : O.wrapper.children('.' + O.params.slideClass + '.' + O.params.slideDuplicateClass + '[data-swiper-slide-index="' + O.realIndex + '"]').addClass(O.params.slideDuplicateActiveClass));
          var Vs = js.next('.' + O.params.slideClass).addClass(O.params.slideNextClass);
          O.params.loop && 0 === Vs.length && (Vs = O.slides.eq(0), Vs.addClass(O.params.slideNextClass));
          var Zs = js.prev('.' + O.params.slideClass).addClass(O.params.slidePrevClass);
          if (O.params.loop && 0 === Zs.length && (Zs = O.slides.eq(-1), Zs.addClass(O.params.slidePrevClass)), d.loop && (Vs.hasClass(O.params.slideDuplicateClass) ? O.wrapper.children('.' + O.params.slideClass + ':not(.' + O.params.slideDuplicateClass + ')[data-swiper-slide-index="' + Vs.attr('data-swiper-slide-index') + '"]').addClass(O.params.slideDuplicateNextClass) : O.wrapper.children('.' + O.params.slideClass + '.' + O.params.slideDuplicateClass + '[data-swiper-slide-index="' + Vs.attr('data-swiper-slide-index') + '"]').addClass(O.params.slideDuplicateNextClass), Zs.hasClass(O.params.slideDuplicateClass) ? O.wrapper.children('.' + O.params.slideClass + ':not(.' + O.params.slideDuplicateClass + ')[data-swiper-slide-index="' + Zs.attr('data-swiper-slide-index') + '"]').addClass(O.params.slideDuplicatePrevClass) : O.wrapper.children('.' + O.params.slideClass + '.' + O.params.slideDuplicateClass + '[data-swiper-slide-index="' + Zs.attr('data-swiper-slide-index') + '"]').addClass(O.params.slideDuplicatePrevClass)), O.paginationContainer && 0 < O.paginationContainer.length) {
            var Ks, Us = O.params.loop ? Math.ceil((O.slides.length - 2 * O.loopedSlides) / O.params.slidesPerGroup) : O.snapGrid.length;
            if (O.params.loop ? (Ks = Math.ceil((O.activeIndex - O.loopedSlides) / O.params.slidesPerGroup), Ks > O.slides.length - 1 - 2 * O.loopedSlides && (Ks -= O.slides.length - 2 * O.loopedSlides), Ks > Us - 1 && (Ks -= Us), 0 > Ks && 'bullets' !== O.params.paginationType && (Ks = Us + Ks)) : 'undefined' == typeof O.snapIndex ? Ks = O.activeIndex || 0 : Ks = O.snapIndex, 'bullets' === O.params.paginationType && O.bullets && 0 < O.bullets.length && (O.bullets.removeClass(O.params.bulletActiveClass), 1 < O.paginationContainer.length ? O.bullets.each(function() {
                a(this).index() === Ks && a(this).addClass(O.params.bulletActiveClass)
              }) : O.bullets.eq(Ks).addClass(O.params.bulletActiveClass)), 'fraction' === O.params.paginationType && (O.paginationContainer.find('.' + O.params.paginationCurrentClass).text(Ks + 1), O.paginationContainer.find('.' + O.params.paginationTotalClass).text(Us)), 'progress' === O.params.paginationType) {
              var Qs = (Ks + 1) / Us,
                $s = Qs,
                Js = 1;
              O.isHorizontal() || (Js = Qs, $s = 1), O.paginationContainer.find('.' + O.params.paginationProgressbarClass).transform('translate3d(0,0,0) scaleX(' + $s + ') scaleY(' + Js + ')').transition(O.params.speed)
            }
            'custom' === O.params.paginationType && O.params.paginationCustomRender && (O.paginationContainer.html(O.params.paginationCustomRender(O, Ks + 1, Us)), O.emit('onPaginationRendered', O, O.paginationContainer[0]))
          }
          O.params.loop || (O.params.prevButton && O.prevButton && 0 < O.prevButton.length && (O.isBeginning ? (O.prevButton.addClass(O.params.buttonDisabledClass), O.params.a11y && O.a11y && O.a11y.disable(O.prevButton)) : (O.prevButton.removeClass(O.params.buttonDisabledClass), O.params.a11y && O.a11y && O.a11y.enable(O.prevButton))), O.params.nextButton && O.nextButton && 0 < O.nextButton.length && (O.isEnd ? (O.nextButton.addClass(O.params.buttonDisabledClass), O.params.a11y && O.a11y && O.a11y.disable(O.nextButton)) : (O.nextButton.removeClass(O.params.buttonDisabledClass), O.params.a11y && O.a11y && O.a11y.enable(O.nextButton))))
        }, O.updatePagination = function() {
          if (O.params.pagination && O.paginationContainer && 0 < O.paginationContainer.length) {
            var js = '';
            if ('bullets' === O.params.paginationType) {
              for (var Vs = O.params.loop ? Math.ceil((O.slides.length - 2 * O.loopedSlides) / O.params.slidesPerGroup) : O.snapGrid.length, Zs = 0; Zs < Vs; Zs++) js += O.params.paginationBulletRender ? O.params.paginationBulletRender(O, Zs, O.params.bulletClass) : '<' + O.params.paginationElement + ' class="' + O.params.bulletClass + '"></' + O.params.paginationElement + '>';
              O.paginationContainer.html(js), O.bullets = O.paginationContainer.find('.' + O.params.bulletClass), O.params.paginationClickable && O.params.a11y && O.a11y && O.a11y.initPagination()
            }
            'fraction' === O.params.paginationType && (js = O.params.paginationFractionRender ? O.params.paginationFractionRender(O, O.params.paginationCurrentClass, O.params.paginationTotalClass) : '<span class="' + O.params.paginationCurrentClass + '"></span> / <span class="' + O.params.paginationTotalClass + '"></span>', O.paginationContainer.html(js)), 'progress' === O.params.paginationType && (js = O.params.paginationProgressRender ? O.params.paginationProgressRender(O, O.params.paginationProgressbarClass) : '<span class="' + O.params.paginationProgressbarClass + '"></span>', O.paginationContainer.html(js)), 'custom' !== O.params.paginationType && O.emit('onPaginationRendered', O, O.paginationContainer[0])
          }
        }, O.update = function(js) {
          function Vs() {
            O.rtl ? -O.translate : O.translate, Zs = Math.min(Math.max(O.translate, O.maxTranslate()), O.minTranslate()), O.setWrapperTranslate(Zs), O.updateActiveIndex(), O.updateClasses()
          }
          if (O) {
            O.updateContainerSize(), O.updateSlidesSize(), O.updateProgress(), O.updatePagination(), O.updateClasses(), O.params.scrollbar && O.scrollbar && O.scrollbar.set();
            var Zs;
            if (js) {
              var Us;
              O.controller && O.controller.spline && (O.controller.spline = void 0), O.params.freeMode ? (Vs(), O.params.autoHeight && O.updateAutoHeight()) : (Us = ('auto' === O.params.slidesPerView || 1 < O.params.slidesPerView) && O.isEnd && !O.params.centeredSlides ? O.slideTo(O.slides.length - 1, 0, !1, !0) : O.slideTo(O.activeIndex, 0, !1, !0), !Us && Vs())
            } else O.params.autoHeight && O.updateAutoHeight()
          }
        }, O.onResize = function(js) {
          O.params.onBeforeResize && O.params.onBeforeResize(O), O.params.breakpoints && O.setBreakpoint();
          var Vs = O.params.allowSwipeToPrev,
            Zs = O.params.allowSwipeToNext;
          O.params.allowSwipeToPrev = O.params.allowSwipeToNext = !0, O.updateContainerSize(), O.updateSlidesSize(), ('auto' === O.params.slidesPerView || O.params.freeMode || js) && O.updatePagination(), O.params.scrollbar && O.scrollbar && O.scrollbar.set(), O.controller && O.controller.spline && (O.controller.spline = void 0);
          var Us = !1;
          if (O.params.freeMode) {
            var Ks = Math.min(Math.max(O.translate, O.maxTranslate()), O.minTranslate());
            O.setWrapperTranslate(Ks), O.updateActiveIndex(), O.updateClasses(), O.params.autoHeight && O.updateAutoHeight()
          } else O.updateClasses(), Us = ('auto' === O.params.slidesPerView || 1 < O.params.slidesPerView) && O.isEnd && !O.params.centeredSlides ? O.slideTo(O.slides.length - 1, 0, !1, !0) : O.slideTo(O.activeIndex, 0, !1, !0);
          O.params.lazyLoading && !Us && O.lazy && O.lazy.load(), O.params.allowSwipeToPrev = Vs, O.params.allowSwipeToNext = Zs, O.params.onAfterResize && O.params.onAfterResize(O)
        }, O.touchEventsDesktop = {
          start: 'mousedown',
          move: 'mousemove',
          end: 'mouseup'
        }, window.navigator.pointerEnabled ? O.touchEventsDesktop = {
          start: 'pointerdown',
          move: 'pointermove',
          end: 'pointerup'
        } : window.navigator.msPointerEnabled && (O.touchEventsDesktop = {
          start: 'MSPointerDown',
          move: 'MSPointerMove',
          end: 'MSPointerUp'
        }), O.touchEvents = {
          start: O.support.touch || !O.params.simulateTouch ? 'touchstart' : O.touchEventsDesktop.start,
          move: O.support.touch || !O.params.simulateTouch ? 'touchmove' : O.touchEventsDesktop.move,
          end: O.support.touch || !O.params.simulateTouch ? 'touchend' : O.touchEventsDesktop.end
        }, (window.navigator.pointerEnabled || window.navigator.msPointerEnabled) && ('container' === O.params.touchEventsTarget ? O.container : O.wrapper).addClass('swiper-wp8-' + O.params.direction), O.initEvents = function(js) {
          var Vs = js ? 'off' : 'on',
            Zs = js ? 'removeEventListener' : 'addEventListener',
            Us = 'container' === O.params.touchEventsTarget ? O.container[0] : O.wrapper[0],
            Ks = O.support.touch ? Us : document,
            Qs = !!O.params.nested;
          if (O.browser.ie) Us[Zs](O.touchEvents.start, O.onTouchStart, !1), Ks[Zs](O.touchEvents.move, O.onTouchMove, Qs), Ks[Zs](O.touchEvents.end, O.onTouchEnd, !1);
          else {
            if (O.support.touch) {
              var $s = 'touchstart' === O.touchEvents.start && O.support.passiveListener && O.params.passiveListeners && {
                passive: !0,
                capture: !1
              };
              Us[Zs](O.touchEvents.start, O.onTouchStart, $s), Us[Zs](O.touchEvents.move, O.onTouchMove, Qs), Us[Zs](O.touchEvents.end, O.onTouchEnd, $s)
            }(d.simulateTouch && !O.device.ios && !O.device.android || d.simulateTouch && !O.support.touch && O.device.ios) && (Us[Zs]('mousedown', O.onTouchStart, !1), document[Zs]('mousemove', O.onTouchMove, Qs), document[Zs]('mouseup', O.onTouchEnd, !1))
          }
          window[Zs]('resize', O.onResize), O.params.nextButton && O.nextButton && 0 < O.nextButton.length && (O.nextButton[Vs]('click', O.onClickNext), O.params.a11y && O.a11y && O.nextButton[Vs]('keydown', O.a11y.onEnterKey)), O.params.prevButton && O.prevButton && 0 < O.prevButton.length && (O.prevButton[Vs]('click', O.onClickPrev), O.params.a11y && O.a11y && O.prevButton[Vs]('keydown', O.a11y.onEnterKey)), O.params.pagination && O.params.paginationClickable && (O.paginationContainer[Vs]('click', '.' + O.params.bulletClass, O.onClickIndex), O.params.a11y && O.a11y && O.paginationContainer[Vs]('keydown', '.' + O.params.bulletClass, O.a11y.onEnterKey)), (O.params.preventClicks || O.params.preventClicksPropagation) && Us[Zs]('click', O.preventClicks, !0)
        }, O.attachEvents = function() {
          O.initEvents()
        }, O.detachEvents = function() {
          O.initEvents(!0)
        }, O.allowClick = !0, O.preventClicks = function(js) {
          O.allowClick || (O.params.preventClicks && js.preventDefault(), O.params.preventClicksPropagation && O.animating && (js.stopPropagation(), js.stopImmediatePropagation()))
        }, O.onClickNext = function(js) {
          js.preventDefault(), O.isEnd && !O.params.loop || O.slideNext()
        }, O.onClickPrev = function(js) {
          js.preventDefault(), O.isBeginning && !O.params.loop || O.slidePrev()
        }, O.onClickIndex = function(js) {
          js.preventDefault();
          var Vs = a(this).index() * O.params.slidesPerGroup;
          O.params.loop && (Vs += O.loopedSlides), O.slideTo(Vs)
        }, O.updateClickedSlide = function(js) {
          var Vs = c(js, '.' + O.params.slideClass),
            Zs = !1;
          if (Vs)
            for (var Us = 0; Us < O.slides.length; Us++) O.slides[Us] === Vs && (Zs = !0);
          if (Vs && Zs) O.clickedSlide = Vs, O.clickedIndex = a(Vs).index();
          else return O.clickedSlide = void 0, void(O.clickedIndex = void 0);
          if (O.params.slideToClickedSlide && void 0 !== O.clickedIndex && O.clickedIndex !== O.activeIndex) {
            var $s, Ks = O.clickedIndex,
              Qs = 'auto' === O.params.slidesPerView ? O.currentSlidesPerView() : O.params.slidesPerView;
            if (O.params.loop) {
              if (O.animating) return;
              $s = parseInt(a(O.clickedSlide).attr('data-swiper-slide-index'), 10), O.params.centeredSlides ? Ks < O.loopedSlides - Qs / 2 || Ks > O.slides.length - O.loopedSlides + Qs / 2 ? (O.fixLoop(), Ks = O.wrapper.children('.' + O.params.slideClass + '[data-swiper-slide-index="' + $s + '"]:not(.' + O.params.slideDuplicateClass + ')').eq(0).index(), setTimeout(function() {
                O.slideTo(Ks)
              }, 0)) : O.slideTo(Ks) : Ks > O.slides.length - Qs ? (O.fixLoop(), Ks = O.wrapper.children('.' + O.params.slideClass + '[data-swiper-slide-index="' + $s + '"]:not(.' + O.params.slideDuplicateClass + ')').eq(0).index(), setTimeout(function() {
                O.slideTo(Ks)
              }, 0)) : O.slideTo(Ks)
            } else O.slideTo(Ks)
          }
        };
        var ws, _s, Os, Rs, Ms, Bs, Ws, Hs, Xs, Fs, j = 'input, select, textarea, button, video',
          pt = Date.now(),
          ua = [];
        O.animating = !1, O.touches = {
          startX: 0,
          startY: 0,
          currentX: 0,
          currentY: 0,
          diff: 0
        };
        var Ns, Gs;
        for (var Ys in O.onTouchStart = function(js) {
            if (js.originalEvent && (js = js.originalEvent), Ns = 'touchstart' === js.type, Ns || !('which' in js) || 3 !== js.which) {
              if (O.params.noSwiping && c(js, '.' + O.params.noSwipingClass)) return void(O.allowClick = !0);
              if (!O.params.swipeHandler || c(js, O.params.swipeHandler)) {
                var Vs = O.touches.currentX = 'touchstart' === js.type ? js.targetTouches[0].pageX : js.pageX,
                  Zs = O.touches.currentY = 'touchstart' === js.type ? js.targetTouches[0].pageY : js.pageY;
                if (!(O.device.ios && O.params.iOSEdgeSwipeDetection && Vs <= O.params.iOSEdgeSwipeThreshold)) {
                  if (ws = !0, _s = !1, Os = !0, Ms = void 0, Gs = void 0, O.touches.startX = Vs, O.touches.startY = Zs, Rs = Date.now(), O.allowClick = !0, O.updateContainerSize(), O.swipeDirection = void 0, 0 < O.params.threshold && (Hs = !1), 'touchstart' !== js.type) {
                    var Us = !0;
                    a(js.target).is(j) && (Us = !1), document.activeElement && a(document.activeElement).is(j) && document.activeElement.blur(), Us && js.preventDefault()
                  }
                  O.emit('onTouchStart', O, js)
                }
              }
            }
          }, O.onTouchMove = function(js) {
            if (js.originalEvent && (js = js.originalEvent), !(Ns && 'mousemove' === js.type)) {
              if (js.preventedByNestedSwiper) return O.touches.startX = 'touchmove' === js.type ? js.targetTouches[0].pageX : js.pageX, void(O.touches.startY = 'touchmove' === js.type ? js.targetTouches[0].pageY : js.pageY);
              if (O.params.onlyExternal) return O.allowClick = !1, void(ws && (O.touches.startX = O.touches.currentX = 'touchmove' === js.type ? js.targetTouches[0].pageX : js.pageX, O.touches.startY = O.touches.currentY = 'touchmove' === js.type ? js.targetTouches[0].pageY : js.pageY, Rs = Date.now()));
              if (Ns && O.params.touchReleaseOnEdges && !O.params.loop)
                if (!O.isHorizontal()) {
                  if (O.touches.currentY < O.touches.startY && O.translate <= O.maxTranslate() || O.touches.currentY > O.touches.startY && O.translate >= O.minTranslate()) return;
                } else if (O.touches.currentX < O.touches.startX && O.translate <= O.maxTranslate() || O.touches.currentX > O.touches.startX && O.translate >= O.minTranslate()) return;
              if (Ns && document.activeElement && js.target === document.activeElement && a(js.target).is(j)) return _s = !0, void(O.allowClick = !1);
              if (Os && O.emit('onTouchMove', O, js), !(js.targetTouches && 1 < js.targetTouches.length)) {
                if (O.touches.currentX = 'touchmove' === js.type ? js.targetTouches[0].pageX : js.pageX, O.touches.currentY = 'touchmove' === js.type ? js.targetTouches[0].pageY : js.pageY, 'undefined' == typeof Ms) {
                  var Vs;
                  O.isHorizontal() && O.touches.currentY === O.touches.startY || !O.isHorizontal() && O.touches.currentX === O.touches.startX ? Ms = !1 : (Vs = 180 * Math.atan2(Math.abs(O.touches.currentY - O.touches.startY), Math.abs(O.touches.currentX - O.touches.startX)) / Math.PI, Ms = O.isHorizontal() ? Vs > O.params.touchAngle : 90 - Vs > O.params.touchAngle)
                }
                if (Ms && O.emit('onTouchMoveOpposite', O, js), 'undefined' == typeof Gs && (O.touches.currentX !== O.touches.startX || O.touches.currentY !== O.touches.startY) && (Gs = !0), !!ws) {
                  if (Ms) return void(ws = !1);
                  if (Gs) {
                    O.allowClick = !1, O.emit('onSliderMove', O, js), js.preventDefault(), O.params.touchMoveStopPropagation && !O.params.nested && js.stopPropagation(), _s || (d.loop && O.fixLoop(), Ws = O.getWrapperTranslate(), O.setWrapperTransition(0), O.animating && O.wrapper.trigger('webkitTransitionEnd transitionend oTransitionEnd MSTransitionEnd msTransitionEnd'), O.params.autoplay && O.autoplaying && (O.params.autoplayDisableOnInteraction ? O.stopAutoplay() : O.pauseAutoplay()), Fs = !1, O.params.grabCursor && (!0 === O.params.allowSwipeToNext || !0 === O.params.allowSwipeToPrev) && O.setGrabCursor(!0)), _s = !0;
                    var Zs = O.touches.diff = O.isHorizontal() ? O.touches.currentX - O.touches.startX : O.touches.currentY - O.touches.startY;
                    Zs *= O.params.touchRatio, O.rtl && (Zs = -Zs), O.swipeDirection = 0 < Zs ? 'prev' : 'next', Bs = Zs + Ws;
                    var Us = !0;
                    if (0 < Zs && Bs > O.minTranslate() ? (Us = !1, O.params.resistance && (Bs = O.minTranslate() - 1 + Math.pow(-O.minTranslate() + Ws + Zs, O.params.resistanceRatio))) : 0 > Zs && Bs < O.maxTranslate() && (Us = !1, O.params.resistance && (Bs = O.maxTranslate() + 1 - Math.pow(O.maxTranslate() - Ws - Zs, O.params.resistanceRatio))), Us && (js.preventedByNestedSwiper = !0), !O.params.allowSwipeToNext && 'next' === O.swipeDirection && Bs < Ws && (Bs = Ws), !O.params.allowSwipeToPrev && 'prev' === O.swipeDirection && Bs > Ws && (Bs = Ws), 0 < O.params.threshold) {
                      if (!(Math.abs(Zs) > O.params.threshold || Hs)) return void(Bs = Ws);
                      if (!Hs) return Hs = !0, O.touches.startX = O.touches.currentX, O.touches.startY = O.touches.currentY, Bs = Ws, void(O.touches.diff = O.isHorizontal() ? O.touches.currentX - O.touches.startX : O.touches.currentY - O.touches.startY)
                    }
                    O.params.followFinger && ((O.params.freeMode || O.params.watchSlidesProgress) && O.updateActiveIndex(), O.params.freeMode && (0 === ua.length && ua.push({
                      position: O.touches[O.isHorizontal() ? 'startX' : 'startY'],
                      time: Rs
                    }), ua.push({
                      position: O.touches[O.isHorizontal() ? 'currentX' : 'currentY'],
                      time: new window.Date().getTime()
                    })), O.updateProgress(Bs), O.setWrapperTranslate(Bs))
                  }
                }
              }
            }
          }, O.onTouchEnd = function(js) {
            if (js.originalEvent && (js = js.originalEvent), Os && O.emit('onTouchEnd', O, js), Os = !1, !!ws) {
              O.params.grabCursor && _s && ws && (!0 === O.params.allowSwipeToNext || !0 === O.params.allowSwipeToPrev) && O.setGrabCursor(!1);
              var Vs = Date.now(),
                Zs = Vs - Rs;
              if (O.allowClick && (O.updateClickedSlide(js), O.emit('onTap', O, js), 300 > Zs && 300 < Vs - pt && (Xs && clearTimeout(Xs), Xs = setTimeout(function() {
                  O && (O.params.paginationHide && 0 < O.paginationContainer.length && !a(js.target).hasClass(O.params.bulletClass) && O.paginationContainer.toggleClass(O.params.paginationHiddenClass), O.emit('onClick', O, js))
                }, 300)), 300 > Zs && 300 > Vs - pt && (Xs && clearTimeout(Xs), O.emit('onDoubleTap', O, js))), pt = Date.now(), setTimeout(function() {
                  O && (O.allowClick = !0)
                }, 0), !ws || !_s || !O.swipeDirection || 0 === O.touches.diff || Bs === Ws) return void(ws = _s = !1);
              ws = _s = !1;
              var Us;
              if (Us = O.params.followFinger ? O.rtl ? O.translate : -O.translate : -Bs, O.params.freeMode) {
                if (Us < -O.minTranslate()) return void O.slideTo(O.activeIndex);
                if (Us > -O.maxTranslate()) return void(O.slides.length < O.snapGrid.length ? O.slideTo(O.snapGrid.length - 1) : O.slideTo(O.slides.length - 1));
                if (O.params.freeModeMomentum) {
                  if (1 < ua.length) {
                    var Ks = ua.pop(),
                      Qs = ua.pop(),
                      $s = Ks.position - Qs.position,
                      Js = Ks.time - Qs.time;
                    O.velocity = $s / Js, O.velocity /= 2, Math.abs(O.velocity) < O.params.freeModeMinimumVelocity && (O.velocity = 0), (150 < Js || 300 < new window.Date().getTime() - Ks.time) && (O.velocity = 0)
                  } else O.velocity = 0;
                  O.velocity *= O.params.freeModeMomentumVelocityRatio, ua.length = 0;
                  var ei = 1e3 * O.params.freeModeMomentumRatio,
                    ti = O.velocity * ei,
                    ai = O.translate + ti;
                  O.rtl && (ai = -ai);
                  var ri, si = !1,
                    ii = 20 * Math.abs(O.velocity) * O.params.freeModeMomentumBounceRatio;
                  if (ai < O.maxTranslate()) O.params.freeModeMomentumBounce ? (ai + O.maxTranslate() < -ii && (ai = O.maxTranslate() - ii), ri = O.maxTranslate(), si = !0, Fs = !0) : ai = O.maxTranslate();
                  else if (ai > O.minTranslate()) O.params.freeModeMomentumBounce ? (ai - O.minTranslate() > ii && (ai = O.minTranslate() + ii), ri = O.minTranslate(), si = !0, Fs = !0) : ai = O.minTranslate();
                  else if (O.params.freeModeSticky) {
                    var ni, oi = 0;
                    for (oi = 0; oi < O.snapGrid.length; oi += 1)
                      if (O.snapGrid[oi] > -ai) {
                        ni = oi;
                        break
                      }
                    ai = Math.abs(O.snapGrid[ni] - ai) < Math.abs(O.snapGrid[ni - 1] - ai) || 'next' === O.swipeDirection ? O.snapGrid[ni] : O.snapGrid[ni - 1], O.rtl || (ai = -ai)
                  }
                  if (0 !== O.velocity) ei = O.rtl ? Math.abs((-ai - O.translate) / O.velocity) : Math.abs((ai - O.translate) / O.velocity);
                  else if (O.params.freeModeSticky) return void O.slideReset();
                  O.params.freeModeMomentumBounce && si ? (O.updateProgress(ri), O.setWrapperTransition(ei), O.setWrapperTranslate(ai), O.onTransitionStart(), O.animating = !0, O.wrapper.transitionEnd(function() {
                    O && Fs && (O.emit('onMomentumBounce', O), O.setWrapperTransition(O.params.speed), O.setWrapperTranslate(ri), O.wrapper.transitionEnd(function() {
                      O && O.onTransitionEnd()
                    }))
                  })) : O.velocity ? (O.updateProgress(ai), O.setWrapperTransition(ei), O.setWrapperTranslate(ai), O.onTransitionStart(), !O.animating && (O.animating = !0, O.wrapper.transitionEnd(function() {
                    O && O.onTransitionEnd()
                  }))) : O.updateProgress(ai), O.updateActiveIndex()
                }
                return void((!O.params.freeModeMomentum || Zs >= O.params.longSwipesMs) && (O.updateProgress(), O.updateActiveIndex()))
              }
              var pi, li = 0,
                di = O.slidesSizesGrid[0];
              for (pi = 0; pi < O.slidesGrid.length; pi += O.params.slidesPerGroup) 'undefined' == typeof O.slidesGrid[pi + O.params.slidesPerGroup] ? Us >= O.slidesGrid[pi] && (li = pi, di = O.slidesGrid[O.slidesGrid.length - 1] - O.slidesGrid[O.slidesGrid.length - 2]) : Us >= O.slidesGrid[pi] && Us < O.slidesGrid[pi + O.params.slidesPerGroup] && (li = pi, di = O.slidesGrid[pi + O.params.slidesPerGroup] - O.slidesGrid[pi]);
              var mi = (Us - O.slidesGrid[li]) / di;
              if (Zs > O.params.longSwipesMs) {
                if (!O.params.longSwipes) return void O.slideTo(O.activeIndex);
                'next' === O.swipeDirection && (mi >= O.params.longSwipesRatio ? O.slideTo(li + O.params.slidesPerGroup) : O.slideTo(li)), 'prev' === O.swipeDirection && (mi > 1 - O.params.longSwipesRatio ? O.slideTo(li + O.params.slidesPerGroup) : O.slideTo(li))
              } else {
                if (!O.params.shortSwipes) return void O.slideTo(O.activeIndex);
                'next' === O.swipeDirection && O.slideTo(li + O.params.slidesPerGroup), 'prev' === O.swipeDirection && O.slideTo(li)
              }
            }
          }, O._slideTo = function(js, Vs) {
            return O.slideTo(js, Vs, !0, !0)
          }, O.slideTo = function(js, Vs, Zs, Us) {
            'undefined' == typeof Zs && (Zs = !0), 'undefined' == typeof js && (js = 0), 0 > js && (js = 0), O.snapIndex = Math.floor(js / O.params.slidesPerGroup), O.snapIndex >= O.snapGrid.length && (O.snapIndex = O.snapGrid.length - 1);
            var Ks = -O.snapGrid[O.snapIndex];
            if (O.params.autoplay && O.autoplaying && (Us || !O.params.autoplayDisableOnInteraction ? O.pauseAutoplay(Vs) : O.stopAutoplay()), O.updateProgress(Ks), O.params.normalizeSlideIndex)
              for (var Qs = 0; Qs < O.slidesGrid.length; Qs++) - Math.floor(100 * Ks) >= Math.floor(100 * O.slidesGrid[Qs]) && (js = Qs);
            return !O.params.allowSwipeToNext && Ks < O.translate && Ks < O.minTranslate() ? !1 : !O.params.allowSwipeToPrev && Ks > O.translate && Ks > O.maxTranslate() && (O.activeIndex || 0) !== js ? !1 : ('undefined' == typeof Vs && (Vs = O.params.speed), O.previousIndex = O.activeIndex || 0, O.activeIndex = js, O.updateRealIndex(), O.rtl && -Ks === O.translate || !O.rtl && Ks === O.translate) ? (O.params.autoHeight && O.updateAutoHeight(), O.updateClasses(), 'slide' !== O.params.effect && O.setWrapperTranslate(Ks), !1) : (O.updateClasses(), O.onTransitionStart(Zs), 0 === Vs || O.browser.lteIE9 ? (O.setWrapperTranslate(Ks), O.setWrapperTransition(0), O.onTransitionEnd(Zs)) : (O.setWrapperTranslate(Ks), O.setWrapperTransition(Vs), !O.animating && (O.animating = !0, O.wrapper.transitionEnd(function() {
              O && O.onTransitionEnd(Zs)
            }))), !0)
          }, O.onTransitionStart = function(js) {
            'undefined' == typeof js && (js = !0), O.params.autoHeight && O.updateAutoHeight(), O.lazy && O.lazy.onTransitionStart(), js && (O.emit('onTransitionStart', O), O.activeIndex !== O.previousIndex && (O.emit('onSlideChangeStart', O), O.activeIndex > O.previousIndex ? O.emit('onSlideNextStart', O) : O.emit('onSlidePrevStart', O)))
          }, O.onTransitionEnd = function(js) {
            O.animating = !1, O.setWrapperTransition(0), 'undefined' == typeof js && (js = !0), O.lazy && O.lazy.onTransitionEnd(), js && (O.emit('onTransitionEnd', O), O.activeIndex !== O.previousIndex && (O.emit('onSlideChangeEnd', O), O.activeIndex > O.previousIndex ? O.emit('onSlideNextEnd', O) : O.emit('onSlidePrevEnd', O))), O.params.history && O.history && O.history.setHistory(O.params.history, O.activeIndex), O.params.hashnav && O.hashnav && O.hashnav.setHash()
          }, O.slideNext = function(js, Vs, Zs) {
            return O.params.loop ? !O.animating && (O.fixLoop(), O.container[0].clientLeft, O.slideTo(O.activeIndex + O.params.slidesPerGroup, Vs, js, Zs)) : O.slideTo(O.activeIndex + O.params.slidesPerGroup, Vs, js, Zs)
          }, O._slideNext = function(js) {
            return O.slideNext(!0, js, !0)
          }, O.slidePrev = function(js, Vs, Zs) {
            return O.params.loop ? !O.animating && (O.fixLoop(), O.container[0].clientLeft, O.slideTo(O.activeIndex - 1, Vs, js, Zs)) : O.slideTo(O.activeIndex - 1, Vs, js, Zs)
          }, O._slidePrev = function(js) {
            return O.slidePrev(!0, js, !0)
          }, O.slideReset = function(js, Vs) {
            return O.slideTo(O.activeIndex, Vs, js)
          }, O.disableTouchControl = function() {
            return O.params.onlyExternal = !0, !0
          }, O.enableTouchControl = function() {
            return O.params.onlyExternal = !1, !0
          }, O.setWrapperTransition = function(js, Vs) {
            O.wrapper.transition(js), 'slide' !== O.params.effect && O.effects[O.params.effect] && O.effects[O.params.effect].setTransition(js), O.params.parallax && O.parallax && O.parallax.setTransition(js), O.params.scrollbar && O.scrollbar && O.scrollbar.setTransition(js), O.params.control && O.controller && O.controller.setTransition(js, Vs), O.emit('onSetTransition', O, js)
          }, O.setWrapperTranslate = function(js, Vs, Zs) {
            var Us = 0,
              Ks = 0;
            O.isHorizontal() ? Us = O.rtl ? -js : js : Ks = js, O.params.roundLengths && (Us = p(Us), Ks = p(Ks)), O.params.virtualTranslate || (O.support.transforms3d ? O.wrapper.transform('translate3d(' + Us + 'px, ' + Ks + 'px, ' + 0 + 'px)') : O.wrapper.transform('translate(' + Us + 'px, ' + Ks + 'px)')), O.translate = O.isHorizontal() ? Us : Ks;
            var $s, Qs = O.maxTranslate() - O.minTranslate();
            $s = 0 == Qs ? 0 : (js - O.minTranslate()) / Qs, $s !== O.progress && O.updateProgress(js), Vs && O.updateActiveIndex(), 'slide' !== O.params.effect && O.effects[O.params.effect] && O.effects[O.params.effect].setTranslate(O.translate), O.params.parallax && O.parallax && O.parallax.setTranslate(O.translate), O.params.scrollbar && O.scrollbar && O.scrollbar.setTranslate(O.translate), O.params.control && O.controller && O.controller.setTranslate(O.translate, Zs), O.emit('onSetTranslate', O, O.translate)
          }, O.getTranslate = function(js, Vs) {
            var Zs, Us, Ks, Qs;
            return ('undefined' == typeof Vs && (Vs = 'x'), O.params.virtualTranslate) ? O.rtl ? -O.translate : O.translate : (Ks = window.getComputedStyle(js, null), window.WebKitCSSMatrix ? (Us = Ks.transform || Ks.webkitTransform, 6 < Us.split(',').length && (Us = Us.split(', ').map(function($s) {
              return $s.replace(',', '.')
            }).join(', ')), Qs = new window.WebKitCSSMatrix('none' === Us ? '' : Us)) : (Qs = Ks.MozTransform || Ks.OTransform || Ks.MsTransform || Ks.msTransform || Ks.transform || Ks.getPropertyValue('transform').replace('translate(', 'matrix(1, 0, 0, 1,'), Zs = Qs.toString().split(',')), 'x' === Vs && (window.WebKitCSSMatrix ? Us = Qs.m41 : 16 === Zs.length ? Us = parseFloat(Zs[12]) : Us = parseFloat(Zs[4])), 'y' === Vs && (window.WebKitCSSMatrix ? Us = Qs.m42 : 16 === Zs.length ? Us = parseFloat(Zs[13]) : Us = parseFloat(Zs[5])), O.rtl && Us && (Us = -Us), Us || 0)
          }, O.getWrapperTranslate = function(js) {
            return 'undefined' == typeof js && (js = O.isHorizontal() ? 'x' : 'y'), O.getTranslate(O.wrapper[0], js)
          }, O.observers = [], O.initObservers = function() {
            if (O.params.observeParents)
              for (var js = O.container.parents(), Vs = 0; Vs < js.length; Vs++) u(js[Vs]);
            u(O.container[0], {
              childList: !1
            }), u(O.wrapper[0], {
              attributes: !1
            })
          }, O.disconnectObservers = function() {
            for (var js = 0; js < O.observers.length; js++) O.observers[js].disconnect();
            O.observers = []
          }, O.createLoop = function() {
            O.wrapper.children('.' + O.params.slideClass + '.' + O.params.slideDuplicateClass).remove();
            var js = O.wrapper.children('.' + O.params.slideClass);
            'auto' !== O.params.slidesPerView || O.params.loopedSlides || (O.params.loopedSlides = js.length), O.loopedSlides = parseInt(O.params.loopedSlides || O.params.slidesPerView, 10), O.loopedSlides += O.params.loopAdditionalSlides, O.loopedSlides > js.length && (O.loopedSlides = js.length);
            var Us, Vs = [],
              Zs = [];
            for (js.each(function(Ks, Qs) {
                var $s = a(this);
                Ks < O.loopedSlides && Zs.push(Qs), Ks < js.length && Ks >= js.length - O.loopedSlides && Vs.push(Qs), $s.attr('data-swiper-slide-index', Ks)
              }), Us = 0; Us < Zs.length; Us++) O.wrapper.append(a(Zs[Us].cloneNode(!0)).addClass(O.params.slideDuplicateClass));
            for (Us = Vs.length - 1; 0 <= Us; Us--) O.wrapper.prepend(a(Vs[Us].cloneNode(!0)).addClass(O.params.slideDuplicateClass))
          }, O.destroyLoop = function() {
            O.wrapper.children('.' + O.params.slideClass + '.' + O.params.slideDuplicateClass).remove(), O.slides.removeAttr('data-swiper-slide-index')
          }, O.reLoop = function(js) {
            var Vs = O.activeIndex - O.loopedSlides;
            O.destroyLoop(), O.createLoop(), O.updateSlidesSize(), js && O.slideTo(Vs + O.loopedSlides, 0, !1)
          }, O.fixLoop = function() {
            var js;
            O.activeIndex < O.loopedSlides ? (js = O.slides.length - 3 * O.loopedSlides + O.activeIndex, js += O.loopedSlides, O.slideTo(js, 0, !1, !0)) : ('auto' === O.params.slidesPerView && O.activeIndex >= 2 * O.loopedSlides || O.activeIndex > O.slides.length - 2 * O.params.slidesPerView) && (js = -O.slides.length + O.activeIndex + O.loopedSlides, js += O.loopedSlides, O.slideTo(js, 0, !1, !0))
          }, O.appendSlide = function(js) {
            if (O.params.loop && O.destroyLoop(), 'object' == typeof js && js.length)
              for (var Vs = 0; Vs < js.length; Vs++) js[Vs] && O.wrapper.append(js[Vs]);
            else O.wrapper.append(js);
            O.params.loop && O.createLoop(), O.params.observer && O.support.observer || O.update(!0)
          }, O.prependSlide = function(js) {
            O.params.loop && O.destroyLoop();
            var Vs = O.activeIndex + 1;
            if ('object' == typeof js && js.length) {
              for (var Zs = 0; Zs < js.length; Zs++) js[Zs] && O.wrapper.prepend(js[Zs]);
              Vs = O.activeIndex + js.length
            } else O.wrapper.prepend(js);
            O.params.loop && O.createLoop(), O.params.observer && O.support.observer || O.update(!0), O.slideTo(Vs, 0, !1)
          }, O.removeSlide = function(js) {
            O.params.loop && (O.destroyLoop(), O.slides = O.wrapper.children('.' + O.params.slideClass));
            var Zs, Vs = O.activeIndex;
            if ('object' == typeof js && js.length) {
              for (var Us = 0; Us < js.length; Us++) Zs = js[Us], O.slides[Zs] && O.slides.eq(Zs).remove(), Zs < Vs && Vs--;
              Vs = Math.max(Vs, 0)
            } else Zs = js, O.slides[Zs] && O.slides.eq(Zs).remove(), Zs < Vs && Vs--, Vs = Math.max(Vs, 0);
            O.params.loop && O.createLoop(), O.params.observer && O.support.observer || O.update(!0), O.params.loop ? O.slideTo(Vs + O.loopedSlides, 0, !1) : O.slideTo(Vs, 0, !1)
          }, O.removeAllSlides = function() {
            for (var js = [], Vs = 0; Vs < O.slides.length; Vs++) js.push(Vs);
            O.removeSlide(js)
          }, O.effects = {
            fade: {
              setTranslate: function() {
                for (var js = 0; js < O.slides.length; js++) {
                  var Vs = O.slides.eq(js),
                    Zs = Vs[0].swiperSlideOffset,
                    Us = -Zs;
                  O.params.virtualTranslate || (Us -= O.translate);
                  var Ks = 0;
                  O.isHorizontal() || (Ks = Us, Us = 0);
                  var Qs = O.params.fade.crossFade ? Math.max(1 - Math.abs(Vs[0].progress), 0) : 1 + Math.min(Math.max(Vs[0].progress, -1), 0);
                  Vs.css({
                    opacity: Qs
                  }).transform('translate3d(' + Us + 'px, ' + Ks + 'px, 0px)')
                }
              },
              setTransition: function(js) {
                if (O.slides.transition(js), O.params.virtualTranslate && 0 !== js) {
                  var Vs = !1;
                  O.slides.transitionEnd(function() {
                    if (!Vs && O) {
                      Vs = !0, O.animating = !1;
                      for (var Zs = ['webkitTransitionEnd', 'transitionend', 'oTransitionEnd', 'MSTransitionEnd', 'msTransitionEnd'], Us = 0; Us < Zs.length; Us++) O.wrapper.trigger(Zs[Us])
                    }
                  })
                }
              }
            },
            flip: {
              setTranslate: function() {
                for (var js = 0; js < O.slides.length; js++) {
                  var Vs = O.slides.eq(js),
                    Zs = Vs[0].progress;
                  O.params.flip.limitRotation && (Zs = Math.max(Math.min(Vs[0].progress, 1), -1));
                  var Us = Vs[0].swiperSlideOffset,
                    Ks = -180 * Zs,
                    Qs = Ks,
                    $s = 0,
                    Js = -Us,
                    ei = 0;
                  if (O.isHorizontal() ? O.rtl && (Qs = -Qs) : (ei = Js, Js = 0, $s = -Qs, Qs = 0), Vs[0].style.zIndex = -Math.abs(Math.round(Zs)) + O.slides.length, O.params.flip.slideShadows) {
                    var ti = O.isHorizontal() ? Vs.find('.swiper-slide-shadow-left') : Vs.find('.swiper-slide-shadow-top'),
                      ai = O.isHorizontal() ? Vs.find('.swiper-slide-shadow-right') : Vs.find('.swiper-slide-shadow-bottom');
                    0 === ti.length && (ti = a('<div class="swiper-slide-shadow-' + (O.isHorizontal() ? 'left' : 'top') + '"></div>'), Vs.append(ti)), 0 === ai.length && (ai = a('<div class="swiper-slide-shadow-' + (O.isHorizontal() ? 'right' : 'bottom') + '"></div>'), Vs.append(ai)), ti.length && (ti[0].style.opacity = Math.max(-Zs, 0)), ai.length && (ai[0].style.opacity = Math.max(Zs, 0))
                  }
                  Vs.transform('translate3d(' + Js + 'px, ' + ei + 'px, 0px) rotateX(' + $s + 'deg) rotateY(' + Qs + 'deg)')
                }
              },
              setTransition: function(js) {
                if (O.slides.transition(js).find('.swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left').transition(js), O.params.virtualTranslate && 0 !== js) {
                  var Vs = !1;
                  O.slides.eq(O.activeIndex).transitionEnd(function() {
                    if (!Vs && O && a(this).hasClass(O.params.slideActiveClass)) {
                      Vs = !0, O.animating = !1;
                      for (var Zs = ['webkitTransitionEnd', 'transitionend', 'oTransitionEnd', 'MSTransitionEnd', 'msTransitionEnd'], Us = 0; Us < Zs.length; Us++) O.wrapper.trigger(Zs[Us])
                    }
                  })
                }
              }
            },
            cube: {
              setTranslate: function() {
                var Vs, js = 0;
                O.params.cube.shadow && (O.isHorizontal() ? (Vs = O.wrapper.find('.swiper-cube-shadow'), 0 === Vs.length && (Vs = a('<div class="swiper-cube-shadow"></div>'), O.wrapper.append(Vs)), Vs.css({
                  height: O.width + 'px'
                })) : (Vs = O.container.find('.swiper-cube-shadow'), 0 === Vs.length && (Vs = a('<div class="swiper-cube-shadow"></div>'), O.container.append(Vs))));
                for (var Zs = 0; Zs < O.slides.length; Zs++) {
                  var Us = O.slides.eq(Zs),
                    Ks = 90 * Zs,
                    Qs = Math.floor(Ks / 360);
                  O.rtl && (Ks = -Ks, Qs = Math.floor(-Ks / 360));
                  var $s = Math.max(Math.min(Us[0].progress, 1), -1),
                    Js = 0,
                    ei = 0,
                    ti = 0;
                  0 == Zs % 4 ? (Js = 4 * -Qs * O.size, ti = 0) : 0 == (Zs - 1) % 4 ? (Js = 0, ti = 4 * -Qs * O.size) : 0 == (Zs - 2) % 4 ? (Js = O.size + 4 * Qs * O.size, ti = O.size) : 0 == (Zs - 3) % 4 && (Js = -O.size, ti = 3 * O.size + 4 * O.size * Qs), O.rtl && (Js = -Js), O.isHorizontal() || (ei = Js, Js = 0);
                  var ai = 'rotateX(' + (O.isHorizontal() ? 0 : -Ks) + 'deg) rotateY(' + (O.isHorizontal() ? Ks : 0) + 'deg) translate3d(' + Js + 'px, ' + ei + 'px, ' + ti + 'px)';
                  if (1 >= $s && -1 < $s && (js = 90 * Zs + 90 * $s, O.rtl && (js = 90 * -Zs - 90 * $s)), Us.transform(ai), O.params.cube.slideShadows) {
                    var si = O.isHorizontal() ? Us.find('.swiper-slide-shadow-left') : Us.find('.swiper-slide-shadow-top'),
                      ii = O.isHorizontal() ? Us.find('.swiper-slide-shadow-right') : Us.find('.swiper-slide-shadow-bottom');
                    0 === si.length && (si = a('<div class="swiper-slide-shadow-' + (O.isHorizontal() ? 'left' : 'top') + '"></div>'), Us.append(si)), 0 === ii.length && (ii = a('<div class="swiper-slide-shadow-' + (O.isHorizontal() ? 'right' : 'bottom') + '"></div>'), Us.append(ii)), si.length && (si[0].style.opacity = Math.max(-$s, 0)), ii.length && (ii[0].style.opacity = Math.max($s, 0))
                  }
                }
                if (O.wrapper.css({
                    '-webkit-transform-origin': '50% 50% -' + O.size / 2 + 'px',
                    '-moz-transform-origin': '50% 50% -' + O.size / 2 + 'px',
                    '-ms-transform-origin': '50% 50% -' + O.size / 2 + 'px',
                    'transform-origin': '50% 50% -' + O.size / 2 + 'px'
                  }), O.params.cube.shadow)
                  if (O.isHorizontal()) Vs.transform('translate3d(0px, ' + (O.width / 2 + O.params.cube.shadowOffset) + 'px, ' + -O.width / 2 + 'px) rotateX(90deg) rotateZ(0deg) scale(' + O.params.cube.shadowScale + ')');
                  else {
                    var ri = Math.abs(js) - 90 * Math.floor(Math.abs(js) / 90),
                      oi = 1.5 - (Math.sin(2 * ri * Math.PI / 360) / 2 + Math.cos(2 * ri * Math.PI / 360) / 2),
                      ni = O.params.cube.shadowScale,
                      li = O.params.cube.shadowScale / oi,
                      di = O.params.cube.shadowOffset;
                    Vs.transform('scale3d(' + ni + ', 1, ' + li + ') translate3d(0px, ' + (O.height / 2 + di) + 'px, ' + -O.height / 2 / li + 'px) rotateX(-90deg)')
                  }
                var pi = O.isSafari || O.isUiWebView ? -O.size / 2 : 0;
                O.wrapper.transform('translate3d(0px,0,' + pi + 'px) rotateX(' + (O.isHorizontal() ? 0 : js) + 'deg) rotateY(' + (O.isHorizontal() ? -js : 0) + 'deg)')
              },
              setTransition: function(js) {
                O.slides.transition(js).find('.swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left').transition(js), O.params.cube.shadow && !O.isHorizontal() && O.container.find('.swiper-cube-shadow').transition(js)
              }
            },
            coverflow: {
              setTranslate: function() {
                for (var js = O.translate, Vs = O.isHorizontal() ? -js + O.width / 2 : -js + O.height / 2, Zs = O.isHorizontal() ? O.params.coverflow.rotate : -O.params.coverflow.rotate, Us = O.params.coverflow.depth, Ks = 0, Qs = O.slides.length; Ks < Qs; Ks++) {
                  var $s = O.slides.eq(Ks),
                    Js = O.slidesSizesGrid[Ks],
                    ei = $s[0].swiperSlideOffset,
                    ti = (Vs - ei - Js / 2) / Js * O.params.coverflow.modifier,
                    ai = O.isHorizontal() ? Zs * ti : 0,
                    si = O.isHorizontal() ? 0 : Zs * ti,
                    ii = -Us * Math.abs(ti),
                    ri = O.isHorizontal() ? 0 : O.params.coverflow.stretch * ti,
                    oi = O.isHorizontal() ? O.params.coverflow.stretch * ti : 0;
                  1e-3 > Math.abs(oi) && (oi = 0), 1e-3 > Math.abs(ri) && (ri = 0), 1e-3 > Math.abs(ii) && (ii = 0), 1e-3 > Math.abs(ai) && (ai = 0), 1e-3 > Math.abs(si) && (si = 0);
                  var ni = 'translate3d(' + oi + 'px,' + ri + 'px,' + ii + 'px)  rotateX(' + si + 'deg) rotateY(' + ai + 'deg)';
                  if ($s.transform(ni), $s[0].style.zIndex = -Math.abs(Math.round(ti)) + 1, O.params.coverflow.slideShadows) {
                    var li = O.isHorizontal() ? $s.find('.swiper-slide-shadow-left') : $s.find('.swiper-slide-shadow-top'),
                      di = O.isHorizontal() ? $s.find('.swiper-slide-shadow-right') : $s.find('.swiper-slide-shadow-bottom');
                    0 === li.length && (li = a('<div class="swiper-slide-shadow-' + (O.isHorizontal() ? 'left' : 'top') + '"></div>'), $s.append(li)), 0 === di.length && (di = a('<div class="swiper-slide-shadow-' + (O.isHorizontal() ? 'right' : 'bottom') + '"></div>'), $s.append(di)), li.length && (li[0].style.opacity = 0 < ti ? ti : 0), di.length && (di[0].style.opacity = 0 < -ti ? -ti : 0)
                  }
                }
                if (O.browser.ie) {
                  var pi = O.wrapper[0].style;
                  pi.perspectiveOrigin = Vs + 'px 50%'
                }
              },
              setTransition: function(js) {
                O.slides.transition(js).find('.swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left').transition(js)
              }
            }
          }, O.lazy = {
            initialImageLoaded: !1,
            loadImageInSlide: function(js, Vs) {
              if ('undefined' != typeof js && ('undefined' == typeof Vs && (Vs = !0), 0 !== O.slides.length)) {
                var Zs = O.slides.eq(js),
                  Us = Zs.find('.' + O.params.lazyLoadingClass + ':not(.' + O.params.lazyStatusLoadedClass + '):not(.' + O.params.lazyStatusLoadingClass + ')');
                !Zs.hasClass(O.params.lazyLoadingClass) || Zs.hasClass(O.params.lazyStatusLoadedClass) || Zs.hasClass(O.params.lazyStatusLoadingClass) || (Us = Us.add(Zs[0])), 0 === Us.length || Us.each(function() {
                  var Ks = a(this);
                  Ks.addClass(O.params.lazyStatusLoadingClass);
                  var Qs = Ks.attr('data-background'),
                    $s = Ks.attr('data-src'),
                    Js = Ks.attr('data-srcset'),
                    ei = Ks.attr('data-sizes');
                  O.loadImage(Ks[0], $s || Qs, Js, ei, !1, function() {
                    if ('undefined' != typeof O && null !== O && O) {
                      if (Qs ? (Ks.css('background-image', 'url("' + Qs + '")'), Ks.removeAttr('data-background')) : (Js && (Ks.attr('srcset', Js), Ks.removeAttr('data-srcset')), ei && (Ks.attr('sizes', ei), Ks.removeAttr('data-sizes')), $s && (Ks.attr('src', $s), Ks.removeAttr('data-src'))), Ks.addClass(O.params.lazyStatusLoadedClass).removeClass(O.params.lazyStatusLoadingClass), Zs.find('.' + O.params.lazyPreloaderClass + ', .' + O.params.preloaderClass).remove(), O.params.loop && Vs) {
                        var ti = Zs.attr('data-swiper-slide-index');
                        if (Zs.hasClass(O.params.slideDuplicateClass)) {
                          var ai = O.wrapper.children('[data-swiper-slide-index="' + ti + '"]:not(.' + O.params.slideDuplicateClass + ')');
                          O.lazy.loadImageInSlide(ai.index(), !1)
                        } else {
                          var si = O.wrapper.children('.' + O.params.slideDuplicateClass + '[data-swiper-slide-index="' + ti + '"]');
                          O.lazy.loadImageInSlide(si.index(), !1)
                        }
                      }
                      O.emit('onLazyImageReady', O, Zs[0], Ks[0])
                    }
                  }), O.emit('onLazyImageLoad', O, Zs[0], Ks[0])
                })
              }
            },
            load: function() {
              var Vs, js = O.params.slidesPerView;
              if ('auto' === js && (js = 0), O.lazy.initialImageLoaded || (O.lazy.initialImageLoaded = !0), O.params.watchSlidesVisibility) O.wrapper.children('.' + O.params.slideVisibleClass).each(function() {
                O.lazy.loadImageInSlide(a(this).index())
              });
              else if (1 < js)
                for (Vs = O.activeIndex; Vs < O.activeIndex + js; Vs++) O.slides[Vs] && O.lazy.loadImageInSlide(Vs);
              else O.lazy.loadImageInSlide(O.activeIndex);
              if (O.params.lazyLoadingInPrevNext)
                if (1 < js || O.params.lazyLoadingInPrevNextAmount && 1 < O.params.lazyLoadingInPrevNextAmount) {
                  var Zs = O.params.lazyLoadingInPrevNextAmount,
                    Us = js,
                    Ks = Math.min(O.activeIndex + Us + Math.max(Zs, Us), O.slides.length),
                    Qs = Math.max(O.activeIndex - Math.max(Us, Zs), 0);
                  for (Vs = O.activeIndex + js; Vs < Ks; Vs++) O.slides[Vs] && O.lazy.loadImageInSlide(Vs);
                  for (Vs = Qs; Vs < O.activeIndex; Vs++) O.slides[Vs] && O.lazy.loadImageInSlide(Vs)
                } else {
                  var $s = O.wrapper.children('.' + O.params.slideNextClass);
                  0 < $s.length && O.lazy.loadImageInSlide($s.index());
                  var Js = O.wrapper.children('.' + O.params.slidePrevClass);
                  0 < Js.length && O.lazy.loadImageInSlide(Js.index())
                }
            },
            onTransitionStart: function() {
              O.params.lazyLoading && (O.params.lazyLoadingOnTransitionStart || !O.params.lazyLoadingOnTransitionStart && !O.lazy.initialImageLoaded) && O.lazy.load()
            },
            onTransitionEnd: function() {
              O.params.lazyLoading && !O.params.lazyLoadingOnTransitionStart && O.lazy.load()
            }
          }, O.scrollbar = {
            isTouched: !1,
            setDragPosition: function(js) {
              var Vs = O.scrollbar,
                Zs = O.isHorizontal() ? 'touchstart' === js.type || 'touchmove' === js.type ? js.targetTouches[0].pageX : js.pageX || js.clientX : 'touchstart' === js.type || 'touchmove' === js.type ? js.targetTouches[0].pageY : js.pageY || js.clientY,
                Us = Zs - Vs.track.offset()[O.isHorizontal() ? 'left' : 'top'] - Vs.dragSize / 2,
                Ks = -O.minTranslate() * Vs.moveDivider,
                Qs = -O.maxTranslate() * Vs.moveDivider;
              Us < Ks ? Us = Ks : Us > Qs && (Us = Qs), Us = -Us / Vs.moveDivider, O.updateProgress(Us), O.setWrapperTranslate(Us, !0)
            },
            dragStart: function(js) {
              var Vs = O.scrollbar;
              Vs.isTouched = !0, js.preventDefault(), js.stopPropagation(), Vs.setDragPosition(js), clearTimeout(Vs.dragTimeout), Vs.track.transition(0), O.params.scrollbarHide && Vs.track.css('opacity', 1), O.wrapper.transition(100), Vs.drag.transition(100), O.emit('onScrollbarDragStart', O)
            },
            dragMove: function(js) {
              var Vs = O.scrollbar;
              Vs.isTouched && (js.preventDefault ? js.preventDefault() : js.returnValue = !1, Vs.setDragPosition(js), O.wrapper.transition(0), Vs.track.transition(0), Vs.drag.transition(0), O.emit('onScrollbarDragMove', O))
            },
            dragEnd: function() {
              var js = O.scrollbar;
              js.isTouched && (js.isTouched = !1, O.params.scrollbarHide && (clearTimeout(js.dragTimeout), js.dragTimeout = setTimeout(function() {
                js.track.css('opacity', 0), js.track.transition(400)
              }, 1e3)), O.emit('onScrollbarDragEnd', O), O.params.scrollbarSnapOnRelease && O.slideReset())
            },
            draggableEvents: function() {
              return !1 !== O.params.simulateTouch || O.support.touch ? O.touchEvents : O.touchEventsDesktop
            }(),
            enableDraggable: function() {
              var js = O.scrollbar,
                Vs = O.support.touch ? js.track : document;
              a(js.track).on(js.draggableEvents.start, js.dragStart), a(Vs).on(js.draggableEvents.move, js.dragMove), a(Vs).on(js.draggableEvents.end, js.dragEnd)
            },
            disableDraggable: function() {
              var js = O.scrollbar,
                Vs = O.support.touch ? js.track : document;
              a(js.track).off(js.draggableEvents.start, js.dragStart), a(Vs).off(js.draggableEvents.move, js.dragMove), a(Vs).off(js.draggableEvents.end, js.dragEnd)
            },
            set: function() {
              if (O.params.scrollbar) {
                var js = O.scrollbar;
                js.track = a(O.params.scrollbar), O.params.uniqueNavElements && 'string' == typeof O.params.scrollbar && 1 < js.track.length && 1 === O.container.find(O.params.scrollbar).length && (js.track = O.container.find(O.params.scrollbar)), js.drag = js.track.find('.swiper-scrollbar-drag'), 0 === js.drag.length && (js.drag = a('<div class="swiper-scrollbar-drag"></div>'), js.track.append(js.drag)), js.drag[0].style.width = '', js.drag[0].style.height = '', js.trackSize = O.isHorizontal() ? js.track[0].offsetWidth : js.track[0].offsetHeight, js.divider = O.size / O.virtualSize, js.moveDivider = js.divider * (js.trackSize / O.size), js.dragSize = js.trackSize * js.divider, O.isHorizontal() ? js.drag[0].style.width = js.dragSize + 'px' : js.drag[0].style.height = js.dragSize + 'px', js.track[0].style.display = 1 <= js.divider ? 'none' : '', O.params.scrollbarHide && (js.track[0].style.opacity = 0)
              }
            },
            setTranslate: function() {
              if (O.params.scrollbar) {
                var Us, js = O.scrollbar,
                  Vs = O.translate || 0,
                  Zs = js.dragSize;
                Us = (js.trackSize - js.dragSize) * O.progress, O.rtl && O.isHorizontal() ? (Us = -Us, 0 < Us ? (Zs = js.dragSize - Us, Us = 0) : -Us + js.dragSize > js.trackSize && (Zs = js.trackSize + Us)) : 0 > Us ? (Zs = js.dragSize + Us, Us = 0) : Us + js.dragSize > js.trackSize && (Zs = js.trackSize - Us), O.isHorizontal() ? (O.support.transforms3d ? js.drag.transform('translate3d(' + Us + 'px, 0, 0)') : js.drag.transform('translateX(' + Us + 'px)'), js.drag[0].style.width = Zs + 'px') : (O.support.transforms3d ? js.drag.transform('translate3d(0px, ' + Us + 'px, 0)') : js.drag.transform('translateY(' + Us + 'px)'), js.drag[0].style.height = Zs + 'px'), O.params.scrollbarHide && (clearTimeout(js.timeout), js.track[0].style.opacity = 1, js.timeout = setTimeout(function() {
                  js.track[0].style.opacity = 0, js.track.transition(400)
                }, 1e3))
              }
            },
            setTransition: function(js) {
              O.params.scrollbar && O.scrollbar.drag.transition(js)
            }
          }, O.controller = {
            LinearSpline: function(js, Vs) {
              var Zs = function() {
                var $s, Js, ei;
                return function(ti, ai) {
                  for (Js = -1, $s = ti.length; 1 < $s - Js;) ti[ei = $s + Js >> 1] <= ai ? Js = ei : $s = ei;
                  return $s
                }
              }();
              this.x = js, this.y = Vs, this.lastIndex = js.length - 1;
              var Ks, Qs, Us = this.x.length;
              this.interpolate = function($s) {
                return $s ? (Qs = Zs(this.x, $s), Ks = Qs - 1, ($s - this.x[Ks]) * (this.y[Qs] - this.y[Ks]) / (this.x[Qs] - this.x[Ks]) + this.y[Ks]) : 0
              }
            },
            getInterpolateFunction: function(js) {
              O.controller.spline || (O.controller.spline = O.params.loop ? new O.controller.LinearSpline(O.slidesGrid, js.slidesGrid) : new O.controller.LinearSpline(O.snapGrid, js.snapGrid))
            },
            setTranslate: function(js, Vs) {
              function Zs(Js) {
                js = Js.rtl && 'horizontal' === Js.params.direction ? -O.translate : O.translate, 'slide' === O.params.controlBy && (O.controller.getInterpolateFunction(Js), Qs = -O.controller.spline.interpolate(-js)), Qs && 'container' !== O.params.controlBy || (Ks = (Js.maxTranslate() - Js.minTranslate()) / (O.maxTranslate() - O.minTranslate()), Qs = (js - O.minTranslate()) * Ks + Js.minTranslate()), O.params.controlInverse && (Qs = Js.maxTranslate() - Qs), Js.updateProgress(Qs), Js.setWrapperTranslate(Qs, !1, O), Js.updateActiveIndex()
              }
              var Ks, Qs, Us = O.params.control;
              if (Array.isArray(Us))
                for (var $s = 0; $s < Us.length; $s++) Us[$s] !== Vs && Us[$s] instanceof s && Zs(Us[$s]);
              else Us instanceof s && Vs !== Us && Zs(Us)
            },
            setTransition: function(js, Vs) {
              function Zs(Qs) {
                Qs.setWrapperTransition(js, O), 0 !== js && (Qs.onTransitionStart(), Qs.wrapper.transitionEnd(function() {
                  Us && (Qs.params.loop && 'slide' === O.params.controlBy && Qs.fixLoop(), Qs.onTransitionEnd())
                }))
              }
              var Ks, Us = O.params.control;
              if (Array.isArray(Us))
                for (Ks = 0; Ks < Us.length; Ks++) Us[Ks] !== Vs && Us[Ks] instanceof s && Zs(Us[Ks]);
              else Us instanceof s && Vs !== Us && Zs(Us)
            }
          }, O.hashnav = {
            onHashCange: function() {
              var js = document.location.hash.replace('#', ''),
                Vs = O.slides.eq(O.activeIndex).attr('data-hash');
              js !== Vs && O.slideTo(O.wrapper.children('.' + O.params.slideClass + '[data-hash="' + js + '"]').index())
            },
            attachEvents: function(js) {
              var Vs = js ? 'off' : 'on';
              a(window)[Vs]('hashchange', O.hashnav.onHashCange)
            },
            setHash: function() {
              if (O.hashnav.initialized && O.params.hashnav)
                if (O.params.replaceState && window.history && window.history.replaceState) window.history.replaceState(null, null, '#' + O.slides.eq(O.activeIndex).attr('data-hash') || '');
                else {
                  var js = O.slides.eq(O.activeIndex),
                    Vs = js.attr('data-hash') || js.attr('data-history');
                  document.location.hash = Vs || ''
                }
            },
            init: function() {
              if (O.params.hashnav && !O.params.history) {
                O.hashnav.initialized = !0;
                var js = document.location.hash.replace('#', '');
                if (js)
                  for (var Vs = 0, Zs = O.slides.length; Vs < Zs; Vs++) {
                    var Us = O.slides.eq(Vs),
                      Ks = Us.attr('data-hash') || Us.attr('data-history');
                    if (Ks === js && !Us.hasClass(O.params.slideDuplicateClass)) {
                      var Qs = Us.index();
                      O.slideTo(Qs, 0, O.params.runCallbacksOnInit, !0)
                    }
                  }
                O.params.hashnavWatchState && O.hashnav.attachEvents()
              }
            },
            destroy: function() {
              O.params.hashnavWatchState && O.hashnav.attachEvents(!0)
            }
          }, O.history = {
            init: function() {
              if (O.params.history) {
                if (!window.history || !window.history.pushState) return O.params.history = !1, void(O.params.hashnav = !0);
                O.history.initialized = !0, this.paths = this.getPathValues(), (this.paths.key || this.paths.value) && (this.scrollToSlide(0, this.paths.value, O.params.runCallbacksOnInit), !O.params.replaceState && window.addEventListener('popstate', this.setHistoryPopState))
              }
            },
            setHistoryPopState: function() {
              O.history.paths = O.history.getPathValues(), O.history.scrollToSlide(O.params.speed, O.history.paths.value, !1)
            },
            getPathValues: function() {
              var js = window.location.pathname.slice(1).split('/'),
                Vs = js.length,
                Zs = js[Vs - 2],
                Us = js[Vs - 1];
              return {
                key: Zs,
                value: Us
              }
            },
            setHistory: function(js, Vs) {
              if (O.history.initialized && O.params.history) {
                var Zs = O.slides.eq(Vs),
                  Us = this.slugify(Zs.attr('data-history'));
                window.location.pathname.includes(js) || (Us = js + '/' + Us), O.params.replaceState ? window.history.replaceState(null, null, Us) : window.history.pushState(null, null, Us)
              }
            },
            slugify: function(js) {
              return js.toString().toLowerCase().replace(/\s+/g, '-').replace(/[^\w\-]+/g, '').replace(/\-\-+/g, '-').replace(/^-+/, '').replace(/-+$/, '')
            },
            scrollToSlide: function(js, Vs, Zs) {
              if (Vs)
                for (var Us = 0, Ks = O.slides.length; Us < Ks; Us++) {
                  var Qs = O.slides.eq(Us),
                    $s = this.slugify(Qs.attr('data-history'));
                  if ($s === Vs && !Qs.hasClass(O.params.slideDuplicateClass)) {
                    var Js = Qs.index();
                    O.slideTo(Js, js, Zs)
                  }
                } else O.slideTo(0, js, Zs)
            }
          }, O.disableKeyboardControl = function() {
            O.params.keyboardControl = !1, a(document).off('keydown', g)
          }, O.enableKeyboardControl = function() {
            O.params.keyboardControl = !0, a(document).on('keydown', g)
          }, O.mousewheel = {
            event: !1,
            lastScrollTime: new window.Date().getTime()
          }, O.params.mousewheelControl && (O.mousewheel.event = -1 < navigator.userAgent.indexOf('firefox') ? 'DOMMouseScroll' : h() ? 'wheel' : 'mousewheel'), O.disableMousewheelControl = function() {
            if (!O.mousewheel.event) return !1;
            var js = O.container;
            return 'container' !== O.params.mousewheelEventsTarged && (js = a(O.params.mousewheelEventsTarged)), js.off(O.mousewheel.event, y), O.params.mousewheelControl = !1, !0
          }, O.enableMousewheelControl = function() {
            if (!O.mousewheel.event) return !1;
            var js = O.container;
            return 'container' !== O.params.mousewheelEventsTarged && (js = a(O.params.mousewheelEventsTarged)), js.on(O.mousewheel.event, y), O.params.mousewheelControl = !0, !0
          }, O.parallax = {
            setTranslate: function() {
              O.container.children('[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]').each(function() {
                v(this, O.progress)
              }), O.slides.each(function() {
                var js = a(this);
                js.find('[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]').each(function() {
                  var Vs = Math.min(Math.max(js[0].progress, -1), 1);
                  v(this, Vs)
                })
              })
            },
            setTransition: function(js) {
              'undefined' == typeof js && (js = O.params.speed), O.container.find('[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]').each(function() {
                var Vs = a(this),
                  Zs = parseInt(Vs.attr('data-swiper-parallax-duration'), 10) || js;
                0 === js && (Zs = 0), Vs.transition(Zs)
              })
            }
          }, O.zoom = {
            scale: 1,
            currentScale: 1,
            isScaling: !1,
            gesture: {
              slide: void 0,
              slideWidth: void 0,
              slideHeight: void 0,
              image: void 0,
              imageWrap: void 0,
              zoomMax: O.params.zoomMax
            },
            image: {
              isTouched: void 0,
              isMoved: void 0,
              currentX: void 0,
              currentY: void 0,
              minX: void 0,
              minY: void 0,
              maxX: void 0,
              maxY: void 0,
              width: void 0,
              height: void 0,
              startX: void 0,
              startY: void 0,
              touchesStart: {},
              touchesCurrent: {}
            },
            velocity: {
              x: void 0,
              y: void 0,
              prevPositionX: void 0,
              prevPositionY: void 0,
              prevTime: void 0
            },
            getDistanceBetweenTouches: function(js) {
              if (2 > js.targetTouches.length) return 1;
              var Vs = js.targetTouches[0].pageX,
                Zs = js.targetTouches[0].pageY,
                Us = js.targetTouches[1].pageX,
                Ks = js.targetTouches[1].pageY,
                Qs = Math.sqrt(Math.pow(Us - Vs, 2) + Math.pow(Ks - Zs, 2));
              return Qs
            },
            onGestureStart: function(js) {
              var Vs = O.zoom;
              if (!O.support.gestures) {
                if ('touchstart' !== js.type || 'touchstart' === js.type && 2 > js.targetTouches.length) return;
                Vs.gesture.scaleStart = Vs.getDistanceBetweenTouches(js)
              }
              return Vs.gesture.slide && Vs.gesture.slide.length || (Vs.gesture.slide = a(this), 0 === Vs.gesture.slide.length && (Vs.gesture.slide = O.slides.eq(O.activeIndex)), Vs.gesture.image = Vs.gesture.slide.find('img, svg, canvas'), Vs.gesture.imageWrap = Vs.gesture.image.parent('.' + O.params.zoomContainerClass), Vs.gesture.zoomMax = Vs.gesture.imageWrap.attr('data-swiper-zoom') || O.params.zoomMax, 0 !== Vs.gesture.imageWrap.length) ? void(Vs.gesture.image.transition(0), Vs.isScaling = !0) : void(Vs.gesture.image = void 0)
            },
            onGestureChange: function(js) {
              var Vs = O.zoom;
              if (!O.support.gestures) {
                if ('touchmove' !== js.type || 'touchmove' === js.type && 2 > js.targetTouches.length) return;
                Vs.gesture.scaleMove = Vs.getDistanceBetweenTouches(js)
              }
              Vs.gesture.image && 0 !== Vs.gesture.image.length && (Vs.scale = O.support.gestures ? js.scale * Vs.currentScale : Vs.gesture.scaleMove / Vs.gesture.scaleStart * Vs.currentScale, Vs.scale > Vs.gesture.zoomMax && (Vs.scale = Vs.gesture.zoomMax - 1 + Math.pow(Vs.scale - Vs.gesture.zoomMax + 1, 0.5)), Vs.scale < O.params.zoomMin && (Vs.scale = O.params.zoomMin + 1 - Math.pow(O.params.zoomMin - Vs.scale + 1, 0.5)), Vs.gesture.image.transform('translate3d(0,0,0) scale(' + Vs.scale + ')'))
            },
            onGestureEnd: function(js) {
              var Vs = O.zoom;
              (O.support.gestures || 'touchend' === js.type && ('touchend' !== js.type || !(2 > js.changedTouches.length))) && Vs.gesture.image && 0 !== Vs.gesture.image.length && (Vs.scale = Math.max(Math.min(Vs.scale, Vs.gesture.zoomMax), O.params.zoomMin), Vs.gesture.image.transition(O.params.speed).transform('translate3d(0,0,0) scale(' + Vs.scale + ')'), Vs.currentScale = Vs.scale, Vs.isScaling = !1, 1 === Vs.scale && (Vs.gesture.slide = void 0))
            },
            onTouchStart: function(js, Vs) {
              var Zs = js.zoom;
              !Zs.gesture.image || 0 === Zs.gesture.image.length || Zs.image.isTouched || ('android' === js.device.os && Vs.preventDefault(), Zs.image.isTouched = !0, Zs.image.touchesStart.x = 'touchstart' === Vs.type ? Vs.targetTouches[0].pageX : Vs.pageX, Zs.image.touchesStart.y = 'touchstart' === Vs.type ? Vs.targetTouches[0].pageY : Vs.pageY)
            },
            onTouchMove: function(js) {
              var Vs = O.zoom;
              if (Vs.gesture.image && 0 !== Vs.gesture.image.length && (O.allowClick = !1, Vs.image.isTouched && Vs.gesture.slide)) {
                Vs.image.isMoved || (Vs.image.width = Vs.gesture.image[0].offsetWidth, Vs.image.height = Vs.gesture.image[0].offsetHeight, Vs.image.startX = O.getTranslate(Vs.gesture.imageWrap[0], 'x') || 0, Vs.image.startY = O.getTranslate(Vs.gesture.imageWrap[0], 'y') || 0, Vs.gesture.slideWidth = Vs.gesture.slide[0].offsetWidth, Vs.gesture.slideHeight = Vs.gesture.slide[0].offsetHeight, Vs.gesture.imageWrap.transition(0), O.rtl && (Vs.image.startX = -Vs.image.startX), O.rtl && (Vs.image.startY = -Vs.image.startY));
                var Zs = Vs.image.width * Vs.scale,
                  Us = Vs.image.height * Vs.scale;
                if (!(Zs < Vs.gesture.slideWidth && Us < Vs.gesture.slideHeight)) {
                  if (Vs.image.minX = Math.min(Vs.gesture.slideWidth / 2 - Zs / 2, 0), Vs.image.maxX = -Vs.image.minX, Vs.image.minY = Math.min(Vs.gesture.slideHeight / 2 - Us / 2, 0), Vs.image.maxY = -Vs.image.minY, Vs.image.touchesCurrent.x = 'touchmove' === js.type ? js.targetTouches[0].pageX : js.pageX, Vs.image.touchesCurrent.y = 'touchmove' === js.type ? js.targetTouches[0].pageY : js.pageY, !Vs.image.isMoved && !Vs.isScaling) {
                    if (O.isHorizontal() && Math.floor(Vs.image.minX) === Math.floor(Vs.image.startX) && Vs.image.touchesCurrent.x < Vs.image.touchesStart.x || Math.floor(Vs.image.maxX) === Math.floor(Vs.image.startX) && Vs.image.touchesCurrent.x > Vs.image.touchesStart.x) return void(Vs.image.isTouched = !1);
                    if (!O.isHorizontal() && Math.floor(Vs.image.minY) === Math.floor(Vs.image.startY) && Vs.image.touchesCurrent.y < Vs.image.touchesStart.y || Math.floor(Vs.image.maxY) === Math.floor(Vs.image.startY) && Vs.image.touchesCurrent.y > Vs.image.touchesStart.y) return void(Vs.image.isTouched = !1)
                  }
                  js.preventDefault(), js.stopPropagation(), Vs.image.isMoved = !0, Vs.image.currentX = Vs.image.touchesCurrent.x - Vs.image.touchesStart.x + Vs.image.startX, Vs.image.currentY = Vs.image.touchesCurrent.y - Vs.image.touchesStart.y + Vs.image.startY, Vs.image.currentX < Vs.image.minX && (Vs.image.currentX = Vs.image.minX + 1 - Math.pow(Vs.image.minX - Vs.image.currentX + 1, 0.8)), Vs.image.currentX > Vs.image.maxX && (Vs.image.currentX = Vs.image.maxX - 1 + Math.pow(Vs.image.currentX - Vs.image.maxX + 1, 0.8)), Vs.image.currentY < Vs.image.minY && (Vs.image.currentY = Vs.image.minY + 1 - Math.pow(Vs.image.minY - Vs.image.currentY + 1, 0.8)), Vs.image.currentY > Vs.image.maxY && (Vs.image.currentY = Vs.image.maxY - 1 + Math.pow(Vs.image.currentY - Vs.image.maxY + 1, 0.8)), Vs.velocity.prevPositionX || (Vs.velocity.prevPositionX = Vs.image.touchesCurrent.x), Vs.velocity.prevPositionY || (Vs.velocity.prevPositionY = Vs.image.touchesCurrent.y), Vs.velocity.prevTime || (Vs.velocity.prevTime = Date.now()), Vs.velocity.x = (Vs.image.touchesCurrent.x - Vs.velocity.prevPositionX) / (Date.now() - Vs.velocity.prevTime) / 2, Vs.velocity.y = (Vs.image.touchesCurrent.y - Vs.velocity.prevPositionY) / (Date.now() - Vs.velocity.prevTime) / 2, 2 > Math.abs(Vs.image.touchesCurrent.x - Vs.velocity.prevPositionX) && (Vs.velocity.x = 0), 2 > Math.abs(Vs.image.touchesCurrent.y - Vs.velocity.prevPositionY) && (Vs.velocity.y = 0), Vs.velocity.prevPositionX = Vs.image.touchesCurrent.x, Vs.velocity.prevPositionY = Vs.image.touchesCurrent.y, Vs.velocity.prevTime = Date.now(), Vs.gesture.imageWrap.transform('translate3d(' + Vs.image.currentX + 'px, ' + Vs.image.currentY + 'px,0)')
                }
              }
            },
            onTouchEnd: function(js) {
              var Vs = js.zoom;
              if (Vs.gesture.image && 0 !== Vs.gesture.image.length) {
                if (!Vs.image.isTouched || !Vs.image.isMoved) return Vs.image.isTouched = !1, void(Vs.image.isMoved = !1);
                Vs.image.isTouched = !1, Vs.image.isMoved = !1;
                var Zs = 300,
                  Us = 300,
                  Ks = Vs.velocity.x * Zs,
                  Qs = Vs.image.currentX + Ks,
                  $s = Vs.velocity.y * Us,
                  Js = Vs.image.currentY + $s;
                0 !== Vs.velocity.x && (Zs = Math.abs((Qs - Vs.image.currentX) / Vs.velocity.x)), 0 !== Vs.velocity.y && (Us = Math.abs((Js - Vs.image.currentY) / Vs.velocity.y));
                var ei = Math.max(Zs, Us);
                Vs.image.currentX = Qs, Vs.image.currentY = Js;
                var ti = Vs.image.width * Vs.scale,
                  ai = Vs.image.height * Vs.scale;
                Vs.image.minX = Math.min(Vs.gesture.slideWidth / 2 - ti / 2, 0), Vs.image.maxX = -Vs.image.minX, Vs.image.minY = Math.min(Vs.gesture.slideHeight / 2 - ai / 2, 0), Vs.image.maxY = -Vs.image.minY, Vs.image.currentX = Math.max(Math.min(Vs.image.currentX, Vs.image.maxX), Vs.image.minX), Vs.image.currentY = Math.max(Math.min(Vs.image.currentY, Vs.image.maxY), Vs.image.minY), Vs.gesture.imageWrap.transition(ei).transform('translate3d(' + Vs.image.currentX + 'px, ' + Vs.image.currentY + 'px,0)')
              }
            },
            onTransitionEnd: function(js) {
              var Vs = js.zoom;
              Vs.gesture.slide && js.previousIndex !== js.activeIndex && (Vs.gesture.image.transform('translate3d(0,0,0) scale(1)'), Vs.gesture.imageWrap.transform('translate3d(0,0,0)'), Vs.gesture.slide = Vs.gesture.image = Vs.gesture.imageWrap = void 0, Vs.scale = Vs.currentScale = 1)
            },
            toggleZoom: function(js, Vs) {
              var Zs = js.zoom;
              if (Zs.gesture.slide || (Zs.gesture.slide = js.clickedSlide ? a(js.clickedSlide) : js.slides.eq(js.activeIndex), Zs.gesture.image = Zs.gesture.slide.find('img, svg, canvas'), Zs.gesture.imageWrap = Zs.gesture.image.parent('.' + js.params.zoomContainerClass)), Zs.gesture.image && 0 !== Zs.gesture.image.length) {
                var Us, Ks, Qs, $s, Js, ei, ti, ai, si, ii, ri, oi, ni, li, di, pi, mi, ci;
                'undefined' == typeof Zs.image.touchesStart.x && Vs ? (Us = 'touchend' === Vs.type ? Vs.changedTouches[0].pageX : Vs.pageX, Ks = 'touchend' === Vs.type ? Vs.changedTouches[0].pageY : Vs.pageY) : (Us = Zs.image.touchesStart.x, Ks = Zs.image.touchesStart.y), Zs.scale && 1 !== Zs.scale ? (Zs.scale = Zs.currentScale = 1, Zs.gesture.imageWrap.transition(300).transform('translate3d(0,0,0)'), Zs.gesture.image.transition(300).transform('translate3d(0,0,0) scale(1)'), Zs.gesture.slide = void 0) : (Zs.scale = Zs.currentScale = Zs.gesture.imageWrap.attr('data-swiper-zoom') || js.params.zoomMax, Vs ? (mi = Zs.gesture.slide[0].offsetWidth, ci = Zs.gesture.slide[0].offsetHeight, Qs = Zs.gesture.slide.offset().left, $s = Zs.gesture.slide.offset().top, Js = Qs + mi / 2 - Us, ei = $s + ci / 2 - Ks, si = Zs.gesture.image[0].offsetWidth, ii = Zs.gesture.image[0].offsetHeight, ri = si * Zs.scale, oi = ii * Zs.scale, ni = Math.min(mi / 2 - ri / 2, 0), li = Math.min(ci / 2 - oi / 2, 0), di = -ni, pi = -li, ti = Js * Zs.scale, ai = ei * Zs.scale, ti < ni && (ti = ni), ti > di && (ti = di), ai < li && (ai = li), ai > pi && (ai = pi)) : (ti = 0, ai = 0), Zs.gesture.imageWrap.transition(300).transform('translate3d(' + ti + 'px, ' + ai + 'px,0)'), Zs.gesture.image.transition(300).transform('translate3d(0,0,0) scale(' + Zs.scale + ')'))
              }
            },
            attachEvents: function(js) {
              var Vs = js ? 'off' : 'on';
              if (O.params.zoom) {
                var Zs = O.slides,
                  Us = 'touchstart' === O.touchEvents.start && O.support.passiveListener && O.params.passiveListeners && {
                    passive: !0,
                    capture: !1
                  };
                O.support.gestures ? (O.slides[Vs]('gesturestart', O.zoom.onGestureStart, Us), O.slides[Vs]('gesturechange', O.zoom.onGestureChange, Us), O.slides[Vs]('gestureend', O.zoom.onGestureEnd, Us)) : 'touchstart' === O.touchEvents.start && (O.slides[Vs](O.touchEvents.start, O.zoom.onGestureStart, Us), O.slides[Vs](O.touchEvents.move, O.zoom.onGestureChange, Us), O.slides[Vs](O.touchEvents.end, O.zoom.onGestureEnd, Us)), O[Vs]('touchStart', O.zoom.onTouchStart), O.slides.each(function(Ks, Qs) {
                  0 < a(Qs).find('.' + O.params.zoomContainerClass).length && a(Qs)[Vs](O.touchEvents.move, O.zoom.onTouchMove)
                }), O[Vs]('touchEnd', O.zoom.onTouchEnd), O[Vs]('transitionEnd', O.zoom.onTransitionEnd), O.params.zoomToggle && O.on('doubleTap', O.zoom.toggleZoom)
              }
            },
            init: function() {
              O.zoom.attachEvents()
            },
            destroy: function() {
              O.zoom.attachEvents(!0)
            }
          }, O._plugins = [], O.plugins) {
          var qs = O.plugins[Ys](O, O.params[Ys]);
          qs && O._plugins.push(qs)
        }
        return O.callPlugins = function(js) {
          for (var Vs = 0; Vs < O._plugins.length; Vs++) js in O._plugins[Vs] && O._plugins[Vs][js](arguments[1], arguments[2], arguments[3], arguments[4], arguments[5])
        }, O.emitterEventListeners = {}, O.emit = function(js) {
          O.params[js] && O.params[js](arguments[1], arguments[2], arguments[3], arguments[4], arguments[5]);
          var Vs;
          if (O.emitterEventListeners[js])
            for (Vs = 0; Vs < O.emitterEventListeners[js].length; Vs++) O.emitterEventListeners[js][Vs](arguments[1], arguments[2], arguments[3], arguments[4], arguments[5]);
          O.callPlugins && O.callPlugins(js, arguments[1], arguments[2], arguments[3], arguments[4], arguments[5])
        }, O.on = function(js, Vs) {
          return js = x(js), O.emitterEventListeners[js] || (O.emitterEventListeners[js] = []), O.emitterEventListeners[js].push(Vs), O
        }, O.off = function(js, Vs) {
          var Zs;
          if (js = x(js), 'undefined' == typeof Vs) return O.emitterEventListeners[js] = [], O;
          if (O.emitterEventListeners[js] && 0 !== O.emitterEventListeners[js].length) {
            for (Zs = 0; Zs < O.emitterEventListeners[js].length; Zs++) O.emitterEventListeners[js][Zs] === Vs && O.emitterEventListeners[js].splice(Zs, 1);
            return O
          }
        }, O.once = function(js, Vs) {
          js = x(js);
          var Zs = function() {
            Vs(arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]), O.off(js, Zs)
          };
          return O.on(js, Zs), O
        }, O.a11y = {
          makeFocusable: function(js) {
            return js.attr('tabIndex', '0'), js
          },
          addRole: function(js, Vs) {
            return js.attr('role', Vs), js
          },
          addLabel: function(js, Vs) {
            return js.attr('aria-label', Vs), js
          },
          disable: function(js) {
            return js.attr('aria-disabled', !0), js
          },
          enable: function(js) {
            return js.attr('aria-disabled', !1), js
          },
          onEnterKey: function(js) {
            13 !== js.keyCode || (a(js.target).is(O.params.nextButton) ? (O.onClickNext(js), O.isEnd ? O.a11y.notify(O.params.lastSlideMessage) : O.a11y.notify(O.params.nextSlideMessage)) : a(js.target).is(O.params.prevButton) && (O.onClickPrev(js), O.isBeginning ? O.a11y.notify(O.params.firstSlideMessage) : O.a11y.notify(O.params.prevSlideMessage)), a(js.target).is('.' + O.params.bulletClass) && a(js.target)[0].click())
          },
          liveRegion: a('<span class="' + O.params.notificationClass + '" aria-live="assertive" aria-atomic="true"></span>'),
          notify: function(js) {
            var Vs = O.a11y.liveRegion;
            0 === Vs.length || (Vs.html(''), Vs.html(js))
          },
          init: function() {
            O.params.nextButton && O.nextButton && 0 < O.nextButton.length && (O.a11y.makeFocusable(O.nextButton), O.a11y.addRole(O.nextButton, 'button'), O.a11y.addLabel(O.nextButton, O.params.nextSlideMessage)), O.params.prevButton && O.prevButton && 0 < O.prevButton.length && (O.a11y.makeFocusable(O.prevButton), O.a11y.addRole(O.prevButton, 'button'), O.a11y.addLabel(O.prevButton, O.params.prevSlideMessage)), a(O.container).append(O.a11y.liveRegion)
          },
          initPagination: function() {
            O.params.pagination && O.params.paginationClickable && O.bullets && O.bullets.length && O.bullets.each(function() {
              var js = a(this);
              O.a11y.makeFocusable(js), O.a11y.addRole(js, 'button'), O.a11y.addLabel(js, O.params.paginationBulletMessage.replace(/{{index}}/, js.index() + 1))
            })
          },
          destroy: function() {
            O.a11y.liveRegion && 0 < O.a11y.liveRegion.length && O.a11y.liveRegion.remove()
          }
        }, O.init = function() {
          O.params.loop && O.createLoop(), O.updateContainerSize(), O.updateSlidesSize(), O.updatePagination(), O.params.scrollbar && O.scrollbar && (O.scrollbar.set(), O.params.scrollbarDraggable && O.scrollbar.enableDraggable()), 'slide' !== O.params.effect && O.effects[O.params.effect] && (!O.params.loop && O.updateProgress(), O.effects[O.params.effect].setTranslate()), O.params.loop ? O.slideTo(O.params.initialSlide + O.loopedSlides, 0, O.params.runCallbacksOnInit) : (O.slideTo(O.params.initialSlide, 0, O.params.runCallbacksOnInit), 0 === O.params.initialSlide && (O.parallax && O.params.parallax && O.parallax.setTranslate(), O.lazy && O.params.lazyLoading && (O.lazy.load(), O.lazy.initialImageLoaded = !0))), O.attachEvents(), O.params.observer && O.support.observer && O.initObservers(), O.params.preloadImages && !O.params.lazyLoading && O.preloadImages(), O.params.zoom && O.zoom && O.zoom.init(), O.params.autoplay && O.startAutoplay(), O.params.keyboardControl && O.enableKeyboardControl && O.enableKeyboardControl(), O.params.mousewheelControl && O.enableMousewheelControl && O.enableMousewheelControl(), O.params.hashnavReplaceState && (O.params.replaceState = O.params.hashnavReplaceState), O.params.history && O.history && O.history.init(), O.params.hashnav && O.hashnav && O.hashnav.init(), O.params.a11y && O.a11y && O.a11y.init(), O.emit('onInit', O)
        }, O.cleanupStyles = function() {
          O.container.removeClass(O.classNames.join(' ')).removeAttr('style'), O.wrapper.removeAttr('style'), O.slides && O.slides.length && O.slides.removeClass([O.params.slideVisibleClass, O.params.slideActiveClass, O.params.slideNextClass, O.params.slidePrevClass].join(' ')).removeAttr('style').removeAttr('data-swiper-column').removeAttr('data-swiper-row'), O.paginationContainer && O.paginationContainer.length && O.paginationContainer.removeClass(O.params.paginationHiddenClass), O.bullets && O.bullets.length && O.bullets.removeClass(O.params.bulletActiveClass), O.params.prevButton && a(O.params.prevButton).removeClass(O.params.buttonDisabledClass), O.params.nextButton && a(O.params.nextButton).removeClass(O.params.buttonDisabledClass), O.params.scrollbar && O.scrollbar && (O.scrollbar.track && O.scrollbar.track.length && O.scrollbar.track.removeAttr('style'), O.scrollbar.drag && O.scrollbar.drag.length && O.scrollbar.drag.removeAttr('style'))
        }, O.destroy = function(js, Vs) {
          O.detachEvents(), O.stopAutoplay(), O.params.scrollbar && O.scrollbar && O.params.scrollbarDraggable && O.scrollbar.disableDraggable(), O.params.loop && O.destroyLoop(), Vs && O.cleanupStyles(), O.disconnectObservers(), O.params.zoom && O.zoom && O.zoom.destroy(), O.params.keyboardControl && O.disableKeyboardControl && O.disableKeyboardControl(), O.params.mousewheelControl && O.disableMousewheelControl && O.disableMousewheelControl(), O.params.a11y && O.a11y && O.a11y.destroy(), O.params.history && !O.params.replaceState && window.removeEventListener('popstate', O.history.setHistoryPopState), O.params.hashnav && O.hashnav && O.hashnav.destroy(), O.emit('onDestroy'), !1 !== js && (O = null)
        }, O.init(), O
      }
    };
    s.prototype = {
      isSafari: function() {
        var l = window.navigator.userAgent.toLowerCase();
        return 0 <= l.indexOf('safari') && 0 > l.indexOf('chrome') && 0 > l.indexOf('android')
      }(),
      isUiWebView: /(iPhone|iPod|iPad).*AppleWebKit(?!.*Safari)/i.test(window.navigator.userAgent),
      isArray: function(l) {
        return '[object Array]' === Object.prototype.toString.apply(l)
      },
      browser: {
        ie: window.navigator.pointerEnabled || window.navigator.msPointerEnabled,
        ieTouch: window.navigator.msPointerEnabled && 1 < window.navigator.msMaxTouchPoints || window.navigator.pointerEnabled && 1 < window.navigator.maxTouchPoints,
        lteIE9: function() {
          var l = document.createElement('div');
          return l.innerHTML = '<!--[if lte IE 9]><i></i><![endif]-->', 1 === l.getElementsByTagName('i').length
        }()
      },
      device: function() {
        var l = window.navigator.userAgent,
          d = l.match(/(Android);?[\s\/]+([\d.]+)?/),
          p = l.match(/(iPad).*OS\s([\d_]+)/),
          m = l.match(/(iPod)(.*OS\s([\d_]+))?/),
          c = !p && l.match(/(iPhone\sOS|iOS)\s([\d_]+)/);
        return {
          ios: p || c || m,
          android: d
        }
      }(),
      support: {
        touch: window.Modernizr && !0 === Modernizr.touch || function() {
          return !!('ontouchstart' in window || window.DocumentTouch && document instanceof DocumentTouch)
        }(),
        transforms3d: window.Modernizr && !0 === Modernizr.csstransforms3d || function() {
          var l = document.createElement('div').style;
          return 'webkitPerspective' in l || 'MozPerspective' in l || 'OPerspective' in l || 'MsPerspective' in l || 'perspective' in l
        }(),
        flexbox: function() {
          for (var l = document.createElement('div').style, d = 'alignItems webkitAlignItems webkitBoxAlign msFlexAlign mozBoxAlign webkitFlexDirection msFlexDirection mozBoxDirection mozBoxOrient webkitBoxDirection webkitBoxOrient'.split(' '), p = 0; p < d.length; p++)
            if (d[p] in l) return !0
        }(),
        observer: function() {
          return 'MutationObserver' in window || 'WebkitMutationObserver' in window
        }(),
        passiveListener: function() {
          var l = !1;
          try {
            var d = Object.defineProperty({}, 'passive', {
              get: function() {
                l = !0
              }
            });
            window.addEventListener('testPassiveListener', null, d)
          } catch (p) {}
          return l
        }(),
        gestures: function() {
          return 'ongesturestart' in window
        }()
      },
      plugins: {}
    };
    for (var r = ['jQuery', 'Zepto', 'Dom7'], o = 0; o < r.length; o++) window[r[o]] && t(window[r[o]]);
    var n;
    n = 'undefined' == typeof Dom7 ? window.Dom7 || window.Zepto || window.jQuery : Dom7, n && (!('transitionEnd' in n.fn) && (n.fn.transitionEnd = function(l) {
      function d(u) {
        if (u.target === this)
          for (l.call(this, u), c = 0; c < p.length; c++) m.off(p[c], d)
      }
      var c, p = ['webkitTransitionEnd', 'transitionend', 'oTransitionEnd', 'MSTransitionEnd', 'msTransitionEnd'],
        m = this;
      if (l)
        for (c = 0; c < p.length; c++) m.on(p[c], d);
      return this
    }), !('transform' in n.fn) && (n.fn.transform = function(l) {
      for (var p, d = 0; d < this.length; d++) p = this[d].style, p.webkitTransform = p.MsTransform = p.msTransform = p.MozTransform = p.OTransform = p.transform = l;
      return this
    }), !('transition' in n.fn) && (n.fn.transition = function(l) {
      'string' != typeof l && (l += 'ms');
      for (var p, d = 0; d < this.length; d++) p = this[d].style, p.webkitTransitionDuration = p.MsTransitionDuration = p.msTransitionDuration = p.MozTransitionDuration = p.OTransitionDuration = p.transitionDuration = l;
      return this
    }), !('outerWidth' in n.fn) && (n.fn.outerWidth = function(l) {
      return 0 < this.length ? l ? this[0].offsetWidth + parseFloat(this.css('margin-right')) + parseFloat(this.css('margin-left')) : this[0].offsetWidth : null
    })), window.Swiper = s
  }(), 'undefined' == typeof module ? 'function' == typeof define && define.amd && define([], function() {
    'use strict';
    return window.Swiper
  }) : module.exports = window.Swiper;
jQuery(document).ready(function(t) {
  'use strict';

  function a(l) {
    l.get(0).paused ? l.get(0).play() : l.get(0).pause()
  }

  function s(l, d) {
    l.length && (t(l).on('playing', function() {
      d.addClass(r), d.removeClass(o)
    }), 0 < t(l).currentTime && d.addClass('is-playing is-visible'), l.get(0).paused ? (d.addClass(o), d.removeClass(r)) : (d.addClass(r), d.removeClass(o)))
  }
  var r = 'video-playing',
    o = 'video-paused',
    n = t('.js-video-container');
  n.each(function(l, d) {
    var p = t(d),
      m = p.find('.js-video'),
      c = p.find('.js-video-playpause'),
      u = p.parents('.js-video-background');
    s(m, p), m.controls = 0, c.on('click', function(g) {
      g.preventDefault(), u.length || (a(m), s(m, p))
    }), u.on('click', function() {
      a(m), s(m, p)
    })
  })
});
